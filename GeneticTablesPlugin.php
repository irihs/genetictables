<?php
/**
 * Main plugin class
 */

ini_set("log_errors", 1);
ini_set("error_log", __DIR__."/php-error.log");

class GeneticTablesPlugin extends Omeka_Plugin_AbstractPlugin
{
	protected $_hooks = array(
		'install', 
		'uninstall',
		'upgrade',
		'after_save_record',
		'initialize',
		'public_items_show',
		'public_items_browse_each',
		'define_acl',
		'config',
		'config_form'
	);
	
	protected $_filters = array(
		'admin_items_form_tabs',
		'admin_navigation_main',
		'public_navigation_main'
	);
	
	//names of the omeka item types added by this plugin
	public $workTypeName = "Work";
	public $folioTypeName = "Folio";
	public $tableTypeName = "Genetic Table";
	
	//names of the omeka elements (metadata fields) added by this plugin
	public $numberElementName = "Folio Number";
	public $transcriptionElementName = "Folio Transcription";
	public $transcriptionCssElementName = "Transcription CSS";
	public $transcriptionHelpElementName = "Transcription Help";
	public $tableFooterElementName = "Table Footer";
	public $folioIncipitElementName = "Incipit";
	public $folioExplicitElementName = "Explicit";
	public $workAuthorNotesElementName = "Author Notes Description";
	public $folioAuthorNotesElementName = "Author Notes";
	public $folioAuthorNotesTagsElementName = "Author Notes Tags";
	public $workCensoredElementName = "Censored Description";
	public $folioCensoredElementName = "Censored";
	
	//DB tables names
	public $sourceTableName = 'genetictables_sources';
	public $fragmentTableName = 'genetictables_fragments';
	public $tableFragmentsTableName = 'genetictables_tablefragments';
	public $chaptersTableName = 'genetictables_chapters';
	public $displayTableName = 'genetictables_display';
	public $codeTableName = 'genetictables_codes';
	public $tagsTableName = 'genetictables_tags';
	
	//commonly used tables (see hook initialize)
	public $itemTable;
	public $itemTypeTable;
	public $elementTable;
	public $elementTextTable;
	public $collectionTable;
	
	//commonly used ids (see hook initialize)
	public $numberElementId;
	public $transcriptionElementId;
	
	//commonly used translations (see hook initialize)
	public $folioNumberTranslation;
	public $folioTranscriptionTranslation;
	public $transcriptionCssTranslation;
	public $transcriptionHelpTranslation;
	public $tableFooterTranslation;
	public $workAuthorNotesTranslation;
	public $workCensoredTranslation;
	public $folioAuthorNotesTranslation;
	public $folioAuthorNotesTagsTranslation;
	public $folioCensoredTranslation;
	
	public $installing = false;
	
	public function hookInstall()
	{
		$db = $this->_db;
		$this->installing = true;
		
		//add the new elements
		$elementSet = $db->getTable('ElementSet')->findByName('Item Type Metadata');
		$elementSet->addElements(
				array(
						array('name' => $this->numberElementName, 'description' => __("The folio number within the source.")),
						array('name' => $this->transcriptionElementName, 'description' => __("Transcription of the text on the folio.")),
						array('name' => $this->transcriptionCssElementName, 'description' => __("CSS stylesheet to use for the transcriptions.")),
						array('name' => $this->transcriptionHelpElementName, 'description' => __("Inline help for the transcriptions.")),
						array('name' => $this->tableFooterElementName, 'description' => __("Genetic edition table footer.")),
						array('name' => $this->folioIncipitElementName, 'description' => __("The first few words of a folio.")),
						array('name' => $this->folioExplicitElementName, 'description' => __("The last few words of a folio.")),
						array('name' => $this->workAuthorNotesElementName, 'description' => __("Description of the notes from the author.")),
						array('name' => $this->folioAuthorNotesElementName, 'description' => __("Notes from the author on a folio.")),
						array('name' => $this->folioAuthorNotesTagsElementName, 'description' => __("Category of a note.")),
						array('name' => $this->workCensoredElementName, 'description' => __("Description of the censored text of a work.")),
						array('name' => $this->folioCensoredElementName, 'description' => __("Censored text in an edition."))
				));
		$elementSet->save();
		
		//add the new item types
		$elements = $db->getTable('Element');
		insert_item_type(
				array('name' => $this->workTypeName, 'description' => __("A work made up of folios.")),
				array(
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->transcriptionCssElementName),
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->transcriptionHelpElementName),
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->workAuthorNotesElementName),
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->workCensoredElementName)
				));
		insert_item_type(
				array('name' => $this->folioTypeName, 'description' => __("A folio that is part of the genetic edition of a manuscript.")),
				array(
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->numberElementName),
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->transcriptionElementName),
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->folioIncipitElementName),
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->folioExplicitElementName),
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->folioAuthorNotesElementName),
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->folioAuthorNotesTagsElementName),
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->folioCensoredElementName)
				));
		insert_item_type(
				array('name' => $this->tableTypeName, 'description' => __("Representation of the genetic edition of a manuscript.")),
				array(
						$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->tableFooterElementName)
				));
		
		//create model (GeneticTablesList.php)
		$query = "
			CREATE TABLE IF NOT EXISTS `$db->GeneticTablesList` (
				`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
		$db->query($query);
		
		//create the tags table
		$query = "
			CREATE TABLE IF NOT EXISTS `".$this->tagsTableName."` (
				`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`tableId` int(10) unsigned NOT NULL,
				`rank` int(10) unsigned NOT NULL,
				`color` int(10) unsigned NOT NULL,
				`shortName` varchar(10) NOT NULL,
				`name` TINYTEXT NOT NULL,
				PRIMARY KEY (`id`),
				FOREIGN KEY (`tableId`) REFERENCES `".$db->getTable('Item')->getTableName()."` (`id`) ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
		$db->query($query);
		
		//create the fragment table
		$query = "
			CREATE TABLE IF NOT EXISTS `".$this->fragmentTableName."` (
				`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`folioId` int(10) unsigned NOT NULL,
				`fromPage` int(10) unsigned NOT NULL,
				`fromLine` int(10) unsigned NOT NULL,
				`toPage` int(10) unsigned NOT NULL,
				`toLine` int(10) unsigned NOT NULL,
				`position` tinytext COLLATE utf8_unicode_ci NOT NULL,
				`withheld` tinyint(4) unsigned NOT NULL,
				`parentId` int(10) unsigned,
				`previousId` int(10) unsigned,
				PRIMARY KEY (`id`),
				KEY `parentId` (`parentId`),
				FOREIGN KEY (`folioId`) REFERENCES `".$db->getTable('Item')->getTableName()."` (`id`) ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
		$db->query($query);
		$db->query("ALTER TABLE `".$this->fragmentTableName."` ADD FOREIGN KEY (`parentId`) REFERENCES `".$this->fragmentTableName."` (`id`) ON DELETE SET NULL");
		$db->query("ALTER TABLE `".$this->fragmentTableName."` ADD FOREIGN KEY (`previousId`) REFERENCES `".$this->fragmentTableName."` (`id`) ON DELETE SET NULL");
		
		//create the table fragments table
		$query = "
			CREATE TABLE IF NOT EXISTS `".$this->tableFragmentsTableName."` (
				`tagId` int(10) unsigned NOT NULL,				
				`fragmentId` int(10) unsigned NOT NULL,
				PRIMARY KEY (`tagId`, `fragmentId`),
				FOREIGN KEY (`fragmentId`) REFERENCES `".$this->fragmentTableName."` (`id`) ON DELETE CASCADE,
				FOREIGN KEY (`tagId`) REFERENCES `".$this->tagsTableName."` (`id`) ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
		$db->query($query);
		
		//create the sources table
		$query = "
			CREATE TABLE IF NOT EXISTS `".$this->sourceTableName."` (
				`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`workId` int(10) unsigned NOT NULL,
				`rank` int(10) unsigned NOT NULL,
				`color` int(10) unsigned NOT NULL,
				`shortName` varchar(10) NOT NULL,
				PRIMARY KEY (`id`),
				FOREIGN KEY (`id`) REFERENCES `".$db->getTable('Collection')->getTableName()."` (`id`) ON DELETE CASCADE,
				FOREIGN KEY (`workId`) REFERENCES `".$db->getTable('Item')->getTableName()."` (`id`) ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
		$db->query($query);
		
		//create the chapters table
		$query = "
			CREATE TABLE `".$this->chaptersTableName."` (
				`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				`workId` INT(10) UNSIGNED NOT NULL,
				`toPage` INT(10) NOT NULL,
				`name` TINYTEXT NOT NULL,
				PRIMARY KEY (`id`),
				FOREIGN KEY (`workId`) REFERENCES `".$db->getTable('Item')->getTableName()."` (`id`) ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
		$db->query($query);
		
		//create the display table
		$query = "
			CREATE TABLE `".$this->displayTableName."` (
				`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				`tableId` INT(10) UNSIGNED NOT NULL,
				`workId` INT(10) UNSIGNED NOT NULL,
				`chapterDisplay` TINYINT NOT NULL,
				`pageSpan` INT(10) UNSIGNED NOT NULL,
				`tableColor` INT(10) UNSIGNED NOT NULL,
				`textColor` INT(10) UNSIGNED NOT NULL,
				`incipitSourceId` INT(10) UNSIGNED,
				PRIMARY KEY (`id`),
				FOREIGN KEY (`tableId`) REFERENCES `".$db->getTable('Item')->getTableName()."` (`id`) ON DELETE CASCADE,
				FOREIGN KEY (`workId`) REFERENCES `".$db->getTable('Item')->getTableName()."` (`id`) ON DELETE CASCADE,
				FOREIGN KEY (`incipitSourceId`) REFERENCES `".$db->getTable('Collection')->getTableName()."` (`id`) ON DELETE SET NULL
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
		$db->query($query);
		
		//create the codes table
		$query = "
			CREATE TABLE `".$this->codeTableName."` (
				`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				`workId` INT(10) UNSIGNED NOT NULL,
				`name` TINYTEXT NOT NULL,
				`style` TINYTEXT NOT NULL,
				`rank` INT(10) UNSIGNED NOT NULL,
				PRIMARY KEY (`id`),
				FOREIGN KEY (`workId`) REFERENCES `".$db->getTable('Item')->getTableName()."` (`id`) ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
		$db->query($query);
		
		$this->_installOptions();
		$this->installing = false;
	}
	
	public function hookUninstall()
	{
		//delete the database data
		$db = $this->_db;
		$this->installing = true;
		
		//drop tables
		$db->query("DROP TABLE IF EXISTS `$db->GeneticTablesList`");
		$db->query("DROP TABLE IF EXISTS `$this->tableFragmentsTableName`");
		$db->query("DROP TABLE IF EXISTS `$this->fragmentTableName`");
		$db->query("DROP TABLE IF EXISTS `$this->tagsTableName`");
		$db->query("DROP TABLE IF EXISTS `$this->sourceTableName`");
		$db->query("DROP TABLE IF EXISTS `$this->chaptersTableName`");
		$db->query("DROP TABLE IF EXISTS `$this->displayTableName`");
		$db->query("DROP TABLE IF EXISTS `$this->codeTableName`");
		
		//delete the omeka item types
		$types = get_db()->getTable('ItemType');
		$types->findByName($this->tableTypeName)->delete();
		$types->findByName($this->folioTypeName)->delete();
		$types->findByName($this->workTypeName)->delete();
		
		//delete the omeka elements
		$elements = get_db()->getTable('Element');
		$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->numberElementName)->delete();
		$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->transcriptionElementName)->delete();
		$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->transcriptionCssElementName)->delete();
		$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->transcriptionHelpElementName)->delete();
		$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->tableFooterElementName)->delete();
		$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->folioIncipitElementName)->delete();
		$elements->findByElementSetNameAndElementName('Item Type Metadata', $this->folioExplicitElementName)->delete();
		
		//TODO: should we delete omeka items that depend on the plugin?
		
		$this->installing = false;
	}
	
	public function hookUpgrade($args)
	{
		
	}
	//Get the id of metadata element from its name
	function getElementId($elementName)
	{
		return $this->elementTable->findByElementSetNameAndElementName('Item Type Metadata', $elementName)->id;
	}
	//init commonly used values (this is called before any invocation of the plugin)
	public function hookInitialize($args)
	{//error_log(">>>".src("icons", dirname(__FILE__)."/ckeditor/skins/office2013", "png"));
		add_translation_source(dirname(__FILE__) . '/languages');
		
		$this->itemTable = get_db()->getTable('Item');
		$this->itemTypeTable = get_db()->getTable('ItemType');
		$this->elementTable = get_db()->getTable('Element');
		$this->elementTextTable = get_db()->getTable('ElementText');
		$this->collectionTable = get_db()->getTable('Collection');
		
		$this->numberElementId = $this->getElementId($this->numberElementName);
		$this->transcriptionElementId = $this->getElementId($this->transcriptionElementName);
		
		$this->folioNumberTranslation = __("Folio Number");
		$this->folioTranscriptionTranslation = __("Folio Transcription");
		$this->transcriptionCssTranslation = __("Transcription CSS");
		$this->transcriptionHelpTranslation = __("Transcription Help");
		$this->tableFooterTranslation = __("Table Footer");
		$this->workAuthorNotesTranslation = __("Author Notes Description");
		$this->workCensoredTranslation = __("Censored Description");
		$this->folioAuthorNotesTranslation = __("Author Notes");
		$this->folioAuthorNotesTagsTranslation = __("Author Notes Tags");
		$this->folioCensoredTranslation = __("Censored");
		
		$GLOBALS['geneticTablesPlugin'] = $this;
	}
	
	//after an omeka item is saved
	public function hookAfterSaveRecord($args)
	{
		if ($this->installing)
			return;
		//check if this item is a genetic table
		$item = $args['record'];
		if (!$item->item_type_id)
			return;
		$type = get_db()->getTable('ItemType')->find($item->item_type_id);
		if ($type->name == $this->tableTypeName)
		{
			//this might be a new table creation
			require_once('db/db_input_tables.php'); handleCreateTable($this, $item);
		}
		if ($type->name == $this->workTypeName)
		{
			//this might be a new work creation
			require_once('db/db_input_works.php'); handleCreateWork($this, $item);
			//or there might be a flaubert2omeka import request
			//require_once('flaubert2omeka.php'); handleFlaubert2Omeka($this, $item);
			//or there might be requests to create new folios with uploaded images or URLs
			require_once('db/db_input_folios.php'); handleCreateFolios($this, $item);
		}
	}
	
	//add a 'genetic table' tab on table items, 'work' tab on work items and a 'transcription' tab on folio items
	public function filterAdminItemsFormTabs($tabs, $args)
	{
		//if this is request to create a new 'Genetic Table' or 'Work' item (as when invoked from views/admin/index/browse.php)
		if (array_key_exists('geneticTable', $_GET) && $_GET['geneticTable'] == 'true')
		{
			?>
			<script>
				document.addEventListener("DOMContentLoaded", function() 
				{
					//prefill the item type with 'Genetic Table'
					document.getElementById("item-type").value = "<?php echo $this->itemTypeTable->findByName($this->tableTypeName)->id; ?>";
					//add a field holding the work id of the table so the handler script know what it is (appended to the HTML form)
					var field = document.createElement("INPUT");
					field.type = "hidden";
					field.id = "workId";
					field.name = "workId";
					field.value = "<?php echo $_GET['workId']; ?>";
					document.getElementById("item-type").form.appendChild(field);
				});
			</script><?php 
		}
		else if (array_key_exists('geneticWork', $_GET) && $_GET['geneticWork'] == 'true')
		{
			?><script>//prefill the item type with 'Work'
				document.addEventListener("DOMContentLoaded", function()
					{document.getElementById("item-type").value = "<?php echo $this->itemTypeTable->findByName($this->workTypeName)->id; ?>";});</script><?php 
		}
		
		$item = $args['item'];
		//it the item type has not been set yet, don't change tabs
		if (!$item->item_type_id)
			return $tabs;
		$type = $this->itemTypeTable->find($item->item_type_id);
		//if this is a 'Work' item type, add a 'Work' tab
		if ($type->name == $this->workTypeName)
		{
			//place the editor inside the tab
			ob_start();
			include dirname(__FILE__).'/work-editor/gt_workeditor.php';
			$content = ob_get_contents();
			ob_end_clean();
			
			$tabs[__("Work")] = $content;
		}
		//if this is a 'Genetic Table' item type, add a 'Genetic Table' tab
		else if ($type->name == $this->tableTypeName)
		{
			//place the editor inside the tab
			ob_start();
			include dirname(__FILE__).'/table-editor/gt_editor.php';
			$content = ob_get_contents();
			ob_end_clean();
			
			$tabs[__("Genetic Table")] = $content;
		}
		//if this is a 'Folio' item type, add a 'Folio' tab
		else if ($type->name == $this->folioTypeName)
		{
			//place the editor inside the tab
			ob_start();
			include dirname(__FILE__).'/folio-editor/gt_folio_editor.php';
			$content = ob_get_contents();
			ob_end_clean();
			
			$tabs[__("Transcription")] = $content;
		}
		return $tabs;
	}
	
	public function hookConfigForm() {require dirname(__FILE__).'/config_form.php';}
	public function hookConfig()
	{
		set_option('gt_cke_path', $_POST['gt_cke_path']);
	}
	//for proper routing to the admin 'browse' view for genetic tables
	public function hookDefineAcl($args)
	{
 		$acl = $args['acl'];
		
 		$indexResource = new Zend_Acl_Resource('GeneticTables_Index');
 		$acl->add($indexResource);
		
 		$acl->allow(array('super', 'admin'), array('GeneticTables_Index'));
	}
	//adds a browse entry for genetic tables in the admin panel menu
	public function filterAdminNavigationMain($nav)
	{
		$nav[] = array
		(
			'label' => __('Genetic Tables'),
			'uri' => url('genetic-tables'),
			'resource' => 'GeneticTables_Index',
			'privilege' => 'browse'
		);
		return $nav;
	}
	
	function filterPublicNavigationMain($navArray)
	{
// 		array_unshift($navArray, array(
// 				'label' => __('Genetic Editions'),
// 				'uri' => url('genetic-tables')
// 		));
		return $navArray;
	}
	
	//if public viewing folios or tables, we add the viewers
	public function hookPublicItemsShow($args)
	{
		$item = $args['item'];
		if (!$item->item_type_id)
			return;
		$type = $this->itemTypeTable->find($item->item_type_id);
		if ($type->name == $this->tableTypeName)
			include(dirname(__FILE__).'/table-viewer/gt_viewer.php');
		else if ($type->name == $this->folioTypeName)
			include(dirname(__FILE__).'/folio-viewer/gt_folio_viewer.php');
		else if ($type->name == $this->workTypeName)
			include(dirname(__FILE__).'/work-viewer/gt_work_viewer.php');
	}
	
	public function hookPublicItemsBrowseEach($args)
	{
		$item = $args['item'];
		if (!$item->item_type_id)
			return;
		$type = $this->itemTypeTable->find($item->item_type_id);
		if ($type->name == $this->workTypeName)
			include(dirname(__FILE__).'/work-viewer/gt_work_previewer.php');
	}
	
	public function handleAsyncRequest()
	{
		require_once(dirname(__FILE__).'/ajax/ajax_utils.php');
		
		//if there is a data output request
		if (array_key_exists('output', $_GET)) try
		{
			//the 'output' argument gives us the type, the handling is forwarded to the relevant file
			if ($_GET['output'] === 'fragments') {include dirname(__FILE__).'/ajax/ajax_output_fragments.php'; handle($this); exit;}
			else if ($_GET['output'] === 'sources') {include dirname(__FILE__).'/ajax/ajax_output_sources.php'; handle($this); exit;}
			else if ($_GET['output'] === 'tags') {include dirname(__FILE__).'/ajax/ajax_output_tags.php'; handle($this); exit;}
			else if ($_GET['output'] === 'folios') {include dirname(__FILE__).'/ajax/ajax_output_folios.php'; handle($this); exit;}
			else if ($_GET['output'] === 'tables') {include dirname(__FILE__).'/ajax/ajax_output_tables.php'; handle($this); exit;}
			else if ($_GET['output'] === 'works') {include dirname(__FILE__).'/ajax/ajax_output_works.php'; handle($this); exit;}
		}
		catch (Exception $e)
		{
			error_log($e);
			outputErrorXML(__("Server error"));
		}
		
		//if there is a data input request
		if (array_key_exists('input', $_GET)) try
		{
			//make sure the user has the authorization to edit this table
			$itemId = array_key_exists('tableId', $_GET) ? $_GET['tableId'] : 
				(array_key_exists('sourceId', $_GET) ? $_GET['sourceId'] :
					(array_key_exists('folioId', $_GET) ? $_GET['folioId'] : 
						(array_key_exists('workId', $_GET) ? $_GET['workId'] : null)));
			//if no item was found to test privileges
			if ($itemId === null) {outputErrorXML(__("Invalid request")); exit;}
			//if unallowed
			if (!is_allowed(get_db()->getTable('Item')->find($itemId), 'edit')) {outputErrorXML(__("Permission denied")); exit;}
			
			//include the appropriate handler
			if ($_GET['input'] === 'fragments') {include dirname(__FILE__).'/ajax/ajax_input_fragments.php'; handle($this); exit;}
			else if ($_GET['input'] === 'sources') {include dirname(__FILE__).'/ajax/ajax_input_sources.php'; handle($this); exit;}
			else if ($_GET['input'] === 'tags') {include dirname(__FILE__).'/ajax/ajax_input_tags.php'; handle($this); exit;}
			else if ($_GET['input'] === 'folios') {include dirname(__FILE__).'/ajax/ajax_input_folios.php'; handle($this); exit;}
			else if ($_GET['input'] === 'tables') {include dirname(__FILE__).'/ajax/ajax_input_tables.php'; handle($this); exit;}
			else if ($_GET['input'] === 'works') {include dirname(__FILE__).'/ajax/ajax_input_works.php'; handle($this); exit;}
			else if ($_GET['input'] === 'session') {include dirname(__FILE__).'/ajax/ajax_input_session.php'; handle($this);}
		}
		catch (Exception $e)
		{
			error_log($e);
			outputErrorXML(__("Server error"));
		}
	}
}
?>
