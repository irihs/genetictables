<?php 
require_once('db_tables.php');

function createFolioFromFile($p, $work, $source, $sourceId, $num, $makePublic, $dest, $url)
{
	$prefix = $work." - ";
	$name = ((substr($source, 0, strlen($prefix)) == $prefix ? "" : $prefix).$source)." - Folio ".$num;
	//prepare Dublin Core metadata
	$dcArray = array("Title" => array(array("text" => $name, "html" => false)));
	if ($url != null)
		$dcArray["Relation"] = array(array("text" => $url, "html" => false));
	//prepare file data, if any
	$fileArray = array();
	if ($dest != null)
	{
		$fileArray["file_transfer_type"] = "Filesystem";
		$fileArray["files"] = $dest;
		$fileArray["file_ingest_options"] = "";
	}
	//create the omeka item
	return insert_item(
			array(
					"public" => $makePublic,
					"collection_id" => $sourceId,
					"item_type_name" => $p->folioTypeName),
			array(
					"Dublin Core" => $dcArray,
					'Item Type Metadata'=> array(
							$p->numberElementName => array(array("text" => "".$num, "html" => false)))),
			$fileArray
			);
}

/**
 * Create new folio items with the data in POST variables and uploaded files or image URLs
 */
function handleCreateFolios($p, $item)
{
	$folioCnt = 0;
	
	//if there are files uploaded
	if (array_key_exists('selectFolioFiles', $_FILES) && array_key_exists('tmp_name', $_FILES['selectFolioFiles']))
	{
		$work = getWorkTitle($p, $item->id);
		$source = $_POST['folioSourceName'];
		$sourceId = $_POST['folioSourceId'];
		
		$nFiles = count($_FILES['selectFolioFiles']['tmp_name']);
		//if no files are specified, some browsers will give an empty filename as first element
		if (strlen($_FILES['selectFolioFiles']['tmp_name'][0]) == 0)
			$nFiles = 0;
		//for each uploaded image
		for ($i=0;$i<$nFiles;$i++)
			//depending on what the user did, there might be holes in the index so make sure it exists
			if (array_key_exists('FolioNumber'.$i, $_POST))
		{
			$num = $_POST['FolioNumber'.$i];
			$makePublic = array_key_exists('FolioPublic'.$i, $_POST) && $_POST['FolioPublic'.$i] == 'true';
			//omeka will not accept uploaded files with PHP temporary names (because the names have no extensions) so we must move the file to one with a valid name
			//first we get the tmp file parent folder
			$sep = strripos($_FILES['selectFolioFiles']['tmp_name'][$i], "/");
			//now we create a new filename that is the same as the original
			$dest = substr($_FILES['selectFolioFiles']['tmp_name'][$i], 0, $sep)."/".$_FILES['selectFolioFiles']['name'][$i];
			//and finally try to move the tmp file
			if (move_uploaded_file($_FILES['selectFolioFiles']['tmp_name'][$i], $dest))
			{
				$prefix = $work." - ";
				$name = ((substr($source, 0, strlen($prefix)) == $prefix ? "" : $prefix).$source)." - Folio ".$num;
				//create the omeka item
				$folio = createFolioFromFile($p, $work, $source, $sourceId, $num, $makePublic, $dest, null);
			}
		}
		$folioCnt = $nFiles;
	}
	
	//if there are URLs
	if (array_key_exists('selectFolioURLs', $_POST))
	{
		$work = getWorkTitle($p, $item->id);
		$source = $_POST['folioSourceName'];
		$sourceId = $_POST['folioSourceId'];
		
		$urlList = $_POST['selectFolioURLs'];
		//tokenize the URL list
		$tok = strtok($urlList, " ");
		$cnt = $folioCnt;
		//risk of infinite loop if corrupted data, so we need a failsafe
		$err = 0;
		//iterate on the individual URLs
		while ($tok !== false && $err < 100)
		{
			if (array_key_exists('FolioNumber'.$cnt, $_POST))
			{
				$num = $_POST['FolioNumber'.$cnt];
				$makePublic = array_key_exists('FolioPublic'.$cnt, $_POST) && $_POST['FolioPublic'.$cnt] == 'true';
				$url = trim($tok);
				//make sure there is something
				if (strlen($url) == 0)
					continue;
				//generate a temporary file name for download
				$dest = sys_get_temp_dir()."/".substr($url, strripos($url, "/")+1);
				//download the image
				file_put_contents($dest, file_get_contents($url));
				//create the omeka item
				$folio = createFolioFromFile($p, $work, $source, $sourceId, $num, $makePublic, $dest, $url);
				
				//next token
				$tok = strtok(" ");
				$err = 0;
			}
			else
			{
				error_log("no FolioNumber".$cnt);
				$err++;
			}
			$cnt++;
		}
	}		
}
?>