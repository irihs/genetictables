<?php
/**
 * Functions for reading and writing sources.
 * The $p argument in the functions is the plugin object.
 */
require_once('db_tables.php');
//tests if a given source id exists
function sourceExists($p, $id)
{
	$stmt = get_db()->prepare("SELECT COUNT(*) as num FROM $p->sourceTableName WHERE id=?");
	if ($stmt->execute(array($id)))
	{
		$rs = $stmt->fetchAll();
		return $rs[0]['num'] > 0;
	}
	return false;
}
//tests if a given source name exists
function sourceNameExists($p, $name)
{
	$stmt = get_db()->prepare("SELECT COUNT(*) as num FROM ".$p->itemTable->getTableName()." i, ".$p->elementTextTable->getTableName()." e
		WHERE record_id IN (SELECT id FROM ".$p->collectionTable->getTableName().")
		AND text=?");
	if ($stmt->execute(array($name)))
	{
		$rs = $stmt->fetchAll();
		return $rs[0]['num'] > 0;
	}
	return false;
}
//work id for a given source
function getWorkIdForSource($p, $sourceId)
{
	$stmt = get_db()->prepare("SELECT workId FROM $p->sourceTableName WHERE id=?");
	$stmt->execute(array($sourceId));
	return $stmt->fetchAll()[0]['workId'];
}
//work name for a given source
function getWorkForSource($p, $sourceId)
{
	return getWorkTitle($p, getWorkIdForSource($p, $sourceId));
}
//rename a folio to adhere to a source name
function renameFolioSource($p, $folioId, $work, $name)
{
	$titleElementId = $p->elementTable->findByElementSetNameAndElementName('Dublin Core', 'Title')->id;
	$num = get_db()->query("SELECT text FROM ".$p->elementTextTable->getTableName()." WHERE record_id=".$folioId." AND element_id=$p->numberElementId")->fetchAll()[0]['text'];
	$stmt = get_db()->prepare("UPDATE ".$p->elementTextTable->getTableName()." SET text=? WHERE record_id=".$folioId." AND element_id=$titleElementId AND record_type='Collection'");
	$stmt->execute(array($work." - ".$name." - Folio ".$num));
}
//source name
function getNameForSource($p, $sourceId)
{
	$titleElementId = $p->elementTable->findByElementSetNameAndElementName('Dublin Core', 'Title')->id;
	$stmt = get_db()->prepare("SELECT text FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND element_id=$titleElementId AND record_type='Collection'");
	$stmt->execute(array($sourceId));
	return $stmt->fetchAll()[0]['text'];
}
//create a source in DB (not omeka! see ajax_input_sources.php)
function createSource($p, $id, $workId)
{
	$stmt = get_db()->prepare("INSERT INTO $p->sourceTableName (id, workId, rank, color, shortName) VALUES (?, ?, 1, 15277667, '')");
	$stmt->execute(array($id, $workId));
}
function setSourceName($p, $sourceId, $name)
{
	$titleElementId = $p->elementTable->findByElementSetNameAndElementName('Dublin Core', 'Title')->id;
	$stmt = get_db()->prepare("UPDATE ".$p->elementTextTable->getTableName()." SET text=? WHERE record_id=? AND element_id=$titleElementId AND record_type='Collection'");
	$stmt->execute(array($name, $sourceId));
}
//move the from one source to another
function moveFolios($p, $from, $to)
{
	$stmt = get_db()->prepare("UPDATE ".$p->itemTable->getTableName()." SET collection_id=? WHERE collection_id=?");
	$stmt->execute(array($to, $from));
}
// function removeSource($p, $sourceId)
// {
// 	$stmt = get_db()->prepare("DELETE FROM $p->sourceTableName WHERE id=?");
// 	$stmt->execute(array($sourceId));
// }
/**
 * Update function for source attributes
 */
function updateSourceRank($p, $sourceId, $rank)
{
	$stmt = get_db()->prepare("UPDATE $p->sourceTableName SET rank=? WHERE id=?");
	$stmt->execute(array($rank, $sourceId));
}
function updateSourceLabel($p, $sourceId, $label)
{
	$stmt = get_db()->prepare("UPDATE $p->sourceTableName SET shortName=? WHERE id=?");
	$stmt->execute(array($label, $sourceId));
}
function updateSourceColor($p, $sourceId, $color)
{
	$stmt = get_db()->prepare("UPDATE $p->sourceTableName SET color=? WHERE id=?");
	$stmt->execute(array(hexdec($color), $sourceId));
}
function getSourceFolioIds($p, $sourceId)
{
	$stmt = get_db()->prepare("SELECT id FROM ".$p->itemTable->getTableName()." WHERE collection_id=?");
	$stmt->execute(array($sourceId));
	return $stmt->fetchAll();
}
function setSourceVisibility($p, $folioId, $vis)
{
	$folioItem = $p->itemTable->find($folioId);
	$folioItem->public = $vis;
	$folioItem->save();
}
//delete a folio
function removeFolio($p, $folioId)
{
	$folioItem = $p->itemTable->find($folioId);
	$folioItem->delete();
}
//delete a source via omeka api (should cascade to DB)
function removeSource($p, $sourceId)
{
	$stmt = get_db()->prepare("DELETE FROM $p->sourceTableName WHERE id=?");
	$stmt->execute(array($sourceId));
	$p->collectionTable->find($sourceId)->delete();
}
//read array of attributes for the sources in given work
function getSourceData($p, $workId)
{
	$titleElementId = $p->elementTable->findByElementSetNameAndElementName('Dublin Core', 'Title')->id;
	$stmt = get_db()->prepare("SELECT s.id as id, s.rank as rank, s.color as color, s.shortName as shortName, et.text as name FROM $p->sourceTableName s, ".$p->elementTextTable->getTableName()." et 
			WHERE s.workId=? AND et.record_id=s.id AND et.element_id=$titleElementId AND et.record_type='Collection' ORDER BY s.rank");
	$stmt->execute(array($workId));
	return $stmt->fetchAll();
}
//read array of attributes for the sources in given table
function getSourceDataForTable($p, $tableId)
{
	return getSourceData($p, getTableWorkId($p, $tableId));
}
function getSourceTranscription($p, $sourceId, $from, $to)
{
	$stmt = get_db()->prepare("SELECT i.id as folioId, et.text as trans, et2.text as num 
		FROM ".$p->itemTable->getTableName()." i, ".$p->elementTextTable->getTableName()." et, ".$p->elementTextTable->getTableName()." et2, ".$p->itemTypeTable->getTableName()." it 
		WHERE i.collection_id=? AND i.item_type_id=it.id AND it.name='$p->folioTypeName' AND et.record_id=i.id AND et2.record_id=i.id 
		AND et.element_id=$p->transcriptionElementId AND et2.element_id=$p->numberElementId 
		AND CONVERT(et2.text, SIGNED INTEGER)>? AND CONVERT(et2.text, SIGNED INTEGER)<=? ORDER BY CONVERT(et2.text, SIGNED INTEGER)");
	$stmt->execute(array($sourceId, $from, $to));
	$res = $stmt->fetchAll();
	$trans = "";
	foreach ($res as $part)
	{
		$f = strpos($trans, "<hors-corpus>");
		while ($f !== FALSE)
		{
			$e = strpos($trans, "</hors-corpus>");
			$len = strlen($trans);
			$trans = ($f > 0 ? substr($trans, 0, $f) : "").($e+14 < $len ? substr($trans, $e+14, $len-$e-14) : "");
			$f = strpos($trans, "<hors-corpus>");
		}
		$trans = $trans.$part['trans'];
	}
	return $trans;
}
?>