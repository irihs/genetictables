<?php 
/**
 * Functions for reading and writing sources.
 * The $p argument in the functions is the plugin object.
 */
require_once('db_works.php');
require_once('db_folios.php');
//get the work id for a table
function getTableWorkId($p, $tableId)
{
	$stmt = get_db()->prepare("SELECT workId FROM $p->displayTableName WHERE tableId=?");
	$stmt->execute(array($tableId));
	return $stmt->fetchAll()[0]['workId'];
}
//work name
function getTableWorkName($p, $tableId)
{
	return getWorkTitle($p, getTableWorkId($p, $tableId));
}
function getTableTitle($p, $tableId)
{
	return metadata($p->itemTable->find($tableId), array('Dublin Core', 'Title'));
}
function getTableShortTitle($p, $tableId)
{
	$title = getTableTitle($p, $tableId);
	$prefix = metadata($p->itemTable->find(getTableWorkId($p, $tableId)), array('Dublin Core', 'Title'))." - ";
	return substr($title, 0, strlen($prefix)) === $prefix ? substr($title, strlen($prefix)) : $title;
}
//url of the first image found in the table attachments, empty string if none
function getTableImage($p, $tableId)
{
	$img = "";
	//We look at the attached files
	$fileTable = get_db()->getTable('File');
	$files = $fileTable->findByItem($tableId);
	//and stop as soon as an image is found
	for ($i=0;strlen($img) == 0 && $i < count($files);$i++)
		$img = file_display_url($files[$i], 'original');
	return $img;
}
function getTableFooter($p, $tableId)
{
	$item = $p->itemTable->find($tableId);
	$elements = $p->elementTextTable->findByRecord($item);
	$footerId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Item Type Metadata', $p->tableFooterElementName)->id;
	foreach ($elements as $k => $v)
		if ($v->element_id == $footerId)
			return $v->text;
	return "";
}
//adds/replaces the table footer
function setTableFooter($p, $tableId, $footer)
{
	$item = $p->itemTable->find($tableId);
	$elements = $p->elementTextTable->findByRecord($item);
	$footerId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Item Type Metadata', $p->tableFooterElementName)->id;
	//delete any existing footers first
	$stmt = get_db()->prepare("DELETE FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND record_type='Item' AND element_id=$footerId");
	$stmt->execute(array($tableId));
	//then add new one
	$stmt = get_db()->prepare("INSERT INTO ".$p->elementTextTable->getTableName()." (record_id, record_type, element_id, html, text) VALUES (?, 'Item', $footerId, 0, ?)");
	$stmt->execute(array($tableId, $footer));
}
//have display settings been created for this table?
function tableHasDisplay($p, $tableId)
{
	$stmt = get_db()->prepare("SELECT COUNT(*) AS cnt FROM $p->displayTableName WHERE tableId=?");
	$stmt->execute(array($tableId));
	return $stmt->fetchAll()[0]['cnt'] > 0;
}
//create display settings
function createTableDisplay($p, $tableId, $workId)
{
	$stmt = get_db()->prepare("INSERT INTO $p->displayTableName (tableId, workId, chapterDisplay, pageSpan, tableColor, textColor) VALUES (?, ?, 0, 20, 14737632, 16777215)");
	$stmt->execute(array($tableId, $workId));
}
//update display settings
function setTableDisplay($p, $tableId, $chapterDisplay, $pageSpan, $tableColor, $textColor, $incipitSourceId)
{
	$stmt = get_db()->prepare("UPDATE $p->displayTableName SET chapterDisplay=?, pageSpan=?, tableColor=?, textColor=?, incipitSourceId=? WHERE tableId=?");
	$stmt->execute(array($chapterDisplay, $pageSpan, hexdec($tableColor), hexdec($textColor), $incipitSourceId < 0 ? null : $incipitSourceId, $tableId));
}
//array of display settings
function getTableDisplay($p, $tableId)
{
	$stmt = get_db()->prepare("SELECT chapterDisplay, pageSpan, tableColor, textColor, incipitSourceId FROM $p->displayTableName WHERE tableId=?");
	$stmt->execute(array($tableId));
	return $stmt->fetchAll();
}
//get the incipit source: the source used as a reference for page numbers (folios in this source should have incipits and explicits)
function getTableIncipitSourceId($p, $tableId)
{
	$stmt = get_db()->prepare("SELECT incipitSourceId FROM $p->displayTableName WHERE tableId=?");
	$stmt->execute(array($tableId));
	$sourceId = $stmt->fetchAll();
	if (count($sourceId) == 0)
		return -1;
	return $sourceId[0]["incipitSourceId"];
}
function getTableIncipit($p, $tableId, $page)
{
	//get the incipit source
	$sourceId = getTableIncipitSourceId($p, $tableId);
	if ($sourceId === -1)
		return "";
	//get the folio in that source at the specified page
	$folioId = getFolioIdFromSource($p, $sourceId, $page);
	if ($folioId=== -1)
		return "";
	//get the folio's incipit
	return getFolioIncipit($p, $folioId);
}
function getTableExplicit($p, $tableId, $page)
{
	//get the explicit source
	$sourceId = getTableIncipitSourceId($p, $tableId);
	if ($sourceId === -1)
		return "";
	//get the folio in that source at the specified page
	$folioId = getFolioIdFromSource($p, $sourceId, $page);
	if ($folioId=== -1)
		return "";
	//get the folio's explicit
	return getFolioExplicit($p, $folioId);
}
?>