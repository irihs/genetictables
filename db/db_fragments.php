<?php 
/**
 * Functions for reading and modifying fragments attributes.
 * The $p argument in the functions is the plugin object.
 */
//create a new fragment and return the id
function createFragment($p, $folioId)
{
	$stmt = get_db()->prepare("INSERT INTO $p->fragmentTableName (folioId, fromPage, fromLine, toPage, toLine, position, withheld, parentId, previousId) VALUES (?, 1, 1, 1, 1, '', 0, NULL, NULL)");
	$stmt->execute(array($folioId));
	return get_db()->lastInsertId();
}
//update the attributes of a given fragment
function updateFragment($p, $fragmentId, $fromPage, $fromLine, $toPage, $toLine, $position, $withheld, $parentId, $previousId)
{
	$stmt = get_db()->prepare("UPDATE ".$p->fragmentTableName."
			SET fromPage=?, fromLine=?, toPage=?, toLine=?, position=?, withheld=?, parentId=?, previousId=? 
			WHERE id=?");
	$stmt->execute(array(
			$fromPage, $fromLine, $toPage, $toLine, $position, $withheld, $parentId, $previousId, 
			$fragmentId));
}
//remove the given fragment
function removeFragment($p, $fragmentId)
{
	$stmt = get_db()->prepare("DELETE FROM ".$p->fragmentTableName." WHERE id=?");
	$stmt->execute(array($fragmentId));
}
//array of tags for the given fragment
function getTagsForFragment($p, $fragmentId)
{
	$stmt = get_db()->prepare("SELECT t.tableId as tableId, t.id as tagId, t.rank as rank FROM $p->tableFragmentsTableName tf, $p->tagsTableName t WHERE tf.fragmentId=? AND t.id=tf.tagId");
	$stmt->execute(array($fragmentId));
	return $stmt->fetchAll();
}
//array of ids that reference a given fragment as "previous"
function getNextFragmentId($p, $fragId)
{
	$stmt = get_db()->prepare("SELECT id FROM $p->fragmentTableName WHERE previousId=?");
	$stmt->execute(array($fragId));
	return $stmt->fetchAll();
}
//array of ids that reference a given fragment as "parent"
function getChildrenFragmentIds($p, $fragId)
{
	$stmt = get_db()->prepare("SELECT id FROM $p->fragmentTableName WHERE parentId=?");
	$stmt->execute(array($fragId));
	return $stmt->fetchAll();
}
/**
 * Returns the attributes of a set of fragments
 * @param unknown $p Gentic Tables plugin object
 * @param unknown $folioId Id of the folio containing the fragments (can be null)
 * @param unknown $workId Id of the work containing the fragments (can be null)
 * @param unknown $from Page number at the start of a range (can be null)
 * @param unknown $to Page number at the end of a range (can be null)
 * @param unknown $fragmentIds List of fragments ids to return (can be null)
 * @return Array of attributes
 */
function getFragmentData($p, $folioId, $workId, $from, $to, $fragmentIds)
{
	$idList = $fragmentIds === null ? null : str_repeat("?,", count($fragmentIds)-1)."?";
	
	$stmt = get_db()->prepare("
			SELECT f.id as id, f.folioId as folioId, f.fromPage as fromPage, f.fromLine as fromLine, f.toPage as toPage, f.toLine as toLine, f.parentId as parentId, 
				f.previousId as previousId, f.position as position, f.withheld as withheld, s.id as sourceId, ss.rank, et.text as num, i.public as public 
			FROM $p->fragmentTableName f, ".get_db()->getTable('Collection')->getTableName()." s, $p->sourceTableName ss, ".get_db()->getTable('Item')->getTableName()." i, 
				".get_db()->getTable('ElementText')->getTableName()." et 
			WHERE ".
			($folioId === null ? "" : "f.folioId = ? AND ").
			($workId === null ? "" : "f.folioId IN (SELECT it.id FROM ".get_db()->getTable('Item')->getTableName()." it, $p->sourceTableName sst WHERE it.collection_id=sst.id AND sst.workId=?) AND ").
			($from === null ? "" : "f.toPage >= ? AND ").
			($to === null ? "" : "f.fromPage <= ? AND ").
			($fragmentIds === null ? "" : "f.id IN ($idList) AND ").
			"ss.id = s.id AND 
			i.collection_id = s.id AND 
			et.record_id = f.folioId AND 
			i.id = f.folioId AND 
			et.element_id=$p->numberElementId 
			ORDER BY ss.rank");
			
	$params = array();
	if ($folioId !== null) $params[] = $folioId;
	if ($workId !== null) $params[] = $workId;
	if ($from !== null) $params[] = $from;
	if ($to !== null) $params[] = $to;
	if ($fragmentIds !== null) $params = array_merge($params, $fragmentIds);
	
	return array($stmt, $params);
}
//the ids of the work and the source of the given fragments
function getFragmentWorkAndSourceIds($p, $fragId)
{
	$stmt = get_db()->prepare("SELECT s.workId as tid, s.id as sid FROM $p->sourceTableName s, ".$p->itemTable->getTableName()." f WHERE f.id=? AND f.collection_id=s.id");
	$stmt->execute(array($fragId));
	return $stmt->fetchAll()[0];
}
?>