<?php
/**
 * Functions for reading and modifying folio attributes. Since folios are omeka items, most queries target omeka tables. 
 * The $p argument in the functions is the plugin object. 
 */
require_once('db_sources.php');

//id of the given folio's source
function getFolioSourceId($p, $folioId)
{
	$stmt = get_db()->prepare("SELECT collection_id FROM ".$p->itemTable->getTableName()." WHERE id=?");
	$stmt->execute(array($folioId));
	$rs = $stmt->fetchAll();
	return $rs[0]['collection_id'];
}
//id of the given folio's work
function getFolioWorkId($p, $folioId)
{
	return getWorkIdForSource($p, getFolioSourceId($p, $folioId));
}
//ids of all tables associated to a given fragment through a tag
function getFolioTableIds($p, $folioId)
{
	$stmt = get_db()->prepare("SELECT DISTINCT(t.tableId) as id FROM $p->tagsTableName t, $p->fragmentTableName f, $p->tableFragmentsTableName tf 
		WHERE f.folioId=? AND f.id=tf.fragmentId AND tf.tagId=t.id");
	$stmt->execute(array($folioId));
	$rs = $stmt->fetchAll();
	return $rs;
}
//returns whether or not a folio exists within a given source with a given number
function folioNumberExistsInSource($p, $sourceId, $number)
{
	$stmt = get_db()->prepare("SELECT COUNT(fs.id) as num
			FROM ".$p->itemTable->getTableName()." fs, ".$p->elementTextTable->getTableName()." et3
				WHERE fs.collection_id=? AND et3.record_id=fs.id AND et3.element_id=".$p->numberElementId." AND et3.text=?");
	$stmt->execute(array($sourceId, $number));
	$rs = $stmt->fetchAll();
	return $rs[0]['num'] > 0;
}
//set the source of a folio
function setFolioSourceId($p, $folioId, $sourceId)
{
	$stmt = get_db()->prepare("UPDATE ".$p->itemTable->getTableName()." SET collection_id=? WHERE id=?");
	$stmt->execute(array($sourceId, $folioId));
}
//set the number of a folio
function setFolioNumber($p, $folioId, $number)
{
	$stmt = get_db()->prepare("UPDATE ".$p->elementTextTable->getTableName()." SET text=? WHERE record_id=? AND element_id=$p->numberElementId");
	$stmt->execute(array($number, $folioId));
}
//get the id of the folio with the given number within the given work id and source name (-1 if none are found) 
function getFolioId($p, $workId, $source, $number)
{
	$titleElementId = $p->elementTable->findByElementSetNameAndElementName('Dublin Core', 'Title')->id;
	$stmt = get_db()->prepare("SELECT i.id as id FROM ".$p->itemTable->getTableName()." i WHERE "
		."EXISTS (SELECT * FROM ".$p->elementTextTable->getTableName()." WHERE record_id=i.id AND element_id=".$p->numberElementId." AND text=?) AND "
		."EXISTS (SELECT s.* FROM ".$p->sourceTableName." s WHERE s.id=i.collection_id AND s.workId=? AND "
			."EXISTS (SELECT * FROM ".$p->elementTextTable->getTableName()." WHERE record_id=s.id AND element_id=".$titleElementId." AND text=?))");
	$stmt->execute(array($number, $workId, $source));
	$res = $stmt->fetchAll();
	if (count($res) > 0)
		return $res[0]['id'];
	return -1;
}
//get the id of the folio with the given number within the source with the given id (-1 if none are found)
function getFolioIdFromSource($p, $sourceId, $number)
{
	$titleElementId = $p->elementTable->findByElementSetNameAndElementName('Dublin Core', 'Title')->id;
	$stmt = get_db()->prepare("SELECT i.id as id
		FROM ".$p->itemTable->getTableName()." i
		WHERE i.collection_id=? AND "
			."EXISTS (SELECT * FROM ".$p->elementTextTable->getTableName()." WHERE record_id=i.id AND element_id=$p->numberElementId AND text=?)");
	$stmt->execute(array($sourceId, $number));
	$res = $stmt->fetchAll();
	if (count($res) > 0)
		return $res[0]['id'];
	return -1;
}
//name of the given folio
function getFolioName($p, $folioId)
{
	$titleElementId = $p->elementTable->findByElementSetNameAndElementName('Dublin Core', 'Title')->id;
	$stmt = get_db()->prepare("SELECT text FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND element_id=$titleElementId AND record_type='Item'");
	$stmt->execute(array($folioId));
	return $stmt->fetchAll()[0]['text'];
}
//miniature image url of the given folio
function getFolioMiniUrl($p, $folioId)
{
	$val = getFolioImageFormatUrl($p, $folioId, 'thumbnail');
	if (strlen($val) == 0)
		$val = getFolioImageFormatUrl($p, $folioId, 'original');
	return $val;
}
//image url of the given folio
function getFolioImageUrl($p, $folioId)
{
	return getFolioImageFormatUrl($p, $folioId, 'original');
}
//image url of the given folio for the given format
function getFolioImageFormatUrl($p, $folioId, $format)
{
	$img = "";
	//First we look at the attached files
	$fileTable = get_db()->getTable('File');
	$files = $fileTable->findByItem($folioId);
	//and stop as soon as one is found
	for ($i=0;strlen($img) == 0 && $i < count($files);$i++)
		if ($format != 'thumbnail' || $files[$i]->has_derivative_image)
			$img = file_display_url($files[$i], $format);
	//If no file was found, we look for a link in the "Relation" DC field
	if (strlen($img) == 0)
	{
		$relationId = $p->elementTable->findByElementSetNameAndElementName('Dublin Core', 'Relation')->id;
		$stmt = get_db()->prepare("SELECT text FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND element_id=?");
		if ($stmt->execute(array($folioId, $relationId)))
		{
			$rs = $stmt->fetchAll();
			if (count($rs) > 0)
				$img = $rs[0]['text'];
		}
	}
	return $img;
}
//transcription of the given folio
function getFolioTranscription($p, $folioId)
{
	$trans = "";
	//first we check for a transcription from the "Scripto" plugin
	$element = $p->elementTable->findByElementSetNameAndElementName('Scripto', 'Transcription');
	//if the plugin exists
	if ($element)
	{
		$transcriptionId = $element->id;
		$stmt = get_db()->prepare("SELECT text FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND element_id=?");
		if ($stmt->execute(array($folioId, $transcriptionId)))
		{
			$rs = $stmt->fetchAll();
			if (count($rs) > 0)
				$trans = $rs[0]['text'];
		}
	}
	//if no transcription was found, load the custom field for this plugin
	if ($trans == "")
	{
		$transcriptionId = $p->elementTable->findByElementSetNameAndElementName('Item Type Metadata', $p->transcriptionElementName)->id;
		$stmt = get_db()->prepare("SELECT text FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND element_id=?");
		if ($stmt->execute(array($folioId, $transcriptionId)))
		{
			$rs = $stmt->fetchAll();
			if (count($rs) > 0)
				$trans = $rs[0]['text'];
		}
	}
	return $trans;
}
//set the transcription of the given folio
function setFolioTranscription($p, $folioId, $trans)
{
	$transcriptionId = $p->elementTable->findByElementSetNameAndElementName('Item Type Metadata', $p->transcriptionElementName)->id;
	$stmt = get_db()->prepare("SELECT COUNT(*) AS cnt FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND element_id=?");
	$cnt = 0;
	//check if a transcription exists
	if ($stmt->execute(array($folioId, $transcriptionId)))
	{
		$rs = $stmt->fetchAll();
		$cnt = $rs[0]['cnt'];
	}
	//if not, create one
	if ($cnt == 0)
	{
		$stmt = get_db()->prepare("INSERT INTO ".$p->elementTextTable->getTableName()." (record_id, record_type, element_id, html, text) VALUES (?, 'Item', ?, 1, ?)");
		$stmt->execute(array($folioId, $transcriptionId, $trans));
	}
	//if so, update it
	else
	{
		$stmt = get_db()->prepare("UPDATE ".$p->elementTextTable->getTableName()." SET text=? WHERE record_id=? AND element_id=?");
		$stmt->execute(array($trans, $folioId, $transcriptionId));
	}
}
//add an uploaded file to the given folio
function addFolioImage($p, $folioId)
{
	foreach ($_FILES as $k => $v)
	{
		//omeka will not accept uploaded files with PHP temporary names (because the names have no extensions) so we must move the file to one with a valid name
		//first we get the tmp file parent folder
		$sep = strripos($v['tmp_name'], "/");
		//now we create a new filename that is the same as the original
		$dest = substr($v['tmp_name'], 0, $sep)."/".$v['name'];
		//and finally try to move the tmp file
		$_FILES['file']['tmp_name'] = array($v['tmp_name']);
		$_FILES['file']['type'] = array($v['type']);
		if (move_uploaded_file($v['tmp_name'], $dest))
		{
			insert_files_for_item((int)$folioId, "Filesystem", array("file" => $dest), array("file_ingest_options" => ""));
			unlink($dest);
		}
	}
}
//add an image through a link to the given folio
//the image is downloaded and added as a file attachement to the folio and the url is added as "Relation" DC metadata
function addFolioLink($p, $folioId, $url)
{
	$item = $p->itemTable->find($folioId);
	$elements = $p->elementTextTable->findByRecord($item);
	$relId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Dublin Core', 'Relation')->id;
	$stmt = get_db()->prepare("INSERT INTO ".$p->elementTextTable->getTableName()." (record_id, record_type, element_id, html, text) VALUES (?, 'Item', $relId, 0, ?)");
	$stmt->execute(array($folioId, $url));
	
	//generate a temporary file name for download
	$dest = sys_get_temp_dir()."/".substr($url, strripos($url, "/")+1).".jpg";
	if (substr($url, 0, 4) !== "http")
		$url = "http://".$url;
	//download the image
	file_put_contents($dest, file_get_contents($url));
	insert_files_for_item((int)$folioId, "Filesystem", array("file" => $dest), array("file_ingest_options" => ""));
	unlink($dest);
}
/**
 * Get the attributes of a set of folios within the given work and source
 * @param unknown $p Genetic Tables plugin object
 * @param unknown $workId Id of the work containing the folios
 * @param unknown $sourceId Id of the source containing the folios (null means all sources)
 * @param unknown $folioId Id of the fragment (null for all folios)
 * @return An array of attributes
 */
function getFolioData($p, $workId, $sourceId, $folioId)
{
	$stmt = get_db()->prepare("SELECT i.id as id, et3.text as number, s.id as sourceId
			FROM ".$p->itemTable->getTableName()." i,
			$p->sourceTableName s,
			".$p->collectionTable->getTableName()." fs,
			".$p->itemTypeTable->getTableName()." it,
			".$p->elementTextTable->getTableName()." et3
			WHERE s.workId=? AND s.id=fs.id AND i.collection_id=s.id "
			.($folioId === null ? "" : "AND i.id=? ")
			.($sourceId === null ? "" : "AND s.id=? ")
			."AND it.name='$p->folioTypeName' AND it.id=i.item_type_id
			AND et3.record_id=i.id AND et3.element_id=? AND et3.record_type='Item'
			ORDER BY et3.text");
	$params = array($workId);
	if ($folioId !== null) $params[] = $folioId;
	if ($sourceId !== null) $params[] = $sourceId;
	$params[] = $p->numberElementId;
	$stmt->execute($params);
	return $stmt->fetchAll();
}
//incipit of the given folio
function getFolioIncipit($p, $folioId)
{
	$id = $p->elementTable->findByElementSetNameAndElementName('Item Type Metadata', $p->folioIncipitElementName)->id;
	$stmt = get_db()->prepare("SELECT text FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND element_id=?");
	if ($stmt->execute(array($folioId, $id)))
	{
		$rs = $stmt->fetchAll();
		if (count($rs) > 0)
			return $rs[0]['text'];
	}
	return "";
}
//explicit of the given folio
function getFolioExplicit($p, $folioId)
{
	$id = $p->elementTable->findByElementSetNameAndElementName('Item Type Metadata', $p->folioExplicitElementName)->id;
	$stmt = get_db()->prepare("SELECT text FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND element_id=?");
	if ($stmt->execute(array($folioId, $id)))
	{
		$rs = $stmt->fetchAll();
		if (count($rs) > 0)
			return $rs[0]['text'];
	}
	return "";
}
?>