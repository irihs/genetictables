<?php 
/**
 * Functions for reading and writing works.
 * The $p argument in the functions is the plugin object.
 */
require_once('db_tables.php');
require_once('db_sources.php');
//get ids for all works
function getWorks($p)
{
	$workTypeId = $p->itemTypeTable->findByName($p->workTypeName)->id;
	$stmt = get_db()->prepare("SELECT id FROM ".$p->itemTable->getTableName()." WHERE item_type_id=$workTypeId");
	$stmt->execute(array());
	return $stmt->fetchAll();
}
function getWorkTitle($p, $workId)
{
	$titleElementId = $p->elementTable->findByElementSetNameAndElementName('Dublin Core', 'Title')->id;
	$stmt = get_db()->prepare("SELECT text FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND element_id=$titleElementId AND record_type='Item'");
	$stmt->execute(array($workId));
	return $stmt->fetchAll()[0]['text'];
}
//get all tables referencing a given work
function getWorkTables($p, $workId)
{
	$stmt = get_db()->prepare("SELECT tableId as id FROM $p->displayTableName WHERE workId=?");
	$stmt->execute(array($workId));
	return $stmt->fetchAll();
}
//create chapter and return id
function createChapter($p, $workId, $name)
{
	$stmt = get_db()->prepare("INSERT INTO $p->chaptersTableName (workId, toPage, name) VALUES (?, 1, ?)");
	$stmt->execute(array($workId, $name));
	return get_db()->lastInsertId();
}
//chapter attributes for all chapters in work
function getChapterData($p, $workId)
{
	$stmt = get_db()->prepare("SELECT id as id, toPage as toPage, name as name FROM $p->chaptersTableName WHERE workId=? ORDER BY toPage");
	$stmt->execute(array($workId));
	return $stmt->fetchAll();
}
//attributes for the chapter on given page
function getChapterDataForPage($p, $workId, $page)
{
	$stmt = get_db()->prepare("SELECT id as id, toPage as toPage, name as name FROM $p->chaptersTableName 
		WHERE id=(SELECT MIN(id) FROM $p->chaptersTableName WHERE workId=? AND toPage>=?)");
	$stmt->execute(array($workId, $page));
	return $stmt->fetchAll();
}
//update chapter attributes
function updateChapter($p, $workId, $chapterId, $name, $toPage)
{
	$stmt = get_db()->prepare("UPDATE $p->chaptersTableName SET name=?, toPage=? WHERE id=?");
	$stmt->execute(array($name, $toPage, $chapterId));
}
//get css for transcriptions, empty string if none
function getWorkTranscriptionCss($p, $workId)
{
	$item = $p->itemTable->find($workId);
	$elements = $p->elementTextTable->findByRecord($item);
	$cssId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Item Type Metadata', $p->transcriptionCssElementName)->id;
	foreach ($elements as $k => $v)
		if ($v->element_id == $cssId)
			return $v->text;
	return "";
}
//adds/replaces css for transcriptions
function setWorkTranscriptionCss($p, $workId, $css)
{
	$item = $p->itemTable->find($workId);
	$elements = $p->elementTextTable->findByRecord($item);
	$cssId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Item Type Metadata', $p->transcriptionCssElementName)->id;
	//delete first
	$stmt = get_db()->prepare("DELETE FROM ".$p->elementTextTable->getWorkName()." WHERE record_id=? AND record_type='Item' AND element_id=$cssId");
	$stmt->execute(array($workId));
	//then add
	$stmt = get_db()->prepare("INSERT INTO ".$p->elementTextTable->getWorkName()." (record_id, record_type, element_id, html, text) VALUES (?, 'Item', $cssId, 0, ?)");
	$stmt->execute(array($workId, $css));
}
//get help message for transcriptions, empty string if none
function getWorkTranscriptionHelp($p, $workId)
{
	$item = $p->itemTable->find($workId);
	$elements = $p->elementTextTable->findByRecord($item);
	$helpId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Item Type Metadata', $p->transcriptionHelpElementName)->id;
	foreach ($elements as $k => $v)
		if ($v->element_id == $helpId)
			return $v->text;
	return "";
}
//adds/replaces help for transcriptions
function setWorkTranscriptionHelp($p, $workId, $help)
{
	$item = $p->itemTable->find($workId);
	$elements = $p->elementTextTable->findByRecord($item);
	$helpId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Item Type Metadata', $p->transcriptionHelpElementName)->id;
	//delete first
	$stmt = get_db()->prepare("DELETE FROM ".$p->elementTextTable->getTableName()." WHERE record_id=? AND record_type='Item' AND element_id=$helpId");
	$stmt->execute(array($workId));
	//then add
	$stmt = get_db()->prepare("INSERT INTO ".$p->elementTextTable->getTableName()." (record_id, record_type, element_id, html, text) VALUES (?, 'Item', $helpId, 0, ?)");
	$stmt->execute(array($workId, $help));
}
//url of the first image found in the work attachments, empty string if none
function getWorkImage($p, $workId)
{
	$img = "";
	//We look at the attached files
	$fileTable = get_db()->getTable('File');
	$files = $fileTable->findByItem($workId);
	//and stop as soon as an image is found
	for ($i=0;strlen($img) == 0 && $i < count($files);$i++)
		$img = file_display_url($files[$i], 'original');
	return $img;
}
//create a new transcription code at highest rank (shown last) and return id
function createCode($p, $workId, $name)
{
	$stmt = get_db()->prepare("SELECT MAX(rank) AS rank FROM $p->codeTableName WHERE workId=?");
	$stmt->execute(array($workId));
	$res = $stmt->fetchAll();
	$rank = count($res) > 0 && $res[0]['rank'] !== null && $res[0]['rank'] !=='' ? $res[0]['rank']+1 : 0;
	$stmt = get_db()->prepare("INSERT INTO $p->codeTableName (workId, name, style, rank) VALUES (?, ?, '', $rank)");
	$stmt->execute(array($workId, $name));
}
//transcription code attributes
function getCodeData($p, $workId)
{
	$stmt = get_db()->prepare("SELECT id as id, name as name, style as style, rank as rank FROM $p->codeTableName WHERE workId=? ORDER BY rank");
	$stmt->execute(array($workId));
	return $stmt->fetchAll();
}
//update transcription code attributes
function updateCode($p, $workId, $codeId, $name, $style, $rank)
{
	$stmt = get_db()->prepare("UPDATE $p->codeTableName SET name=?, style=?, rank=? WHERE id=?");
	$stmt->execute(array($name, $style, $rank, $codeId));
}
//does the work have any author notes
function workHasAuthorNotes($p, $workId)
{
	$notesId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Item Type Metadata', $p->folioAuthorNotesElementName)->id;
	$stmt = get_db()->prepare("SELECT COUNT(*) as cnt FROM ".$p->itemTable->getTableName()." i, $p->sourceTableName s WHERE i.collection_id=s.id AND s.workId=? AND EXISTS (
			SELECT * FROM ".$p->elementTextTable->getTableName()." WHERE record_id=i.id AND element_id=$notesId)");
	$stmt->execute(array($workId));
	return $stmt->fetchAll()[0]['cnt'] > 0;
}
//all the folios in a work with non empty author notes
function getWorkFoliosWithAuthorNotes($p, $workId)
{
	$notesId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Item Type Metadata', $p->folioAuthorNotesElementName)->id;
	$stmt = get_db()->prepare("SELECT i.id as id FROM ".$p->itemTable->getTableName()." i, $p->sourceTableName s WHERE i.collection_id=s.id AND s.workId=? AND EXISTS (
		SELECT * FROM ".$p->elementTextTable->getTableName()." WHERE record_id=i.id AND element_id=$notesId)");
	$stmt->execute(array($workId));
	return $stmt->fetchAll();
}
//does the work have any censored parts
function workHasCensorship($p, $workId)
{
	$censoredId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Item Type Metadata', $p->folioCensoredElementName)->id;
	$stmt = get_db()->prepare("SELECT COUNT(*) as cnt FROM ".$p->itemTable->getTableName()." i, $p->sourceTableName s WHERE i.collection_id=s.id AND s.workId=? AND EXISTS (
		SELECT * FROM ".$p->elementTextTable->getTableName()." WHERE record_id=i.id AND element_id=$censoredId)");
	$stmt->execute(array($workId));
	return $stmt->fetchAll()[0]['cnt'] > 0;
}
//all the folios in a work with censored parts
function getWorkFoliosWithCensorship($p, $workId)
{
	$censoredId = get_db()->getTable('Element')->findByElementSetNameAndElementName('Item Type Metadata', $p->folioCensoredElementName)->id;
	$stmt = get_db()->prepare("SELECT i.id as id FROM ".$p->itemTable->getTableName()." i, $p->sourceTableName s WHERE i.collection_id=s.id AND s.workId=? AND EXISTS (
			SELECT * FROM ".$p->elementTextTable->getTableName()." WHERE record_id=i.id AND element_id=$censoredId)");
	$stmt->execute(array($workId));
	return $stmt->fetchAll();
}
function getWorkFullTranscriptionSource($p, $workId)
{
	$tables = getWorkTables($p, $workId);
	$sourceId = null;
	foreach ($tables as $table)
	{
		$sourceId = getTableIncipitSourceId($p, $table['id']);
		if ($sourceId != null)
			return $sourceId;
	}
	return null;
}
function workHasFullTranscription($p, $workId)
{
	return getWorkFullTranscriptionSource($p, $workId) != null;
}
function getWorkFullTranscription($p, $workId, $from, $to)
{
	$sourceId = getWorkFullTranscriptionSource($p, $workId);
	if ($sourceId == null)
		return "";
	return getSourceTranscription($p, $sourceId, $from, $to);
}
?>