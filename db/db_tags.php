<?php 
/**
 * Functions for reading and writing tags (groups in UI).
 * The $p argument in the functions is the plugin object.
 */
function tagNameExists($p, $tableId, $name)
{
	$stmt = get_db()->prepare("SELECT COUNT(*) as num FROM $p->tagsTableName 
		WHERE tableId=? AND name=?");
	if ($stmt->execute(array($tableId, $name)))
	{
		$rs = $stmt->fetchAll();
		return $rs[0]['num'] > 0;
	}
	return false;
}
//get tag id from name in a given table (null if none)
function getTagIdForName($p, $tableId, $name)
{
	$stmt = get_db()->prepare("SELECT id FROM $p->tagsTableName WHERE tableId=? AND name=?");
	$stmt->execute(array($tableId, $name));
	$res = $stmt->fetchAll();
	if (count($res) < 1)
		return null;
	return $res[0]['id'];
}
//which table is a given tag in
function getTableIdForTag($p, $tagId)
{
	$stmt = get_db()->prepare("SELECT tableId FROM $p->tagsTableName WHERE id=?");
	$stmt->execute(array($tagId));
	return $stmt->fetchAll()[0]['tableId'];
}
function getNameForTag($p, $tagId)
{
	$stmt = get_db()->prepare("SELECT name FROM $p->tagsTableName WHERE id=?");
	$stmt->execute(array($tagId));
	return $stmt->fetchAll()[0]['name'];
}
//create a new tag and return its id
function createTag($p, $tableId)
{
	$stmt = get_db()->prepare("INSERT INTO $p->tagsTableName (tableId, rank, color, shortName, name) VALUES (?, 1, 15277667, '', '')");
	$stmt->execute(array($tableId));
	return get_db()->lastInsertId();
}
function removeTag($p, $tagId)
{
	$stmt = get_db()->prepare("DELETE FROM $p->tagsTableName WHERE id=?");
	$stmt->execute(array($tagId));
}
/**
 * attribute update functions for tags
 */
function setTagName($p, $tagId, $name)
{
	$stmt = get_db()->prepare("UPDATE $p->tagsTableName SET name=? WHERE id=?");
	$stmt->execute(array($name, $tagId));
}
function updateTagRank($p, $tagId, $rank)
{
	$stmt = get_db()->prepare("UPDATE $p->tagsTableName SET rank=? WHERE id=?");
	$stmt->execute(array($rank, $tagId));
}
function updateTagLabel($p, $tagId, $label)
{
	$stmt = get_db()->prepare("UPDATE $p->tagsTableName SET shortName=? WHERE id=?");
	$stmt->execute(array($label, $tagId));
}
function updateTagColor($p, $tagId, $color)
{
	$stmt = get_db()->prepare("UPDATE $p->tagsTableName SET color=? WHERE id=?");
	$stmt->execute(array(hexdec($color), $tagId));
}
//create association
function addTagFragment($p, $tagId, $fragmentId)
{
	$stmt = get_db()->prepare("INSERT INTO $p->tableFragmentsTableName (tagId, fragmentId) VALUES (?, ?)");
	$stmt->execute(array($tagId, $fragmentId));
}
//delete association
function removeTagFragment($p, $tagId, $fragmentId)
{
	$stmt = get_db()->prepare("DELETE FROM $p->tableFragmentsTableName WHERE tagId=? AND fragmentId=?");
	$stmt->execute(array($tagId, $fragmentId));
}
//all ids of fragments associated to this tag
function getTagFragmentIds($p, $tagId)
{
	$stmt = get_db()->prepare("SELECT fragmentId as id FROM $p->tableFragmentsTableName WHERE tagId=?");
	$stmt->execute(array($tagId));
	return $stmt->fetchAll();
}
//array of attributes for all tags in the table
function getTagData($p, $tableId)
{
	$stmt = get_db()->prepare("SELECT id, rank, color, shortName, name FROM $p->tagsTableName 
			WHERE tableId=? ORDER BY rank");
	$stmt->execute(array($tableId));
	return $stmt->fetchAll();
}
?>