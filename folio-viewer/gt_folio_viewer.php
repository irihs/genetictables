<?php
/**
 * Folio public page. This is appended to the folio public page (or prepended with the Bovary theme)
 */
require_once(dirname(__FILE__).'/../db/db_tables.php');
require_once(dirname(__FILE__).'/../db/db_folios.php');

//Retreive the image for this folio
$img = getFolioImageUrl($this, $item->id);
//Retrieve the transcription for this folio
$trans = getFolioTranscription($this, $item->id);
?>
<style id="transcriptionStyle">
	<?php echo getWorkTranscriptionCss($this, getFolioWorkId($this, $item->id)); ?>
</style>
<!-- the transcription is hidden in this div for easy JS manipulation -->
<div id="transcription" style="display: none">
	<div class="transcription-block"><?php echo $trans; ?></div>
</div>

<?php
require_once('gt_folio_viewer_navigator.php');
?>
<div id="gt_navigation_panel" style="width: 100%"></div>
<script>
//the navigator shows the folio name, previous and next in source buttons and graph view of fragment relationships (initially hidden)
buildNavigationPanel(document.getElementById("gt_navigation_panel"));
</script>

<?php 
require_once(dirname(__FILE__).'/../dual-viewer/gt_dual_viewer.php');
?>
<!-- The digitization/transcription viewer -->
<div id="gt_dual_viewer_div" style="width: 100%; height: 64em; border: 1px solid darkgray">
</div>

<?php
//if an image is available, offer a download link
if ($img != "")
{
	?>
	<a href="<?php echo $img; ?>" download><?php echo __("Download image"); ?></a>
	<?php 
}
?>

<?php
//read the transcription help text and, if present, put in a hideable area
$help = getWorkTranscriptionHelp($this, getFolioWorkId($this, $item->id));
if ($help != "")
{
	?>
	<br/>
	<div style="border: none; text-align: center; width: 100%">
		<button id="toggleTranscriptionHelp" onClick="doToggleTranscriptionHelp();"></button>
		<div id="transcriptionHelp" style="border: 1px solid gray; display: none">
			<?php echo $help; ?>
		</div>
	</div>
	<script>

	//remove borders and restore default alignments (used to view transcriptions while avoiding omeka styling that can mess up the layout)
	function cleanStyles(element)
	{
		if (element.tagName === "TD")
		{
			element.style.border = "none";
			element.style.verticalAlign = "baseline";
		}
		var children = element.children;
		//recursive call to children
		for (var i=0;i<children.length;i++)
			cleanStyles(children[i]);
	}
	cleanStyles(document.getElementById("transcription"));

	//handle the toggle icon label
	var isTranscriptionHelpToggled = false;
	function getToggleTranscriptionHelpLabel() {return "<?php echo __("Transcription indications"); ?> "+(!isTranscriptionHelpToggled ? "&#9662;" : "&#9652;"); }
	document.getElementById("toggleTranscriptionHelp").innerHTML = getToggleTranscriptionHelpLabel();
	//trigger a transcription help toggle
	function doToggleTranscriptionHelp()
	{
		if (isTranscriptionHelpToggled)
			document.getElementById("transcriptionHelp").style.display = "none";
		else document.getElementById("transcriptionHelp").style.display = "block";
		isTranscriptionHelpToggled = !isTranscriptionHelpToggled;
		document.getElementById("toggleTranscriptionHelp").innerHTML = getToggleTranscriptionHelpLabel();
	}
	</script>
	<?php 
}
?>

<script>
	"use strict";
	//override theme style rules on tables to keep the transcription as intended

	//create the viewer area, the viewer itself, and set the sources (image, transcription and style)
	var viewerArea = document.getElementById("gt_dual_viewer_div");
	var viewer = new GTDualViewer(viewerArea);
	viewer.setSrc('<?php echo $img; ?>', document.getElementById("transcription").innerHTML, document.getElementById("transcriptionStyle").innerHTML);
</script>
