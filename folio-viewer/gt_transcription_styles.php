<style>
s { font-style:strike; }  .biffure { background-color: #ffffcc; } .censure { background-color: #C9D3B4; } 
.crayon { color:#A49F9D; } .incertain { background-color:#E2E1E0; } .ital-noir { font-style:italic; } .bleu-droit { color:#3333cc; } 
.note-regie { color: #940000; } .relecture { background-color: #FFCC99; }
hors-corpus { width:100% }

div.transcription-block { font-family: Times New Roman, Times, serif; }
div.transcription-block td { font-family: Times New Roman, Times, serif; border: none; vertical-align: top; line-height: 200%}
div.transcription-block tr { vertical-align: top; padding:0px 5px;} 
div.transcription-block table { border: none; padding:0px; }
div.transcription-block i { font-style:italic; color:#3333cc; }

div#P { font-family: Times New Roman, Times, serif; font-size:13px; line-height:15px; }
div#P td { font-family: Times New Roman, Times, serif; font-size:13px; line-height:15px;  }
div#P tr { vertical-align:top; padding:0px 5px;}  
div#P td { vertical-align:top; padding:0px 5px;} 
div#P table { border:0px; padding:0px; width:1240px; }
div#P i { font-style:italic; color:#3333cc; }

div#F { font-family: Times New Roman, Times, serif; font-size:13px; line-height:15px; }
div#F td { font-family: Times New Roman, Times, serif; font-size:13px; line-height:15px; }
div#F tr { vertical-align: top; padding:0px 5px; } 
div#F td { vertical-align: top; padding:0px 5px; } 
div#F table { border:0px; padding:0px; width:760px; }
div#F i { font-style:italic; color:#000; }

.sep { font-family: arial; font-size:11px; line-height:15px; padding:0px 5px; background-color:#adb5e9; width:760px; height:17px; text-align:left; vertical-align:top; }
.sep-d { font-family: arial; font-size:11px; line-height:15px; padding:0px 5px; background-color:#adb5e9; height:17px; text-align:right; vertical-align:top; }
.sep-l { font-family: arial; font-size:11px; line-height:15px; padding:0px 5px; background-color:#adb5e9; height:17px; text-align:left; vertical-align:top; }
.sep-fin { background-color:#adb5e9; width:760px; height:2px; line-height: 2px; font-size: 2px; text-align:center; margin:5px 0px 0px 0px; }
.jaune { background-color:#ebd223 }

#cartouche { color:#000; margin:10px 0px 15px 0px; background-color:#F5E6AB;text-align:left;
	font-weight:normal; padding:5px 20px; position:relative; font-size:12px; width:760px }
</style>