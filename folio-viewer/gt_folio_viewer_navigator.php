<?php
/**
 * Displays a navigation panel and a toggable display of the relationships of a fragment
 */
require_once(dirname(__FILE__).'/../ajax/ajax_queries.php'); 
require_once(dirname(__FILE__).'/../db/db_folios.php');
require_once(dirname(__FILE__).'/../db/db_fragments.php');
require_once(dirname(__FILE__).'/../db/db_works.php');

//Grab the table and source ids for this fragment ($item)
$tsids = getFragmentWorkAndSourceIds($this, $item->id);
$workId = $tsids['tid'];
$sourceId = $tsids['sid'];
$tableIds = getFolioTableIds($this, $item->id);
?>
<script>
"use strict";

//info for current folio
var folioName = "<?php echo getFolioName($this, $item->id); ?>";
var folioId = <?php echo $item->id; ?>;
var sourceId = <?php echo ($sourceId === null ? "null" : $sourceId); ?>;
var workId = <?php echo ($workId === null ? "null" : $workId); ?>;
var workName = "<?php echo ($workId === null ? "null" : getWorkTitle($this, $workId)); ?>";
var sourceName = "<?php echo ($sourceId === null ? "null" : getNameForSource($this, $sourceId)); ?>";

function shortFolioName(name)
{
// 	if (name.startsWith(workName+" - "))
// 		name = name.substring((workName+" - ").length);
	if (name.startsWith(sourceName+" - "))
		name = name.substring((sourceName+" - ").length);
	return name;
}

//appends a nav panel to parent
function buildNavigationPanel(parent)
{
	var table = document.createElement("TABLE");
	table.style.cssText = "width: 100%";
	parent.appendChild(table);
	table.id = "folioNav";

	var tr = document.createElement("TR");
	table.appendChild(tr);
	tr.style.cssText = "";
	tr.innerHTML = "<td style='border: none; text-align: center; width: 100%' colspan=5>"
		<?php $cnt = 0; foreach ($tableIds as $tableId) { ?>
			+"<a id='gtLink<?php echo $cnt; ?>' href='<?php echo public_url("items/show")."/".$tableId['id']; ?>'><h4>"
				+"<?php echo __("Genetic Table"); ?> &#8617;"
			+"</h4></a><br>"
		<?php $cnt++;} ?>
		+"</td>";

	tr = document.createElement("TR");
	table.appendChild(tr);
	tr.style.cssText = "";

	//prev folio label
	var td = document.createElement("TD");
	td.style.cssText = "visibility: hidden; width: 33%; text-align: center; border: 0px none black";
	tr.appendChild(td);
	td.id = "prevFolio";
	td.innerHTML = "";
	//prev folio button
	td = document.createElement("TD");
	td.style.cssText = "visibility: hidden; border: 0px none black";
	tr.appendChild(td);
	td.id = "prevButton";
	td.innerHTML = "<button><b>&lt;</b></button>";
	//current folio label
	td = document.createElement("TD");
	td.style.cssText = "width: 33%; text-align: center";
	tr.appendChild(td);
	td.innerHTML = "<h1 style='text-transform: none'>"+folioName+"</h1>";
	//next folio button
	td = document.createElement("TD");
	td.style.cssText = "visibility: hidden; border: 0px none black";
	tr.appendChild(td);
	td.id = "nextButton";
	td.innerHTML = "<button><b>&gt;</b></button>";
	//next folio label
	td = document.createElement("TD");
	td.style.cssText = "visibility: hidden; width: 33%; text-align: center; border: 0px none black";
	tr.appendChild(td);
	td.id = "nextFolio";
	td.innerHTML = "";

	//relationship display toggle
	tr = document.createElement("TR");
	table.appendChild(tr);
	tr.style.cssText = "";
	tr.innerHTML = "<td style='border: none; text-align: center; width: 100%' colspan=5><button id='exploreButton' onclick='toggleExplorePanel();'><?php echo __("View fragments"); ?> &#9662;</button></td>";

	//display area (initially hidden)
	var div = document.createElement("DIV");
	div.style.cssText = "margin: 3%; margin-top : 0px; width: 94%; display: none; background-color: ghostwhite; position: relative; border: none; padding: 0px";
	parent.appendChild(div);
	//table for positioning display elemnts
	table = document.createElement("TABLE");
	table.id = "fragNavTable";
	table.style.cssText = "margin: 0px; border: none; padding: 0px; width: 100%";
	div.appendChild(table);
	//canvas that overlaps fragNavTable and on which are drawn the arrows
	var canvas = document.createElement("CANVAS");
	div.appendChild(canvas);
	canvas.id = "fragNavCanvas";
	canvas.style.cssText = "z-index: 10; position: absolute; width: 100%; height: 100%; left: 0px; top: 0px; pointer-events: none";
}

//fill the folio labels for the prev and next buttons
if (workId !== null && sourceId !== null)
	//get the list of folios for the source
	readFolios(workId, sourceId, 
		function(folios)
		{
			//look for current folio
			var index = -1;
			for (var i=0;i<folios.length;i++)
			{
				if (folios[i].id == <?php echo $item->id; ?>)
					{index = i; break;}
			}
			if (index < 0)
				return;

			//set the table link to center on the first fragment of this folio
			readFragmentsForFolio(workId, folios[index].id, function(fragments)
			{
				if (fragments.length > 0)
				{
					<?php $cnt = 0; foreach ($tableIds as $tableId) { ?>
					document.getElementById("gtLink<?php echo $cnt; ?>").href = "<?php echo public_url("items/show")."/".$tableId['id']; ?>?fromPage="+Math.max(1, fragments[0].fromPage-2);
					<?php $cnt++;} ?>
				}
			});
			
			//is there a prev folio?
			if (index > 0)
			{
				readFolioName(folios[index-1].id, function(name)
				{
					//set label value and button link
					var label = document.getElementById("prevFolio");
					label.innerHTML = "<h3>"+shortFolioName(name)+"</h3>";
					label.style.visibility = "visible";
					var button = document.getElementById("prevButton");
					button.children[0].onclick = function() {window.location = "<?php echo public_url("items/show"); ?>/"+folios[index-1].id+"#folioNav"};
					button.style.visibility = "visible";
					
				});
			}
			//is there a next folio?
			if (index < folios.length-1)
			{
				readFolioName(folios[index+1].id, function(name)
				{
					//set label value and button link
					var label = document.getElementById("nextFolio");
					label.innerHTML = "<h3>"+shortFolioName(name)+"</h3>";
					label.style.visibility = "visible";
					var button = document.getElementById("nextButton");
					button.children[0].onclick = function() {window.location = "<?php echo public_url("items/show"); ?>/"+folios[index+1].id+"#folioNav"};
					button.style.visibility = "visible";
				});
			}
		});

/** 
 * Fragment relationship display area
 */
var fontSize = "70%";
var fragsAreShowing = false;
var fragsAreLoaded = false;
var folioFrags = null;

function toggleExplorePanel()
{
	//whether to show or hide the area (and set the button arrow)
	if (!fragsAreShowing)
	{
		var table = document.getElementById("fragNavTable");
		//table.style.display = "table";
		table.parentElement.style.display = "block";
		document.getElementById("exploreButton").innerHTML = "<?php echo __("View fragments"); ?> &#9652;";
		fragsAreShowing = true;
	}
	else if (fragsAreShowing)
	{
		var table = document.getElementById("fragNavTable");
		//table.style.display = "none";
		table.parentElement.style.display = "none";
		document.getElementById("exploreButton").innerHTML = "<?php echo __("View fragments"); ?> &#9662;";
		fragsAreShowing = false;
	}

	//lazy init of relationship data
	if (!fragsAreLoaded)
	{
		readFragmentsForFolio(workId, folioId, onFolioFragsRead);
		fragsAreLoaded = true;
	}
}
//returns standard frag info string
function buildFragLabel(frag)
{
	return "<?php echo __("Fragment"); ?>"+(frag.position !== undefined && frag.position.length > 0 ? " "+frag.position : "")+
		" <?php echo __("p."); ?>"+frag.fromPage+" <?php echo __("l."); ?>"+frag.fromLine+
		" - <?php echo __("p."); ?>"+frag.toPage+" <?php echo __("l."); ?>"+frag.toLine;
}
//builds a cell that will hold frag boxes
function buildFragCell(row, span)
{
	var td = document.createElement("TD");
	td.style.cssText = "width: "+(33*span)+"%; text-align: center; border: none";
	td.colSpan = span;
	row.appendChild(td);
	return td;
}
//callback for readFragmentsForFolio, triggers all the queries for the fragments
function onFolioFragsRead(frags)
{
	folioFrags = frags;
	
	var top = document.getElementById("fragNavTable");
	//layouts are stacked if more than one
	for (var i=0;i<folioFrags.length;i++)
	{
		var frag = folioFrags[i];

		/**
		 * Fragment display layout:
		 *  _______________________
		 * |                       |
		 * |   Child1 Child2 ...   |
		 * |_______________________|
		 * |      |         |      |
		 * | Prev | Current | Next |
		 * |______|_________|______|
		 * |                       |
		 * |         Parent        |
		 * |_______________________|
		 */
		var tr = document.createElement("TR");
		top.appendChild(tr);
		var childCell = buildFragCell(tr, 3);
		if (i > 0)
			tr.style.borderTop = "1px solid gray";

		tr = document.createElement("TR");
		top.appendChild(tr);
		var prevCell = buildFragCell(tr, 1);
		var curCell = buildFragCell(tr, 1);
		curCell.style.height = "15em";
		var nextCell = buildFragCell(tr, 1);

		tr = document.createElement("TR");
		top.appendChild(tr);
		var parentCell = buildFragCell(tr, 3);
		
		frag.curBox = document.createElement("SPAN");
		curCell.appendChild(frag.curBox);
		frag.curBox.style.cssText = "border: 1px solid black; padding: 2em; background-color: white";
		frag.curBox.innerHTML = ""//"<a href='<?php echo public_url("items/show")."/".$workId; ?>?fromPage="+Math.max(1, frag.fromPage-2)+"&nPages=5'>"
			+buildFragLabel(folioFrags[i])
			//+"</a>"
			;

		frag.prevBoxes = [];
		frag.parentBoxes = [];
		frag.nextBoxes = [];
		frag.childBoxes = [];

		//prev and parent relationships are stored in a fragment so we can query their properties directly and send them to buildOnFragCellResult for display
		if (frag.previousId !== null)
			readFragment(frag.previousId, buildOnFragCellResult(prevCell, frag, "prevBoxes"));
		if (frag.parentId !== null)
			readFragment(frag.parentId, buildOnFragCellResult(parentCell, frag, "parentBoxes"));

		//next and child relationships must be computed first, their properties are queried in the buildOnFragIdCellResult
		readFragmentNextId(frag.id, buildOnFragIdCellResult(nextCell, frag, "nextBoxes"));
		readFragmentChildrenIds(frag.id, buildOnFragIdCellResult(childCell, frag, "childBoxes"));
	}
}

//builds a callback for fragment id results
function buildOnFragIdCellResult(cell, curFrag, member)
{
	return function(res)
	{
		//for each fragment (0-1 for 'next', 0-n for 'children')
		var ids = res.split(" ");
		for (var i=0;i<ids.length;i++)
			if (ids[i].length > 0)
		{
			//query its properties
			var id = parseInt(ids[i]);
			if (!isNaN(id))
				readFragment(id, buildOnFragCellResult(cell, curFrag, member));
		}
	};
}
//builds a callback for fragment properties results
function buildOnFragCellResult(cell, curFrag, member)
{
	return function(frags)
	{
		for (var i=0;i<frags.length;i++)
			addFragToCell(frags[i], cell, curFrag, member);
	};
}
/**
 * Adds a fragment display box
 * frag: the fragment to display
 * cell: the html element to populate
 * curFrag: the current frag (center of layout)
 * member: the name of the set of boxes which this frag is a part of (curFrag[member])
 */
function addFragToCell(frag, cell, curFrag, member)
{
	readFolioName(frag.folioId, function(folioName)
	{
		var button = document.createElement("BUTTON");
		cell.appendChild(button);
		curFrag[member].push(button);
		button.style.margin = "1%";
		button.style.fontSize = fontSize;
		var content = document.createElement("DIV");
		content.style.textAlign = "center";
		button.appendChild(content);
		var div = document.createElement("DIV");
		div.style.width = "100%";
		div.style.height = "15em";
		div.style.backgroundSize = "contain";
		div.style.backgroundRepeat = "no-repeat";
		div.style.backgroundPosition = "center";
		//fetch the url of the image
		readFolioMini(frag.folioId, function(img)
		{
			if (img.length > 0)
				div.style.backgroundImage = 'url("'+img+'")';
			else div.style.backgroundColor = "gray";
		});
		content.appendChild(div);
		content.appendChild(document.createTextNode(folioName));
		content.appendChild(document.createElement("BR"));
		content.appendChild(document.createTextNode(buildFragLabel(frag)));
		button.onclick = function() {window.location = "<?php echo public_url("items/show"); ?>/"+frag.folioId+"#folioNav"};

		//refreshCanvas is called eachtime this callback completes (redrawing until completion of all queries)
		refreshCanvas();
	});
}

//coords of a box relative to the canvas
function boxCoords(canvas, box)
{
	var c = canvas.getBoundingClientRect();
	var b = box.getBoundingClientRect();
	var res = {};
	res.x = b.x-c.x;
	res.y = b.y-c.y;
	res.w = b.width;
	res.h = b.height;
	return res;
}
function drawArrow(g, fromx, fromy, tox, toy)
{
	g.beginPath();
	var headlen = g.measureText("m").width;
	var angle = Math.atan2(toy-fromy,tox-fromx);
	g.moveTo(fromx, fromy);
	g.lineTo(tox, toy);
	g.lineTo(tox-headlen*Math.cos(angle-Math.PI/6), toy-headlen*Math.sin(angle-Math.PI/6));
	g.moveTo(tox, toy);
	g.lineTo(tox-headlen*Math.cos(angle+Math.PI/6), toy-headlen*Math.sin(angle+Math.PI/6));
	g.stroke();
}
//redraws the canvas with available information (queries may still be running)
function refreshCanvas()
{
	var top = document.getElementById("fragNavTable");
	var canvas = document.getElementById("fragNavCanvas");
	canvas.width = top.clientWidth;
	canvas.height = top.clientHeight;
	var g = canvas.getContext("2d");
	g.font = "100% Arial";
	var em = g.measureText("m").width;

	//for each fragment of the current folio
	for (var i=0;i<folioFrags.length;i++)
	{
		var frag = folioFrags[i];
		var mid = boxCoords(canvas, frag.curBox);

		//draw all the arrows of relationship for the center fragment (only child can have more than one normally, but multiple are supported for all)
		g.strokeStyle = "#FF0000";
		for (var j=0;j<frag.prevBoxes.length;j++)
		{
			var box = boxCoords(canvas, frag.prevBoxes[j]);
			drawArrow(g, mid.x, mid.y+.5*mid.h, box.x+box.w, box.y+.5*box.h);
		}
		for (var j=0;j<frag.nextBoxes.length;j++)
		{
			var box = boxCoords(canvas, frag.nextBoxes[j]);
			drawArrow(g, mid.x+mid.w, mid.y+.5*mid.h, box.x, box.y+.5*box.h);
		}
		for (var j=0;j<frag.parentBoxes.length;j++)
		{
			var box = boxCoords(canvas, frag.parentBoxes[j]);
			drawArrow(g, mid.x+.5*mid.w, mid.y+mid.h, box.x+.5*box.w, box.y);
		}
		for (var j=0;j<frag.childBoxes.length;j++)
		{
			var box = boxCoords(canvas, frag.childBoxes[j]);
			var k = (box.x+.5*box.w)/canvas.width;
			drawArrow(g, mid.x+k*mid.w, mid.y, box.x+(1-k)*box.w, box.y+box.h);
		}

		//render the names of the relationships
		g.fillStyle = "#000000";
		if (frag.prevBoxes.length > 0)
		{
			var label = "<?php echo __("Previous"); ?>";
			var s = g.measureText(label).width;
			g.fillText(label, mid.x-s-.5*em, mid.y+.5*mid.h-.25*em);
		}
		if (frag.nextBoxes.length > 0)
		{
			var label = "<?php echo __("Next"); ?>";
			g.fillText(label, mid.x+mid.w+.5*em, mid.y+.5*mid.h-.25*em);
		}
		if (frag.childBoxes.length > 0)
		{
			var label = "<?php echo __("Origin of"); ?>";
			var s = g.measureText(label).width;
			g.fillText(label, mid.x+.5*mid.w+.25*em, mid.y-.25*em);
		}
		if (frag.parentBoxes.length > 0)
		{
			var label = "<?php echo __("Comes from"); ?>";
			var s = g.measureText(label).width;
			g.fillText(label, mid.x+.5*mid.w+.25*em, mid.y+mid.h+1.25*em);
		}
	}
}
</script>