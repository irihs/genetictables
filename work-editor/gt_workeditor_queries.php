<?php
require_once(dirname(__FILE__).'/../ajax/ajax_queries.php');
?>

<script>
"use strict";
/**
 * Async queries made by the genetic table editor.
 * Queries are forwarded to "async_queries.php" and the proper refresh calls are made to keep the interface in sync.
 * All inputs should be disabled during a query.
 */

/**
 * Generic callbacks after a request that refresh the appropriate section of the interface
 */
function refreshChapters()
{
	readChapters(<?php echo $item->id; ?>, 
		onChaptersRead,
		onChaptersFailed);
}
function refreshCodes()
{
	readCodes(<?php echo $item->id; ?>, 
		onCodesRead,
		onCodesFailed);
}
function refreshFragments()
{
	currentFragmentList = null;
	refreshFolioEditor();
}
function refreshFolios()
{
	currentFolioList = null;
	refreshFolioEditor();
}
//the source callback accepts an optional id that will be made the currently selected source
function refreshSources(id)
{
	if (id !== undefined)
		currentSourceId = id;
	currentSourceList = null;
	currentFragmentList = null;
	refreshFolioEditor();
}
function refreshTagFragments()
{
	currentFragmentList = null;
	refreshFolioEditor();
}

/** 
 * Functions for requesting write operations on the data. These are all forwarded to "async_queries.php"
 */
function newTagFragment(tagId, fragmentId)
{
	setEnabled(false);
	writeNewTagFragment(<?php echo $item->id; ?>, tagId, fragmentId, refreshTagFragments, refreshTagFragments);
}
function removeTagFragment(tagId, fragmentId)
{
	setEnabled(false);
	writeRemoveTagFragment(<?php echo $item->id; ?>, tagId, fragmentId, refreshTagFragments, refreshTagFragments);
}
function updateChapter(id, name, toPage)
{
	setEnabled(false);
	writeUpdateChapter(<?php echo $item->id; ?>, id, name, toPage, 
		refreshChapters,
		refreshChapters);
}
function updateCode(id, name, style, rank)
{
	setEnabled(false);
	writeUpdateCode(<?php echo $item->id; ?>, id, name, style, rank, 
		refreshChapters,
		refreshChapters);
}
function updateFolioSource(sourceId)
{
	if (currentFolioId < 0)
		return;
	setEnabled(false);
	writeUpdateFolioSource(currentFolioId, sourceId, 
		function(xml)
		{
			currentSourceId = sourceId;
			currentSourceList = null;
			currentFragmentList = null;
			refreshFolioEditor();
		},
		null);
}
function updateFolioNumber(number)
{
	if (currentFolioId < 0)
		return;
	setEnabled(false);
	writeUpdateFolioNumber(currentFolioId, number, 
		refreshFolios,
		refreshFolios);
}
function updateFragment(id, p0, l0, p1, l1, pos, withheld, parent, previous)
{
	setEnabled(false);
	writeUpdateFragment(<?php echo $item->id; ?>, id, p0, l0, p1, l1, pos, withheld, parent, previous,  
		refreshFragments,
		refreshFragments);
}
function newFragment()
{
	if (currentFolioId < 0)
		return;
	setEnabled(false);
	writeNewFragment(<?php echo $item->id; ?>, currentFolioId, 
		refreshFragments,
		refreshFragments);
}
function newChapter()
{
	setEnabled(false);
	writeNewChapter(<?php echo $item->id; ?>, 
		refreshChapters,
		refreshChapters);
}
function newCode()
{
	setEnabled(false);
	writeNewCode(<?php echo $item->id; ?>, 
		refreshCodes,
		refreshCodes);
}
function newSource()
{
	setEnabled(false);
	writeNewSource(<?php echo $item->id; ?>, "<?php echo __("New source"); ?>", 
		refreshSources,
		refreshSources);
}
function updateSourceName(name, renameFolios)
{
	if (currentSourceId < 0)
		return;
	setEnabled(false);
	writeUpdateSourceName(<?php echo $item->id; ?>, currentSourceId, name, renameFolios, 
		function(xml) {refreshSources();},
		refreshSources);
}
function updateSource(rank, label, color)
{
	if (currentSourceId < 0)
		return;
	setEnabled(false);
	writeUpdateSource(currentSourceId, rank,  label, color, 
		function(xml) {refreshSources();},
		refreshSources);
}
function updateSourceVisibility(visibility)
{
	if (currentSourceId < 0)
		return;
	setEnabled(false);
	writeUpdateSourceVisibility(currentSourceId, visibility,
		function(xml) {refreshSources();},
		refreshSources);
}
function removeSource(cascade)
{
	if (currentSourceId < 0)
		return;
	setEnabled(false);
	writeRemoveSource(<?php echo $item->id; ?>, currentSourceId, cascade, 
		refreshSources,
		refreshSources);
}
function mergeSource(mergeId, renameFolios)
{
	if (currentSourceId < 0)
		return;
	setEnabled(false);
	writeMergeSource(<?php echo $item->id; ?>, currentSourceId, mergeId, renameFolios, 
		refreshSources,
		refreshSources);
}
function removeFragment(id)
{
	setEnabled(false);
	writeRemoveFragment(<?php echo $item->id; ?>, id, 
		refreshFragments,
		refreshFragments);
}
</script>