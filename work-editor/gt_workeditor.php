<?php
/**
 * Main file for the work editor.
 * Reading and writing data is done asynchronosly with the methods in "gt_workeditor_queries.php".
 * Static HTML elements are defined in "gt_editor_layout.php".
 * The folio import section is defined in  "gt_workeditor_import.php" (for both uploaded images and flaubert database exports).
 */
require_once(dirname(__FILE__).'/../utils.php');
require_once(dirname(__FILE__).'/../db/db_works.php');
require_once(dirname(__FILE__).'/../db/db_tags.php');
require_once(dirname(__FILE__).'/../db/db_tables.php');
$workName = getWorkTitle($this, $item->id);
//phpinfo();
?>

<script>
"use strict";

//map from table ids to objects
var tables = {};
var table;
var tag;
<?php
//get the tables and tags from the data model
$tables = getWorkTables($this, $item->id);
$tags = array();
foreach ($tables as $table)
	$tags[$table['id']] = getTagData($this, $table['id']);
//for each table in this work
foreach ($tables as $table)
{
	//... a js object is created
	?>table = {};
	table.id = <?php echo $table['id']; ?>;
	table.name = "<?php echo getTableTitle($this, $table['id']); ?>";
	table.tags = {};
	<?php
	//for all tags (or groups)
	foreach ($tags[$table['id']] as $tag)
	{
		//... a js object is created
		?>tag = {};
		tag.id = <?php echo $tag['id']; ?>;
		tag.name = "<?php echo $tag['name']; ?>";
		table.tags[tag.id] = tag;
		<?php
	}
	?>tables[table.id] = table;
	<?php
}
?>

//arrays of items that are displayed. A null value means the list awaits an async query to be made
//the items are in parsed form (see "parsers.php")
var currentChapterList = null;
var currentCodeList = null;
var currentSourceList = null;
var currentFolioList = null;
var currentFragmentList = null;

//ids of the items that are currently being edited (none for fragments because all fragments are displayed at once)
var currentSourceId = -1;
var currentFolioId = -1;

//make all inputs in the editor disabled
function setEnabled(enabled)
{
	setElementEnabled(document.getElementById("folioEditor"), enabled);
	document.getElementById("activityZoneMessage").innerHTML = enabled ? "&nbsp;" : "<?php echo __("Waiting for server..."); ?>";
}

//returns the current source in parsed form based on the value of currentSourceId
//null if none
function getCurrentSource()
{
	if (currentSourceList === null || currentSourceId < 0)
		return null;
	for (var i=0;i<currentSourceList.length;i++)
		if (currentSourceList[i].id == currentSourceId)
			return currentSourceList[i];
	return null;
}
//returns the current folio in parsed form based on the value of currentFolioId
//null if none
function getCurrentFolio()
{
	if (currentFolioList === null || currentFolioId < 0)
		return null;
	for (var i=0;i<currentFolioList.length;i++)
		if (currentFolioList[i].id == currentFolioId)
			return currentFolioList[i];
	return null;
}
function sourceIdExists() {return getCurrentSource() !== null;}
function folioIdExists() {return getCurrentFolio() !== null;}

//callbacks after an async read of the data model
function onChaptersRead(res)
{
	currentChapterList = res;
	refreshChapterList();
	setEnabled(true);
}
function onChaptersFailed()
{
	currentChapterList = [];
	refreshChapterList();
	setEnabled(true);
}
function onCodesRead(res)
{
	currentCodeList = res;
	refreshCodeList();
	setEnabled(true);
}
function onCodesFailed()
{
	currentCodeList = [];
	refreshCodeList();
	setEnabled(true);
}

//callback after requestinbg the list of sources. If the currentSourceId is undefined or invalid, it defaults to the first element of the list
function onSourcesRead(res)
{
	currentSourceList = res;
	if (!sourceIdExists())
		currentSourceId = -1;
	if (currentSourceId < 0 && res.length > 0)
		currentSourceId = res[0].id;
	refreshSourceList(); 
	setEnabled(true);
	refreshFolioEditor();
}
//if reading fails, clear the list
function onSourcesFailed()
{
	currentSourceList = [];
	currentSourceId = -1;
	refreshSourceList();
	setEnabled(true);
	refreshFolioEditor();
}

//callback after requestinbg the list of folios. If the currentFolioId is undefined or invalid, it defaults to the first element of the list
function onFoliosRead(res)
{
	currentFolioList = res;
	if (!folioIdExists())
		currentFolioId = -1;
	if (currentFolioId < 0 && res.length > 0)
		currentFolioId = res[0].id;
	refreshFolioList();
	setEnabled(true);
	refreshFolioEditor();
}
//if reading fails, clear the list
function onFoliosFailed() 
{
	currentFolioList = [];
	currentFolioId = -1;
	refreshFolioList();
	setEnabled(true);
	refreshFolioEditor();
}

//callback after requestinbg the list of fragments
function onFragmentsRead(res)
{
	currentFragmentList = res;
	refreshFragmentList();
	setEnabled(true);
	refreshFolioEditor();
}
//if reading fails, clear the list
function onFragmentsFailed() 
{
	currentFragmentList = [];
	refreshFragmentList();
	setEnabled(true);
	refreshFolioEditor();
}

//main refresh function that determines which queries should be made
function refreshFolioEditor()
{
	//if the source list is null, request sources
	if (currentSourceList === null)
	{
		setEnabled(false);
		currentFolioList = null;
		readSources(<?php echo $item->id; ?>, 
			onSourcesRead,
			onSourcesFailed);
		return;
	}
	//if the folio list is null, request folios
	if (currentFolioList === null)
	{
		if (currentSourceId < 0)
			onFoliosFailed();
		else
		{
			setEnabled(false);
			readFolios(<?php echo $item->id; ?>, currentSourceId, 
				onFoliosRead,
				onFoliosFailed);
		}
		return;
	}
	//if the fragment list is null, request fragments
	if (currentFragmentList === null)
	{
		if (currentFolioId < 0)
			onFragmentsFailed();
		else
		{
			setEnabled(false);
			readFragmentsForFolio(<?php echo $item->id; ?>, currentFolioId, 
				onFragmentsRead,
				onFragmentsFailed);
		}
	}
}

//builds a callback for the selection of a source
function buildSourcesOnClick(source)
{
	return function() {currentSourceId = source.id; currentFolioList = null; currentFragmentList = null; refreshSourceList(); refreshFolioEditor();};
}
//builds a callback for the selection of a folio
function buildFoliosOnClick(folio)
{
	return function() {currentFolioId = folio.id; currentFragmentList = null; refreshFolioList(); refreshFolioEditor();};
}
//strips the full source name of the work name
function shortSourceName(name)
{
	return name.startsWith("<?php echo $workName; ?> - ") ? name.substr("<?php echo $workName; ?> - ".length) : name;
}
//refreshes the 'source' section of the interface with the list of sources and the properties of the currently selected source
function refreshSourceList()
{
	var source = getCurrentSource();
	
	if (typeof updateImportSource !== 'undefined')
		updateImportSource(source !== null ? source.id : "", source !== null ? source.name : "");
	//console.log("???"+currentSourceList.length);
	//clear and repopulate the list of sources
	var folioEditorSources = document.getElementById("folioEditorSources");
	clear(folioEditorSources);
	if (currentSourceList !== null)
		for (var i=0;i<currentSourceList.length;i++)
	{
		if (i > 0)
			folioEditorSources.appendChild(createSpace());
		//a link is created for each source
		var link = document.createElement("BUTTON");
		link.style.cssText = "border-style: none; background: none; text-decoration: none; text-shadow: none; color: black; box-shadow: none; margin: 0px";
		link.innerHTML = shortSourceName(currentSourceList[i].name);
		link.type = "button";
		link.onclick = buildSourcesOnClick(currentSourceList[i]);
		//highlight the selected source
		if (currentSourceId == currentSourceList[i].id)
		{
			link.style.color = "blue";
			link.style.backgroundColor = "#eeeeee";
		}
		folioEditorSources.appendChild(link);
	}
	//a static label if the list is empty (so you know its not because of an error)
	if (currentSourceList === null || currentSourceList.length == 0)
		folioEditorSources.appendChild(document.createTextNode("<?php echo __("No sources to display"); ?>"));

	//fill the drop down lists (for the source of a folio and for the source merge action)
	var folioSourceSelect = document.getElementById("folioSourceSelect");
	clear(folioSourceSelect);
	var sourceMergeSelect = document.getElementById("sourceMergeSelect");
	clear(sourceMergeSelect);
	if (currentSourceList !== null)
		for (var i=0;i<currentSourceList.length;i++)
	{
		var option = document.createElement("OPTION");
		option.innerHTML = shortSourceName(currentSourceList[i].name);
		option.value = currentSourceList[i].id;
		folioSourceSelect.appendChild(option);
		sourceMergeSelect.appendChild(option.cloneNode(true));
	}

	var sourceInfo = document.getElementById("sourceCell");
	var foliosCell = document.getElementById("foliosCell");
	//if no source is selected (ie. the source list is empty), hide the source fields form and the entire folio section
	if (source === null)
	{
		sourceInfo.style.display = "none";
		foliosCell.style.display = "none";
	}
	//otherwise fill all the fields
	else
	{
		sourceInfo.style.display = "table-cell";
		foliosCell.style.display = "table-row";
		document.getElementById("sourceMergeSelect").value = source.id;
		document.getElementById("sourceNameInput").value = shortSourceName(source.name);
		document.getElementById("sourceRankInput").value = source.rank;
		document.getElementById("sourceLabelInput").value = source.label;
		document.getElementById("sourceColorInput").value = source.color;
		document.getElementById("sourceColorDisplay").style.backgroundColor = "#"+source.color;
	}
	//folioSourceSelect.value = currentSourceId;
}

//callback when the 'Remove' button is hit for a folio : opens the Omeka item edit page in a new tab/window
function buildFolioLink(folioId) {return function() 
	{var popup = window.open("about:blank", "_blank"); popup.location = "<?php echo admin_url("items/edit"); ?>/"+folioId;};}
//callback when the 'Remove' button is hit for a folio : opens the Omeka item edit page in a new tab/window
function buildFolioTranscriptionLink(folioId) {return function() 
	{var popup = window.open("about:blank", "_blank"); popup.location = "<?php echo admin_url("items/edit"); ?>/"+folioId+"#transcription-metadata";};}
//refreshes the 'folio' section of the interface with the list of folios and the properties of the currently selected folio
function refreshFolioList()
{
	//clear and repopulate the list of folios
	var folioEditorFolios = document.getElementById("folioEditorFolios");
	clear(folioEditorFolios);
	if (currentFolioList != null)
		for (var i=0;i<currentFolioList.length;i++)
	{
		if (i > 0)
			folioEditorFolios.appendChild(createSpace());
		//a link is created for each source
		var link = document.createElement("BUTTON");
		link.style.cssText = "border-style: none; background: none; text-decoration: none; text-shadow: none; color: black; box-shadow: none; margin: 0px";
		link.innerHTML = currentFolioList[i].number;
		link.onclick = buildFoliosOnClick(currentFolioList[i]);
		//highlight the selected folio
		if (currentFolioId == currentFolioList[i].id)
		{
			link.style.color = "blue";
			link.style.backgroundColor = "#eeeeee";
		}
		folioEditorFolios.appendChild(link);
		if (currentFolioId == currentFolioList[i].id)
			folioEditorFolios.scrollTop = parseInt(link.offsetTop);
	}
	if (currentFolioList === null || currentFolioList.length == 0)
		folioEditorFolios.appendChild(document.createTextNode("<?php echo __("No folios to display"); ?>"));

	var folioInfo = document.getElementById("folioCell");
	var fragmentsInfo = document.getElementById("folioFragmentsCell");
	var folio = getCurrentFolio();
	//if no folio is selcted (empty list), hide the folio fields and the fragment section
	if (folio === null)
	{
		folioInfo.style.display = "none";
		fragmentsInfo.style.display = "none";
		document.getElementById("removeFolioLink").href = "";
		document.getElementById("editFolioLink").onclick = function() {};
	}
	//otherwise fill the folio fields
	else
	{
		folioInfo.style.display = "table-cell";
		fragmentsInfo.style.display = "table-row";
		document.getElementById("folioSourceSelect").value = folio.sourceId;
		document.getElementById("folioNumberInput").value = folio.number;
		document.getElementById("removeFolioLink").href = buildFolioLink(folio.id);
		document.getElementById("editFolioLink").onclick = buildFolioTranscriptionLink(folio.id);
	}
}

//method for the several numeric fields of fragments. If changed they will all trigger a fragment update request
function buildNumberField(id, name, value)
{
	var input = document.createElement("INPUT");
	input.id = name+id;
	input.type = "number";
	input.value = value;
	input.style.cssText = "width: 4em";
	input.onchange = buildUpdateFragmentOnChange(id);
	return input;
}
//method for the text fields of fragments. If changed they will all trigger a fragment update request
function buildTextField(id, name, value)
{
	var input = document.createElement("INPUT");
	input.id = name+id;
	input.type = "text";
	input.value = value;
	input.style.cssText = "width: 8em";
	input.onchange = buildUpdateFragmentOnChange(id);
	return input;
}
//callback for deleting a fragment
function buildRemoveFragmentOnClick(fragmentId)
{
	return function() {removeFragment(fragmentId);};
}
//build a callback for requesting a fragment update
function buildUpdateFragmentOnChange(id)
{
	return function() {updateFragment(id, 
		document.getElementById("p0"+id).value, 
		document.getElementById("l0"+id).value, 
		document.getElementById("p1"+id).value, 
		document.getElementById("l1"+id).value,
		document.getElementById("pos"+id).value,
		document.getElementById("withheld"+id).checked,
		document.getElementById("parent"+id).value,
		document.getElementById("previous"+id).value);}
}
//refreshes the 'fragment' section of the interface with the list of fragments. Each entry displays all the fields associated with that fragment
function refreshFragmentList()
{
	var folioFragmentsTable = document.getElementById("folioFragmentsTable");
	clear(folioFragmentsTable);
	//each entry in the table is created dynamically
	if (currentFragmentList !== null)
		for (var i=0;i<currentFragmentList.length;i++)
	{
		var id = currentFragmentList[i].id;
		var tr = document.createElement("TR");
		folioFragmentsTable.appendChild(tr);
		td = document.createElement("TD");
		td.style.cssText = "border: none;";
		tr.appendChild(td);
		//display the identifier for a fragment. These are used in the 'origin' and 'previous' fields of fragments
		td.innerHTML = "<div class='gteSubHeader'><?php echo __("Fragment "); ?>"+id+"</div>";
		td.appendChild(document.createElement("BR"));
		//orgin field
		td.appendChild(document.createTextNode(" <?php echo __("Origin"); ?> "));
		td.appendChild(buildNumberField(id, "parent", currentFragmentList[i].parentId));
		td.appendChild(createSpace());
		//previous field
		td.appendChild(document.createTextNode(" <?php echo __("Previous"); ?> "));
		td.appendChild(buildNumberField(id, "previous", currentFragmentList[i].previousId));
		td.appendChild(createSpace());
		//withheld box
		var input = document.createElement("INPUT");
		input.type = "checkbox";
		input.id = "withheld"+id;
		input.checked = currentFragmentList[i].withheld;
		input.onchange = buildUpdateFragmentOnChange(id);
		td.appendChild(input);
		td.appendChild(document.createTextNode(" <?php echo __("Withheld"); ?>"));
		td.appendChild(document.createElement("BR"));
		td.appendChild(document.createElement("BR"));

		//from page field
		td.appendChild(document.createTextNode("<?php echo __("From page"); ?> "));
		td.appendChild(buildNumberField(id, "p0", currentFragmentList[i].fromPage));
		//from line field
		td.appendChild(document.createTextNode(" <?php echo __("line"); ?> "));
		td.appendChild(buildNumberField(id, "l0", currentFragmentList[i].fromLine));
		//to page field
		td.appendChild(document.createTextNode(" <?php echo __("to page"); ?> "));
		td.appendChild(buildNumberField(id, "p1", currentFragmentList[i].toPage));
		//to line field
		td.appendChild(document.createTextNode(" <?php echo __("line"); ?> "));
		td.appendChild(buildNumberField(id, "l1", currentFragmentList[i].toLine));
		//position field
		td.appendChild(document.createTextNode(" <?php echo __("located"); ?> "));
		input = document.createElement("INPUT");
		input.id = "pos"+id;
		input.type = "text";
		input.style.cssText = "width: 100px";
		input.value = currentFragmentList[i].position;
		input.onchange = buildUpdateFragmentOnChange(id);
		td.appendChild(input);
		td.appendChild(document.createElement("BR"));
		td.appendChild(document.createElement("BR"));

		//dropdown list for adding a fragment to a table
		//a selection will update the list of groups
		td.appendChild(document.createTextNode("<?php echo __("Add to table"); ?> "));
		td.appendChild(createSpace());
		input = document.createElement("SELECT");
		input.id = "fragmentTable";
		input.onchange = buildTableOnChange(input);
		//make default first option
		var option = document.createElement("OPTION");
		input.appendChild(option);
		option.value = "-1";
		option.innerHTML = "<?php echo __("Select a table"); ?>";
		//build an option element for each table
		for (var tableId in tables)
		{
			option = document.createElement("OPTION");
			input.appendChild(option);
			option.value = tableId;
			option.innerHTML = tables[tableId].name;
		}
		td.appendChild(input);
		td.appendChild(createSpace());
		//dropdown list for adding a fragment to a group
		//a selection will trigger the creation of fragment-group association in the data model
		//the list is filled when table is selected in the previous list
		td.appendChild(document.createTextNode("<?php echo __(" in group "); ?> "));
		td.appendChild(createSpace());
		input = document.createElement("SELECT");
		input.id = "fragmentTag";
		input.onchange = buildTagOnChange(input, id);
		//default first option
		option = document.createElement("OPTION");
		input.appendChild(option);
		option.value = "-1";
		option.innerHTML = "<?php echo __("Select a group"); ?>";
		td.appendChild(input);
		td.appendChild(document.createElement("BR"));
		td.appendChild(document.createElement("BR"));
		//show a list of existing associations
		for (var j=0;j<currentFragmentList[i].tags.length;j++)
		{
			var table = tables[currentFragmentList[i].tagTables[j]];
			//name of the table and name of the group
			td.appendChild(bold((table.name)+" - "+table.tags[currentFragmentList[i].tags[j]].name));
			td.appendChild(createSpace());
			//delete button to remove association
			input = document.createElement("INPUT");
			td.appendChild(input);
			input.type = "button";
			input.value = "<?php echo __("Delete"); ?>";
			input.onclick = buildDeleteTagFragment(table.id, currentFragmentList[i].tags[j], id);
			td.appendChild(document.createElement("BR"));
			td.appendChild(document.createElement("BR"));
		}
		
		//delete button
		input = document.createElement("INPUT");
		td.appendChild(input);
		input.type = "button";
		input.value = "<?php echo __("Delete"); ?>";
		input.onclick = buildRemoveFragmentOnClick(id);
	}
	//if the list is empty, show message so the user knows its not an error
	if (currentFragmentList === null || currentFragmentList.length == 0)
	{
		var tr = document.createElement("TR");
		folioFragmentsTable.appendChild(tr);
		var td = document.createElement("TD");
		tr.appendChild(td);
		td.appendChild(document.createTextNode("<?php echo __("No fragments to display"); ?>"));
	}
}
//call back for deleting an association
function buildDeleteTagFragment(tableId, tagId, fragmentId)
{
	return function()
	{
		removeTagFragment(tagId, fragmentId);
	};
}
//call back for selecting a group for an association
function buildTagOnChange(select, fragmentId)
{
	return function()
	{
		if (select.value != -1)
			newTagFragment(select.value, fragmentId);
	};
}
//call back for selecting a table
function buildTableOnChange(select)
{
	return function()
	{
		//clear the list of groups
		var tags = document.getElementById("fragmentTag");
		tags.innerHTML = "";
		//default option
		var option = document.createElement("OPTION");
		tags.appendChild(option);
		option.value = "-1";
		option.innerHTML = "<?php echo __("Select a group"); ?>";
		//if an actual table was selected
		if (select.value != -1)
		{
			var table = tables[select.value];
			//build an option for each group
			for (var tagId in table.tags)
			{
				option = document.createElement("OPTION");
				tags.appendChild(option);
				option.value = tagId;
				option.innerHTML = table.tags[tagId].name;
			}
		}
	};
}

//build a callback for requesting a chapter update
function buildUpdateChapterOnChange(id)
{
	return function() {updateChapter(id, 
		document.getElementById("chapterName"+id).value, 
		document.getElementById("chapterToPage"+id).value);}
}
//refresh the chapter list after a data model read
function refreshChapterList()
{
	var chaptersTable = document.getElementById("chaptersTable");
	clear(chaptersTable);

	if (currentChapterList !== null)
		for (var i=0;i<currentChapterList.length;i++)
	{
		var id = currentChapterList[i].id;
		//create row
		var tr = document.createElement("TR");
		chaptersTable.appendChild(tr);
		var td = document.createElement("TD");
		td.style.cssText = "border: none;";
		tr.appendChild(td);
		
		//name field
		td.appendChild(document.createTextNode(" <?php echo __("Name"); ?> "));
		var input = document.createElement("INPUT");
		input.id = "chapterName"+id;
		input.type = "text";
		input.value = currentChapterList[i].name;
		input.style.cssText = "width: 12em";
		input.onchange = buildUpdateChapterOnChange(id);
		td.appendChild(input);
		td.appendChild(createSpace());

		//last page field
		td.appendChild(document.createTextNode(" <?php echo __("Last page"); ?> "));
		input = document.createElement("INPUT");
		input.id = "chapterToPage"+id;
		input.type = "number";
		input.value = currentChapterList[i].toPage;
		input.style.cssText = "width: 4em";
		input.onchange = buildUpdateChapterOnChange(id);
		td.appendChild(input);
		td.appendChild(createSpace());
	}
}

//build a callback for requesting a code update
function buildUpdateCodeOnChange(id)
{
	return function() {updateCode(id, 
		document.getElementById("codeName"+id).value, 
		document.getElementById("codeStyle"+id).value, 
		document.getElementById("codeRank"+id).value);}
}
//refresh the transcription code list after a data model read
function refreshCodeList()
{
	var codesTable = document.getElementById("codesTable");
	clear(codesTable);

	if (currentCodeList !== null)
		for (var i=0;i<currentCodeList.length;i++)
	{
		var id = currentCodeList[i].id;
		//create row
		var tr = document.createElement("TR");
		codesTable.appendChild(tr);
		var td = document.createElement("TD");
		td.style.cssText = "border: none;";
		tr.appendChild(td);
		
		//name field
		td.appendChild(document.createTextNode(" <?php echo __("Name"); ?> "));
		var input = document.createElement("INPUT");
		input.id = "codeName"+id;
		input.type = "text";
		input.value = currentCodeList[i].name;
		input.style.cssText = "width: 12em";
		input.onchange = buildUpdateCodeOnChange(id);
		td.appendChild(input);
		td.appendChild(createSpace());

		//style field
		td.appendChild(document.createTextNode(" <?php echo __("Style"); ?> "));
		var input = document.createElement("INPUT");
		input.id = "codeStyle"+id;
		input.type = "text";
		input.value = currentCodeList[i].style;
		input.style.cssText = "width: 16em";
		input.onchange = buildUpdateCodeOnChange(id);
		td.appendChild(input);
		td.appendChild(createSpace());

		//last page field
		td.appendChild(document.createTextNode(" <?php echo __("Rank"); ?> "));
		input = document.createElement("INPUT");
		input.id = "codeRank"+id;
		input.type = "number";
		input.value = currentCodeList[i].rank;
		input.style.cssText = "width: 4em";
		input.onchange = buildUpdateCodeOnChange(id);
		td.appendChild(input);
		td.appendChild(createSpace());
	}
}
</script>

<?php
include('gt_workeditor_queries.php');
include('gt_workeditor_layout.php');
include('gt_workeditor_import.php');
?>

<script>
//init the page with a first refresh (this will detect the 'null' item lists and trigger read requests)
refreshFolioEditor();
refreshChapters();
refreshCodes();
</script>
