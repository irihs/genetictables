<!-- ?php phpinfo(); ? -->
<!--
All the static elements of the table editing interface.
Dynamic behavior is in "gt_editor.php" 
 -->

<script>
"use strict";
//generic error handler used by "async_queries.php"
//pops up an error zone at the top of the page with the message that the user can then hide
function errorHandler(error)
{
	var errorZone = document.getElementById("errorZone");
	if (error === null)
		errorZone.style.display = "none";
	else
	{
		document.getElementById("errorZoneMessage").innerHTML = error;
		errorZone.style.display = "table";
		window.location = "#";
	}
}

//"Are you sure..." messages before sending dangerous queries
function requestRemoveSource()
{
	if (confirm("<?php echo __("Would you like to remove the folios and fragments contained within this source?"); ?>"))
		removeSource("true");
	else if (confirm("<?php echo __("Would you like to remove the source but keep the folios and fragments?"); ?>"))
		removeSource("false");
}
function requestRenameSource(name)
{
	name = "<?php echo $workName; ?> - "+name;
	if (confirm("<?php echo __("Would you like to rename the folios contained within this source?"); ?>"))
		updateSourceName(name, "true");
	else if (confirm("<?php echo __("Would you like to rename the source but keep the folio names?"); ?>"))
		updateSourceName(name, "false");
}
function requestMergeSource(mergeId)
{
	if (currentSourceId < 0 || currentSourceId == mergeId)
		return;
	if (confirm("<?php echo __("This source will be deleted and its contents will be added to the source you just selected. Would you like to rename the folios contained within this source?") ?>"))
		mergeSource(mergeId, "true");
	else if (confirm("<?php echo __("Would you like to merge the sources but keep the folio names?"); ?>"))
		mergeSource(mergeId, "false");
	else document.getElementById('sourceMergeSelect').value = currentSourceId;
}
function requestUpdateSource()
{
	updateSource(
		document.getElementById('sourceRankInput').value,
		document.getElementById('sourceLabelInput').value,
		document.getElementById('sourceColorInput').value);
}
function requestUpdateSourceVisibility()
{
	var v = document.getElementById('sourceVisibilityInput').value;
	if (v < 0)
		return;
	updateSourceVisibility(v == "1");
}
</script>

<style>
.gteSectionCell {border: none;}
.gteSection {margin-bottom: 2em; border: 1px solid gray}
.gteSelectionListRow {}
.gteSelectionList {border: none; border-bottom: 1px solid lightgray; margin: .1em; padding: .1em; overflow-y: auto; resize: vertical; font-size: small; display:inline-block;}
.gteHeader {border: none; background: royalblue; color:white; height: 1.5em}
.gteSubHeader {border: none; background: lightgray; font-weight: bold}
</style>

<!-- Popup error area, can be hidden -->
<span id="activityZoneMessage" style="color: #a0a0ff">&nbsp;</span>
<table id="errorZone" style="width: 100%; display: none; background-color: #dddddd"><tr>
	<td><span id="errorZoneMessage" style="color: red"></span></td>
	<td style="text-align: right"><a style="color: blue; text-decoration: underline" onClick="errorHandler(null);"><?php echo __("Hide"); ?></a></td>
</tr></table>
<table id="folioEditor">

	<!-- Source editing section -->
	<tr><td class="gteSectionCell"><table class="gteSection">
		<tr><td class="gteHeader">
			<b><?php echo __("Sources"); ?></b>
			&nbsp;&nbsp;&nbsp;
			<input type="button" value="<?php echo __("New source"); ?>" onclick="newSource();">
		</td></tr>
		<tr><td style="border: none"><small><?php echo __("List of sources in this work. Click on a source to edit it and view it's folios."); ?></small></td></tr>
		<tr class="gteSelectionListRow"><td class="gteSelectionList" id="folioEditorSources"></td></tr>
		<tr><td style="border: none" id="sourceCell">
			<br/><br/>
			<?php echo __("Name"); ?>
			<input id="sourceNameInput" type="text" style="width: 12em" onChange="requestRenameSource(document.getElementById('sourceNameInput').value);"/>
			&nbsp;&nbsp;&nbsp;
			<?php echo __("Label"); ?>
			<input id="sourceLabelInput" type="text" style="width: 5em" onChange="requestUpdateSource()"/>
			&nbsp;&nbsp;&nbsp;
			<?php echo __("Rank"); ?>
			<input id="sourceRankInput" type="number" style="width: 5em" onChange="requestUpdateSource()"/>
			<span style="display: none">
				&nbsp;&nbsp;&nbsp;
				<?php echo __("Color"); ?>
				<input id="sourceColorInput" type="text" style="width: 8em" onChange="requestUpdateSource()"/>
				<span id="sourceColorDisplay" style="border: 1px solid black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			</span>
			<br/><br/>
			<?php echo __("Merge with"); ?>
			<select id="sourceMergeSelect" onChange="requestMergeSource(document.getElementById('sourceMergeSelect').value);"></select>
			&nbsp;&nbsp;&nbsp;
			<?php echo __("Make folios "); ?>
			<select id="sourceVisibilityInput" onchange="requestUpdateSourceVisibility()">
				<option value="-1"><?php echo __("Select"); ?></option>
				<option value="0"><?php echo __("Private"); ?></option>
				<option value="1"><?php echo __("Public"); ?></option>
			</select>
			<br/><br/>
			<input type="button" value="<?php echo __("Delete"); ?>" onclick="requestRemoveSource();">
		</td></tr>
	</table>
	
	<!-- Folio editing section -->
	<tr id="foliosCell"><td class="gteSectionCell"><table class="gteSection">
		<tr><td class="gteHeader">
			<b><?php echo __("Folios"); ?></b>
			&nbsp;&nbsp;&nbsp;
			<a style="color: linen; text-decoration: underline" href="#importFolios" onClick="blinkImportFolios();"><?php echo __("New folio"); ?></a>
		</td></tr>
		<tr><td style="border: none"><small><?php echo __("List of folios in the selected source. Click on a folio to edit it and view it's fragments."); ?></small></td></tr>
		<tr class="gteSelectionListRow"><td class="gteSelectionList"><div id="folioEditorFolios" style="height: 6em"></div></td></tr>
		<tr><td style="border: none" id="folioCell">
			<br/><br/>
			<?php echo __("Source"); ?>
			<select id="folioSourceSelect" onChange="updateFolioSource(document.getElementById('folioSourceSelect').value);"></select>
			&nbsp;&nbsp;&nbsp;
			<?php echo __("Number"); ?>
			<input id="folioNumberInput" type="text" style="width: 7em" onChange="updateFolioNumber(document.getElementById('folioNumberInput').value);"/>
			<br/><br/>
			<a style="color: blue; text-decoration: underline" id="removeFolioLink" href=""><?php echo __("Delete"); ?></a>
			&nbsp;&nbsp;&nbsp;
			<a style="color: blue; text-decoration: underline; cursor: pointer" id="editFolioLink"><?php echo __("Edit Transcription"); ?></a> 
		</td></tr>
	</table></td></tr>
	
	<!-- Fragment editing section -->
	<tr id="folioFragmentsCell"><td class="gteSectionCell"><table class="gteSection">
		<tr><td class="gteHeader">
			<b><?php echo __("Fragments"); ?></b>
			&nbsp;&nbsp;&nbsp;
			<input type="button" value="<?php echo __("New fragment"); ?>" onclick="newFragment();"></input>
		</td></tr>
		<tr><td style="border: none"><small><?php echo __("List of fragments in the selected folio."); ?></small></td></tr>
		<!-- tr><td style="border: none"></td></tr -->
		<tr><td style="border: none; margin: 0px; padding: 0px">
			<table id="folioFragmentsTable">
				<!-- Populated dynamically in "gt_editor.php" -->
			</table>
		</td></tr>
	</table></td></tr>
	
	<!-- Chapters editing section -->
	<tr id="chaptersCell"><td class="gteSectionCell"><table class="gteSection">
		<tr><td class="gteHeader">
			<b><?php echo __("Chapters"); ?></b>
			&nbsp;&nbsp;&nbsp;
			<input type="button" value="<?php echo __("New chapter"); ?>" onclick="newChapter();"></input>
		</td></tr>
		<tr><td style="border: none"><small><?php echo __("List of chapters."); ?></small></td></tr>
		<tr><td style="border: none; margin: 0px; padding: 0px">
			<table id="chaptersTable">
				<!-- Populated dynamically in "gt_editor.php" -->
			</table>
		</td></tr>
	</table></td></tr>
	
	<!-- Style codes editing section -->
	<tr id="codesCell"><td class="gteSectionCell"><table class="gteSection">
		<tr><td class="gteHeader">
			<b><?php echo __("Style codes"); ?></b>
			&nbsp;&nbsp;&nbsp;
			<input type="button" value="<?php echo __("New code"); ?>" onclick="newCode();"></input>
		</td></tr>
		<tr><td style="border: none"><small><?php echo __("List of codes."); ?></small></td></tr>
		<tr><td style="border: none; margin: 0px; padding: 0px">
			<table id="codesTable">
				<!-- Populated dynamically in "gt_workeditor.php" -->
			</table>
		</td></tr>
	</table></td></tr>
	
</table>
