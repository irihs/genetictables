<script>
	/**
	 * The import area of the work editor. Handles creation of folios from local or remote images and the import of Flaubert databases
	 */

	"use strict";
	//callback to trigger folio renumbering
	function buildRenumberFoliosCall(input, i)
	{
		return function() {renumberFolios(input.value, i);};
	}
	function buildCheckFolioNumberCall(input)
	{
		return function() {if (input.value == "") input.value = "1";};
	}
	//renumber the folio numbers starting at 'from'
	function renumberFolios(val, from)
	{
		//get the recto and suffix settings
		var frontBack = document.getElementById('importOptionsFrontBack').checked;
		var suffix = document.getElementById('importOptionsBackSuffix').value;
		var num = parseInt(val);
		if (isNaN(num))
			return;
		var verso = frontBack && suffix.length > 0 ? val.endsWith(suffix) : false;
		//give the new number for each folio
		for (var i=from+1;i<nSelectedFilesForImport;i++)
		{
			num = !frontBack || verso ? num+1 : num;
			verso = !verso;
			document.getElementById('FolioNumber'+i).value = ""+num+(frontBack && verso ? suffix : "");
		}
	}
	//set the visibility setting for all folios
	function changeFolioVisibility(makePublic)
	{
		for (var i=0;i<nSelectedFilesForImport;i++)
			document.getElementById('FolioPublic'+i).checked = makePublic;
	}
	//split the space separated list of urls into an array
	function parseURLs()
	{
		var vals = document.getElementById('selectFolioURLs').value;
		if (vals.length == 0)
			return [];
		var res = [];
		var toks = vals.split(" ");
		for (var i=0;i<toks.length;i++)
		{
			var tok = toks[i].trim();
			if (tok.length > 0)
				res[res.length] = tok;
		}
		return res;
	}
	//insert a margin into the table
	function buildFolioMargin()
	{
		var tr = document.createElement("TR");
		tr.style.cssText = "margin: 0px; border: none";
		var td = document.createElement("TD");
		td.style.cssText = "border: none; margin: 0px; vertical-align: middle";
		tr.appendChild(td);
		return tr;
	}
	//insert a folio entry (with its fields) into the table
	function buildFolioEntry(name, index, len)
	{
		//row for the entry
		var tr = document.createElement("TR");
		tr.style.cssText = "margin: 0px; border: none";
		//cell for folio name
		var td = document.createElement("TD");
		td.style.cssText = "border: none; margin: 0px; vertical-align: middle";
		td.innerHTML = "<b>&bull; "+name+"</b>";
		tr.appendChild(td);
		//cell for  folio fields
		td = document.createElement("TD");
		td.style.cssText = "border: none; margin: 0px; vertical-align: middle";
		//number field
		td.appendChild(document.createTextNode("<?php echo __("Number"); ?> "));
		var input = document.createElement("INPUT");
		input.type = "text";
		input.style.cssText = "width: 5em";
		input.id = "FolioNumber"+index;
		input.name = "FolioNumber"+index;
		input.value = ""+(index+1);
		input.onchange = buildCheckFolioNumberCall(input);
		td.appendChild(input);
		//renumber button
		var tmp = document.createElement("DIV");
		tmp.innerHTML = "<?php echo __("Renumber"); ?> &darr;";
		var renumber = document.createElement("INPUT");
		renumber.type = "button";
		renumber.style.fontSize = "small";
		renumber.value = (tmp.textContent || tmp.innerText);
		renumber.onclick = buildRenumberFoliosCall(input, index);
		td.appendChild(document.createTextNode(" "));
		td.appendChild(renumber);
		//visibility checkbox
		input = document.createElement("INPUT");
		input.type = "checkbox";
		input.value = "true";
		input.id = "FolioPublic"+index;
		input.name = "FolioPublic"+index;
		td.appendChild(createSpace());
		td.appendChild(input);
		td.appendChild(document.createTextNode(" <?php echo __("Public"); ?>"));
		
		tr.appendChild(td);
		return tr;
	}

	//refresh the table of folios ready for import
	var nSelectedFilesForImport = 0;
	function showSelectedFiles()
	{
		var selectFolioFiles = document.getElementById('selectFolioFiles');
		var selectedFilesDiv = document.getElementById('selectedFilesDiv');
		while (selectedFilesDiv.childNodes.length > 0)
			selectedFilesDiv.removeChild(selectedFilesDiv.childNodes[0]);

		var urls = parseURLs();
		var len = urls.length+selectFolioFiles.files.length;
		//folio entries from files
		for (var i=0;i<selectFolioFiles.files.length;i++)
		{
			selectedFilesDiv.appendChild(buildFolioMargin());
			selectedFilesDiv.appendChild(buildFolioEntry(selectFolioFiles.files[i].name, i, len));
		}
		//folio entries from urls
		for (var i=0;i<urls.length;i++)
		{
			selectedFilesDiv.appendChild(buildFolioMargin());
			selectedFilesDiv.appendChild(buildFolioEntry(urls[i], selectFolioFiles.files.length+i, len));
		}

		nSelectedFilesForImport = len;
		//show the import settings if there are any entries
		document.getElementById('importOptionsRow').style.display = len > 0 ? "table-row" : "none";
	}
	//readies the import folio frame when the selected source changes
	function updateImportSource(id, name)
	{
		document.getElementById('folioSourceId').value = id;
		document.getElementById('folioSourceName').value = name;
		if (id == "")
		{
			if (document.getElementById('noSourceSelectedRow') != null)
			{
				document.getElementById('noSourceSelectedRow').style.display = "table-row";
				document.getElementById('importOptionsRow').style.display = "none";
				document.getElementById('selectFolioFilesRow').style.display = "none";
				document.getElementById('selectedFilesDivRow').style.display = "none";
				document.getElementById('submitImportFoliosRow').style.display = "none";
			}
		}
		else
		{
			if (document.getElementById('noSourceSelectedRow') != null)
			{
				document.getElementById('noSourceSelectedRow').style.display = "none";
				document.getElementById('importOptionsRow').style.display = nSelectedFilesForImport > 0 ? "table-row" : "none";
				document.getElementById('selectFolioFilesRow').style.display = "table-row";
				document.getElementById('selectedFilesDivRow').style.display = "table-row";
				document.getElementById('submitImportFoliosRow').style.display = "table-row";
			}
		}
	}
</script>

<!-- Import folio frame layout -->
<input name="folioSourceId" id="folioSourceId" type="hidden"/>
<input name="folioSourceName" id="folioSourceName" type="hidden"/>

<table id="importFoliosTable" style="margin-bottom: 0px"><tr><td class="gteSectionCell">
<table class="gteSection" style="margin-bottom: 0px">
	<tr><td id="importFolios" class="gteHeader" style="background: slateblue"><b><?php echo __("Create new folios"); ?></b></td></tr>
	<?php
		//if there is no work, show message (TODO: can't happen?)
		if ($workName == null)
		{
	?>
	<tr><td style="border: none">
		<div style="color: red; text-align: center;"><?php echo __("Before importing folios, the work described by the genetic table must be defined. This field is located under the tab <i>Item Type Metadata</i>."); ?></div>
	</td></tr>
	<?php 
		}
		else
		{
	?>
	<tr id="noSourceSelectedRow"><td style="border: none">
		<div><?php echo __("Select a source to import folios"); ?></div>
	</td></tr>
	
	<!-- Button for selecting files and text area for typing urls -->
	<tr id="selectFolioFilesRow" style="display: none"><td style="border: none">
		<div style="border: none"><small><?php echo __("Select images in order to create new folios. You can choose them from your computer, by providing a link or both."); ?></small></div><br/>
		<div class="gteSubHeader"><?php echo __("Image selection"); ?></div><br/>
		<?php echo __("Files"); ?>&nbsp;&nbsp;&nbsp;
		<input id="selectFolioFiles" name="selectFolioFiles[]" type="file" multiple onchange="showSelectedFiles();" accept="image/*"/><br/><br/>
		<?php echo __("URL list"); ?>&nbsp;&nbsp;&nbsp;
		<textArea id="selectFolioURLs" name="selectFolioURLs" onchange="showSelectedFiles();" style="resize: none" rows="3"></textArea><br/>
		<div><small><i><?php echo __("Space separated list of URLs"); ?></i></small></div><br/>
	</td></tr>
	
	<!-- Import options -->
	<tr id="importOptionsRow" style="display: none"><td style="border: none">
		<div class="gteSubHeader"><?php echo __("Import options"); ?></div><br/>
		<input id="importOptionsFrontBack" type="checkbox"></input>
		<?php echo __("Front and back"); ?>
		&nbsp;&nbsp;&nbsp;
		<?php echo __("Back page suffix"); ?>
		<input id="importOptionsBackSuffix" type="text" maxLength="5" style="width: 5em"></input>
		&nbsp;&nbsp;&nbsp;
		<?php echo __("Make folios "); ?>
		<input type="button" value="<?php echo __("public"); ?>" onclick="changeFolioVisibility(true);">
		<input type="button" value="<?php echo __("private"); ?>" onclick="changeFolioVisibility(false);">
		<br/><br/><br/>
		<div class="gteSubHeader"><?php echo __("List of folios to create"); ?></div>
	</td></tr>
	
	<!-- Area where folios ready for import are shown (filled by scripts) -->
	<tr id="selectedFilesDivRow" style="display: none"><td style="border: none; margin: 0px; padding: 0px">
		<table id="selectedFilesDiv">
		</table>
	</td></tr>
	
	<!-- Save button -->
	<tr id="submitImportFoliosRow" style="display: none"><td style="border: none; text-align: center">
		<input type="button" id="submitImportFolios" value="<?php echo __("Save"); ?>" onClick="document.getElementsByName('submit')[0].click();" />
	</td></tr>
	<?php
		}
	?>
</table>
</td></tr></table>
<script>
	"use strict";
	var importFolios = document.getElementById("importFolios");
	var importFoliosBorder = importFolios.style.background;
	var importFoliosBorderHL = "lightblue";
	//make the import folio area blink
	function blinkImportFolios()
	{
		importFolios.style.background = importFoliosBorderHL; 
		setTimeout(function() {importFolios.style.background = importFoliosBorder;}, 300);
		setTimeout(function() {importFolios.style.background = importFoliosBorderHL;}, 600);
		setTimeout(function() {importFolios.style.background = importFoliosBorder;}, 900);
		setTimeout(function() {importFolios.style.background = importFoliosBorderHL;}, 1200);
		setTimeout(function() {importFolios.style.background = importFoliosBorder;}, 1500);
	}
</script>

<!-- Import a Flaubert DB via PHP. TODO: deprecated, now handled via async javascript to avoid timeouts during largs imports - see after

<br/>
<input type="hidden" id="flaubert2omeka" name="flaubert2omeka" value="false"/>

<table style="margin-bottom: 0px"><tr><td class="gteSectionCell">
<table class="gteSection" style="margin-bottom: 0px">
	<tr><td class="gteHeader"  style="background: slateblue" colspan=2><b><?php echo __("Import of a Flaubert DB"); ?></b></td></tr>
	<?php
		if ($workName== null)
		{
	?>
	<tr><td style="border: none">
		<div style="color: red; text-align: center;"><?php echo __("Before importing folios, the work described by the genetic table must be defined. This field is located under the tab <i>Item Type Metadata</i>."); ?></div>
	</td></tr>
	<?php 
		}
		else
		{
	?>
	<tr><td style="border: none" colspan=2>&nbsp;</td></tr>
	<tr style="border: none"><td style="border: none"><?php echo __("Path"); ?></td><td style="border: none; width: 75%">
		<input type="text" style="width: 100%" id="flaubertPath" name="flaubertPath" value=""/>
		<div><small><i><?php echo __("Absolute server path of the directory containing the XML and images exported from a Flaubert database"); ?><br/>
		(<?php echo __("example:") ?> /home/john/fdb, C:&#92;Users&#92;john&#92;Documents&#92;fdb)
		</i></small></div>
	</td></tr>
	<tr style="border: none"><td style="border: none"><?php echo __("XML"); ?></td><td style="border: none">
		<input type="text" style="width: 100%" id="flaubertXml" name="flaubertXml" value=""/>
		<div><small><i><?php echo __("Name of the XML file"); ?></i></small></div>
	</td></tr>
	<tr><td colspan=2 style="border: none; text-align: center">
		<input type="button" value="<?php echo __("Import"); ?>" onClick="document.getElementById('flaubert2omeka').value='true'; document.getElementsByName('submit')[0].click();" />
	</td></tr>
	<?php
		}
	?>
</table>
</td></tr></table -->

<script>
/**
 * Import of a folio DB
 */
//get the path from a file object
function filePath(file)
{
	if (file.webkitRelativePath)
		return file.webkitRelativePath;
	if (file.mozRelativePath)
		return file.mozRelativePath;
	if (file.relativePath)
		return file.relativePath;
	return "???";
}
//insert a file into the filesByName array chain by breaking down the file's path
//all arrays are associative maps with folders for keys and the file names as leaves
function insertFile(file)
{
	var path = filePath(file).split("/");
	var cur = filesByName;
	for (var i=1;i<path.length;i++)
	{
		if (i == path.length-1)
			cur[path[i]] = file;
		else
		{
			if (typeof(cur[path[i]]) === "undefined")
				cur[path[i]] = {};
			cur = cur[path[i]];
		}
	}
}
//get the file object from an image name (links names from the XML to files in the selected folder 
function readImage(folder, image)
{
	if (typeof(filesByName[folder]) === "undefined")
		return null;
	var images = filesByName[folder];
	if (typeof(images["jpg"]) !== "undefined" && typeof(images["jpg"][image]) !== "undefined")
		return images["jpg"][image];
	if (typeof(images[image]) !== "undefined")
		return images[image];
	return null;
}
//array of the found XMLs
var importXmls = [];
//map of file paths to file objects (folder names are the keys in the map)
var filesByName = {};
//display the XMLs after the user selects a local folder
function readLocalDB()
{
	importXmls = [];
	filesByName = {};
	var files = document.getElementById("flaubertDBInput").files;
	//insert all the selected files into the file map and detect XML files
	for (var i=0;i<files.length;i++)
	{
		insertFile(files[i]);
		if (files[i].name.endsWith(".xml"))
			importXmls.push(files[i]);
	}

	var xmlsDiv = document.getElementById("flaubertDBXmls");
	//the links for each XML initiate the import
	for (var i=0;i<importXmls.length;i++)
		xmlsDiv.innerHTML += "&nbsp;<a onclick='doImport(importXmls["+i+"]);' style='cursor: pointer'>"+importXmls[i].name+"</a>&nbsp;";
}
//default colors
var colors =
[
	"616183", "54536f", "45425c", "3b3b49", "2f2f3a", "cc7854", "a5613e", "924d3b", "693629", "652828", "458b74", "376f5c", "295345", "1b372e", "142922",
	"000000", "380356", "6b0078", "93009c", "b000b2", "a06e6e", "906363", "805858", "604242", "4c3434", "445626", "52682d", "6e8b3d", "7c9d45", "96b85d"
];
var colorCnt = 0;
var doc = null;
//maps source names (as they appear in the XML file) to source ids (in the DB)
var sourceMap = {};
//maps folios (ids as they appear in the XML file) to folios (ids as they appear in the DB)
var folioMap = {};
//maps fragments (ids as they appear in the XML file) to fragments (ids as they appear in the DB)
var fragmentMap = {};
//maps tag names (as they appear in the XML file) to tags (ids as they appear in the DB)
var tagMap = {};
var sourceCnt = 0;
//start the import of an XML file
function doImport(file)
{
	sourceMap = {};
	//the xml doc
	doc = null;
	sourceCnt = 0;
	//xml reader
	var reader = new FileReader();
	reader.onload = function(e)
	{
		//disable async requests: the page is blocked, one request at a time, no race conditions server side
		gtUseAsynRequests = false;
		doc = new DOMParser().parseFromString(e.target.result, "text/xml");
		//get arrays for the xml tags
		var flaubert = doc.getElementsByTagName("Flaubert")[0];
		var help = flaubert.getElementsByTagName("Help");
		var footer = flaubert.getElementsByTagName("Footer");
		var tables = flaubert.getElementsByTagName("Table");
		var sources = flaubert.getElementsByTagName("Source");
		//add transcription help to omeka
		writeWorkHelp(<?php echo $item->id; ?>, help[0].textContent, null, null);
		//TODO: unused?		
		var fragsOnly = false;//document.getElementById("importFragmentsOnly").checked;
		if (!fragsOnly)
		{
			//for each table in the xml
			for (var i=0;i<tables.length;i++)
			{
				//the work name is already defined in omeka, we append to it the xml table name
				var name = "<?php echo getWorkTitle($this, $item->id); ?> - "+tables[i].getAttribute("name");
				//tags (groups) in the table
				var tags = tables[i].getAttribute("etats").split(" ");
				//create the associated table in omeka
				writeNewTable(<?php echo $item->id; ?>, name, function(tableId)
				{
					//add table footer
					writeTableFooter(tableId, footer[0].textContent, null, null);
					//add all the tags
					for (var j=0;j<tags.length;j++)
					{
						writeNewTag(tableId, function(tagId)
						{
							tagMap[tags[j]] = tagId;
							//tag is created, add the attributes
							writeUpdateTag(<?php echo $item->id; ?>, tagId, tags[j], tags[j], 1, colors[(colorCnt++)%colors.length], null, function (error) {console.log(error);});
						}, 
						function (error) {console.log(error);});
					}
				}, 
				function (error) {console.log(error);});
			}
			//create the sources in omeka
			for (var i=0;i<sources.length;i++)
			{
				var name = sources[i].getAttribute("name");
				writeNewSource(<?php echo $item->id; ?>, name, 
					buildWriteSourceOnResult(name, i, sources.length), 
					function (error) {console.log(error);});
			}
			//create the chapters
			var chapters = flaubert.getElementsByTagName("Chapitre");
			for (var i=0;i<chapters.length;i++)
			{
				var chapter = chapters[i];
				writeNewChapter(<?php echo $item->id; ?>, buildWriteChapterOnResult(chapter), function(error) {console.log(error);});
			}
		}
		//import the folios
		doImportFolios(fragsOnly);
		//import the fragments
		doImportFragments();
		//reactivate async requests
		gtUseAsynRequests = true;
	};
	reader.readAsText(file);
}
function buildWriteChapterOnResult(chapter)
{
	return function(chapterId)
	{
		//once the chapter is created, add the attributes
		writeUpdateChapter(<?php echo $item->id; ?>, chapterId, chapter.getAttribute("nom"), chapter.getAttribute("fin_page"), 
			function() {}, 
			function(error) {console.log(error);});
	};
}
function buildWriteSourceOnResult(name, index, nSources)
{
	return function (sourceId)
	{
		//once the source is created, add it to the map and add the attributes
		sourceMap[name] = sourceId; 
		writeUpdateSource(sourceId, index, name, colors[(colorCnt++)%colors.length], null, function(error) {console.log(error);});
	};
}

//read folios from the xml doc
function doImportFolios(fragsOnly)
{
	folioMap = {};
	folioCnt = 0;
	var flaubert = doc.getElementsByTagName("Flaubert")[0];
	var folios = flaubert.getElementsByTagName("Folio");
	//for each folio in the xml
	for (var i=0;i<folios.length;i++)
	{
		var folio = folios[i];
		//if the source is unknown (bad xml)
		if (!fragsOnly && typeof(sourceMap[folio.getAttribute("source")]) === "undefined")
			continue;
		//folio number on xml fields
		var number = folio.getAttribute("folio")+
			(folio.getAttribute("recto_verso") != "null" ? folio.getAttribute("recto_verso") : "")+
			(folio.getAttribute("emplacement") != "null" ? folio.getAttribute("emplacement").toLowerCase() : "");

		if (!fragsOnly)
		{
			//add the folio to omeka
			writeNewFolio(<?php echo $item->id; ?>, 
				sourceMap[folio.getAttribute("source")], number,
				folio.hasAttribute("incipit") ? folio.getAttribute("incipit") : "",
				folio.hasAttribute("explicit") ? folio.getAttribute("explicit") : "",
				folio.hasAttribute("notes") && folio.getAttribute("notes") != "null" ? folio.getAttribute("notes") : "",
				folio.hasAttribute("censure") && folio.getAttribute("censure") != "null" ? folio.getAttribute("censure") : "",
				buildWriteFolioOnResult(folio), 
				function (error) {console.log(error);});
		}
		else
		{
			//if no creation, just read id to go to fragments
			readFolioId(<?php echo $item->id; ?>, folio.getAttribute("source"), number, 
				buildReadFolioIdOnResult(folio), 
				function (error) {console.log(error);});
		}
		console.log("Imported folio "+(i+1)+" of "+folios.length);
	}
}
//0 padding
function formatNumberLength(num, length)
{
    var r = "" + num;
    while (r.length < length)
        r = "0" + r;
    return r;
}
//callback when reading fragments only
function buildReadFolioIdOnResult(folio)
{
	return function (folioId)
	{
		//add folio to map
		if (folioId >= 0)
			folioMap[folio.getAttribute("id")] = folioId;
		//otherwise log error
		else
		{
			var number = folio.getAttribute("folio")+
				(folio.getAttribute("recto_verso") != "null" ? folio.getAttribute("recto_verso") : "")+
				(folio.getAttribute("emplacement") != "null" ? folio.getAttribute("emplacement").toLowerCase() : "");
			console.log("Can't find folio "+folio.getAttribute("source")+" "+number);
		}
	};
}
//callback when folio is created in omeka
function buildWriteFolioOnResult(folio)
{
	return function (folioId)
	{
		//add folio to map
		folioMap[folio.getAttribute("id")] = folioId;

		//TODO: useless escapes?
		//str_replace("&quot;", "\"", str_replace("&lt;", "<", str_replace("&amp;", "&", trim($xml->readString()))))
		
		//get transcription in xml
		var trans = folio.textContent.trim().replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&quot;/g, "\"");
		//write to omeka
		writeUpdateFolioTranscription(folioId, trans, 
			function() {}, 
			function (error) {console.log(error);});

		//if the digitized image is a link (nakala)
		if (folio.hasAttribute("url"))
		{
			//add link to omeka
			writeUpdateFolioAddLink(folioId, folio.getAttribute("url"), 
				function() {}, 
				function (error) {console.log(error);});
		}
		//otherwise look for file
		else
		{
			var suffix = (folio.getAttribute("recto_verso") != "null" ? folio.getAttribute("recto_verso") : "")+
				(folio.getAttribute("emplacement") != "null" ? folio.getAttribute("emplacement").toLowerCase() : "");
			var src = folio.getAttribute("source");
			var folder = src.indexOf(" ") < 0 ? src : src.substring(0, src.indexOf(" "));
			//try to get file with 3 padding on page number
			var file = readImage(folder, formatNumberLength(folio.getAttribute("folio"), 3)+suffix+".jpg");
			//if no file, try 4
			if (file == null)
				file = readImage(folder, formatNumberLength(folio.getAttribute("folio"), 4)+suffix+".jpg");
			//no file
			if (file == null)
				console.log("Couldn't find image for "+src+" - "+folio.getAttribute("folio")+suffix);
			//TODO: skip images on localhost for debug, comment out for normal behavior
			else if (window.location.href.indexOf("localhost") >= 0)
				console.log("Skipping image upload on localhost for "+src+" - "+folio.getAttribute("folio")+suffix);
			//otherwise add imega to omeka
			else
			{
				writeUpdateFolioAttachImage(folioId, file, 
					function() {}, 
					function (error) {console.log(error);});
			}
		}		
	};
}
//import fragments from xml
function doImportFragments()
{
	var flaubert = doc.getElementsByTagName("Flaubert")[0];
	var fragments = flaubert.getElementsByTagName("Fragment");
	//for each fragment in the xml
	for (var i=0;i<fragments.length;i++)
	{
		var fragment = fragments[i];
		//unknown folio
		if (typeof(folioMap[fragment.getAttribute("id_folio")]) === "undefined")
			continue;
		//add fragments to omeka
		writeNewFragment(<?php echo $item->id; ?>, folioMap[fragment.getAttribute("id_folio")],
			buildWriteFragmentOnResult(fragment),
			function (error) {console.log(error);});
		console.log("Created fragment "+(i+1)+" of "+fragments.length);
	}
	doUpdateFragments();
}
//once fragment is created, add to map
function buildWriteFragmentOnResult(fragment) {return function (fragmentId) {fragmentMap[fragment.getAttribute("id")] = fragmentId;};}
//when tag (group) is created, add to map and update attributes
function buildWriteTagOnResult(tag)
{
	return function (tagId) 
	{
		tagMap[tag] = tagId;
		writeUpdateTag(<?php echo $item->id; ?>, tagId, tag, tag, 1, colors[(colorCnt++)%colors.length], null, function (error) {console.log(error);});
	};
}
//update fragment attributes
function doUpdateFragments()
{
	var flaubert = doc.getElementsByTagName("Flaubert")[0];
	var fragments = flaubert.getElementsByTagName("Fragment");
	//for each fragment in the xml
	for (var i=0;i<fragments.length;i++)
	{
		var fragment = fragments[i];
		//if unknown fragment
		if (typeof(fragmentMap[fragment.getAttribute("id")]) === "undefined")
			continue;
		//update fragment attributes in omeka
		writeUpdateFragment(<?php echo $item->id; ?>, fragmentMap[fragment.getAttribute("id")], 
			fragment.getAttribute("debut_page"), fragment.getAttribute("debut_ligne"),
			fragment.getAttribute("fin_page"), fragment.getAttribute("fin_ligne"),
			fragment.getAttribute("emplacement") == "null" ? "" : fragment.getAttribute("emplacement"),
			fragment.getAttribute("present") == "o" ? "false" : "true",
			typeof(fragmentMap[fragment.getAttribute("f_origine")]) !== "undefined" ? fragmentMap[fragment.getAttribute("f_origine")] : "",
			typeof(fragmentMap[fragment.getAttribute("f_prec_vertical")]) !== "undefined" ? fragmentMap[fragment.getAttribute("f_prec_vertical")] : "",
			null,
			function (error) {console.log(error);});
		//write tag-fragment association
		writeNewTagFragment(<?php echo $item->id; ?>, tagMap[fragment.getAttribute("etat")], fragmentMap[fragment.getAttribute("id")], 
			null, function (error) {console.log(error);});
		console.log("Updated fragment "+(i+1)+" of "+fragments.length);
	}
}
</script>
<table style="margin-bottom: 0px"><tr><td class="gteSectionCell">
<!-- Import flaubert DB area -->
<table class="gteSection" style="margin-bottom: 0px">
	<tr><td class="gteHeader"  style="background: slateblue" colspan=2><b><?php echo __("Import of a local Flaubert DB"); ?></b></td></tr>
	<tr><td style="border: none">&nbsp;</td></tr>
	<tr style="border: none"><td style="border: none">
		<div class="gteSubHeader"><?php echo __("Folder selection"); ?></div><br/>
		<input type="file" id="flaubertDBInput" mozdirectory="" webkitdirectory="" directory="" onchange="readLocalDB();" />
		<br/><br/>
		<span style="display: none"><input type="checkbox" id="importFragmentsOnly"><?php echo __("Import fragments only"); ?></input></span>
	</td></tr>
	<tr style="border: none"><td style="border: none">
		<div class="gteSubHeader"><?php echo __("File to import"); ?></div><br/>
		<!-- Area with the xmls in the uploaded folder -->
		<div id="flaubertDBXmls"></div>
		<div id="flaubertDBStatus"></div>
	</td></tr>
</table>
</td></tr></table>
