<script>
'use strict';

/**
 * An image viewer with zooming and scrolling controls and a miniature view
 */

function buildGtImageViewerOnResize(viewer)
{
	return function() {viewer.refreshCanvas();};
}

//Creates a new viewer which is appended to a parent HTML element
function GTImageViewer(parent)
{
	this.canvas = document.createElement("CANVAS");
	this.canvas.style.width = "100%";
	this.canvas.style.height = "100%";
	
	parent.appendChild(this.canvas);
	window.addEventListener("resize", buildGtImageViewerOnResize(this));
	var viewer = this;

	//mouse event listeners are forwarded to member functions
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	this.useDragEvents = isChrome;
	this.canvas.addEventListener("mouseenter", function(e) {viewer.setShowOverview(true);});
	this.canvas.addEventListener("mouseleave", function(e) {viewer.setShowOverview(false); viewer.zooming = false; if (!this.useDragEvents) viewer.dragging = false;});
	this.canvas.addEventListener("mousedown", function(e) {viewer.mouseDown(e, false);});
	this.canvas.addEventListener("mousemove", function(e) {viewer.setShowOverview(true); viewer.mouseMove(e, false);});
	this.canvas.addEventListener("mouseup", function(e) {viewer.mouseUp(e, false);});
	if (this.useDragEvents)
	{
		this.canvas.draggable = true;
		this.canvas.addEventListener("dragstart", function(e) {viewer.mouseDown(e, true);});
		this.canvas.addEventListener("drag", function(e) {viewer.mouseMove(e, true);});
		this.canvas.addEventListener("dragend", function(e) {viewer.mouseUp(e, true);});
	}
	
 	this.canvas.addEventListener("wheel", function(e) {viewer.wheel(e);});

 	//the image being shown (can be null)
	this.img = null;
	//image coordinates of the center of the view
	this.x0 = 0;
	this.y0 = 0;
	//the zoom multiplier of the view
	this.scale = 1;

	//indicates if the current mouse drag is zooming (initiated with right click)
	this.zooming = false;
	//last mouse y-coordinate for zooming 
	this.zy = 0;
	//indicates if the current mouse drag is scrolling (initiated with left click)
	this.dragging = false;
	//last mouse coordinates for scrolling
	this.dx = 0;
	this.dy = 0;

	//indicates whether the overview should be displayed (the overview is hidden if the full image fits in the view)
	this.showOverview = false;
	//used to fade the overview in and out
	this.overviewAlpha = 0;
	this.maxPreviewEmSize = 10*parseFloat(getComputedStyle(this.canvas).fontSize);
	this.maxPreviewPropSize = .25;

	//listeners of this component
	this.listeners = [];
	this.dummyImg = document.createElement("IMG");
}

GTImageViewer.prototype.setShowOverview = function(b)
{
	if (b == this.showOverview)
		return;
	this.showOverview = b;
	this.refreshCanvas(false);
}

//change the zoom by "amount" centered on "x" and "y"
GTImageViewer.prototype.zoom = function(amount, x, y)
{
	var k = Math.pow(1.1, amount);
	var dx = x-.5*this.canvas.width;
	var dy = y-.5*this.canvas.height;
	//TODO: bad formula! 
// 	this.x0 -= dx*this.scale*(k-1);
// 	this.y0 -= dy*this.scale*(k-1);
	this.scale *= k;
	this.refreshCanvas();
}

//react to a mouse wheel event
GTImageViewer.prototype.wheel = function(e)
{
	//stops default event handler
	e.preventDefault();
	var zx = .5*this.canvas.width, zy = .5*this.canvas.height;
 	if (typeof e.clientX != "undefined")
 		{zx = e.clientX; zy = e.clientY;}
	this.zoom(-.01*e.deltaY*Math.pow(20, e.deltaMode), zx, zy);
}
//react to a mouse press
GTImageViewer.prototype.mouseDown = function(e, isDrag)
{
	//if left button, this is a scrolling interaction
	if (e.button == 0 && (!this.useDragEvents || isDrag))
	{
		this.dragging = true;
		this.dx = e.screenX;
		this.dy = e.screenY;
		//if the browser supports drag'n'drop, try to prevent a custom cursor
		if (typeof e.dataTransfer != "undefined")
		{
			e.dataTransfer.effectAllowed = "none";
			e.dataTransfer.clearData();
			e.dataTransfer.setDragImage(this.dummyImg, 0, 0);
		}
	}
	//otherwise, it is a zoom interaction
	else if (e.button == 2 && (!this.useDragEvents || !isDrag))
	{
		this.zooming = true;
		this.zy = e.screenY;
	}
}
//react to a mouse move in case of a scrolling or a zoom interaction
GTImageViewer.prototype.mouseMove = function(e, isDrag)
{
	if (this.dragging)
	{
		var tx = e.screenX;
		var ty = e.screenY;
		if (tx == 0 && ty == 0) return;
		this.x0 -= (tx-this.dx)/this.scale;
		this.y0 -= (ty-this.dy)/this.scale;
		this.dx = tx;
		this.dy = ty;
		this.refreshCanvas();
	}
	else if (this.zooming)
	{
		var ty = e.screenY;
		this.zoom(-.1*(ty-this.zy), .5*this.canvas.width, .5*this.canvas.height);
		this.zy = ty;
		this.refreshCanvas();
	}
}
//react to a release of a mouse button which will end interactions
GTImageViewer.prototype.mouseUp = function(e, isDrag)
{
	if (e.button == 0 && (!this.useDragEvents || isDrag))
		this.dragging = false;
	else if (e.button == 2 && (!this.useDragEvents || !isDrag))
	{
		e.preventDefault();
		this.zooming = false;
	}
}
//when the component loads, set the scale for an optimal fit
function buildGtImageViewerOnLoad(viewer, img)
{
	return function()
	{
		if (viewer.img == img)
		{
			var iw = viewer.img.naturalWidth;
			var ih = viewer.img.naturalHeight;
			if (iw > 0 && ih > 0)
			{
				//centers the view
				viewer.x0 = .5*iw;
				viewer.y0 = .5*ih;
				//set the scale for the largest fit
				viewer.scale = Math.min(viewer.canvas.width/iw, viewer.canvas.height/ih);
			}
			viewer.refreshCanvas();
		}
	};
}
//sets the image URL
GTImageViewer.prototype.setSrc = function(src)
{
	this.img = document.createElement("IMG");
	this.img.onload = buildGtImageViewerOnLoad(this, this.img);
	this.img.src = src;
}
//redraw the canvas and optionally notify listeners of the viewer
GTImageViewer.prototype.refreshCanvas = function(notify)
{
	//has the client area changed?
	if (this.canvas.clientWidth != this.canvas.width || this.canvas.clientHeight != this.canvas.height)
	{
		this.canvas.width = this.canvas.clientWidth;
		this.canvas.height = this.canvas.clientHeight;
	}

	var g = this.canvas.getContext('2d');
	//clear the background and add a border
	g.fillStyle = "ghostwhite";
	g.fillRect(0, 0, this.canvas.width, this.canvas.height);
	g.strokeStyle = "lightgray";
	g.strokeRect(0, 0, this.canvas.width, this.canvas.height);

	if (this.img == null || this.img.naturalWidth == 0 || this.img.naturalHeight == 0)
		return;

	g.imageSmoothingEnabled = false;
	g.webkitImageSmoothingEnabled = false;

	//draw the scaled and scrolled image in the canvas
	var cw = this.canvas.width;
	var ch = this.canvas.height;
	var iw = this.img.naturalWidth;
	var ih = this.img.naturalHeight;
	this.scale = Math.max(.5*Math.min(cw/iw, ch/ih), Math.min(20, this.scale));
	this.x0 = Math.max(0, Math.min(iw, this.x0));
	this.y0 = Math.max(0, Math.min(ih, this.y0));
	var x = this.x0*this.scale, y = this.y0*this.scale;
	g.drawImage(this.img, .5*cw-x, .5*ch-y, iw*this.scale, ih*this.scale);

	//draw the overview if alpha is non zero
	if (this.overviewAlpha > .01)
	{
		g.globalAlpha = this.overviewAlpha;
		//compute the area of the overview
		var pew = iw > ih ? this.maxPreviewEmSize : this.maxPreviewEmSize*iw/ih;
		var peh = iw > ih ? this.maxPreviewEmSize*ih/iw : this.maxPreviewEmSize;
		var maxw = this.maxPreviewPropSize*cw;
		var maxh = this.maxPreviewPropSize*ch;
		if (pew > maxw) {pew = maxw; peh = ih*pew/iw;}
		if (peh > maxh) {peh = maxh; pew = iw*peh/ih;}
		
		//draw the overview and a border
		g.drawImage(this.img, 0, 0, pew, peh);
		g.strokeStyle = "#454545";
		g.strokeRect(0, 0, pew, peh);

		//compute the area of the visible view within the overview
		var lx = Math.max(0, (this.x0-.5*cw/this.scale)*pew/iw);
		var ly = Math.max(0, (this.y0-.5*ch/this.scale)*peh/ih);
		var rx = Math.min(pew, (this.x0+.5*cw/this.scale)*pew/iw);
		var ry = Math.min(peh, (this.y0+.5*ch/this.scale)*peh/ih);
		//draw the visible area outline
		g.strokeStyle = "#ff0000";
		g.strokeRect(lx, ly, rx-lx, ry-ly);
		g.globalAlpha = 1;
	}

	//notify listeners
	if (typeof notify == "undefined" || notify)
		for (var i=0;i<this.listeners.length;i++)
			this.listeners[i](this);
	//or update the alpha of the overview and schedule a redraw if necessary 
	else
	{
		if ((this.showOverview && this.overviewAlpha < 1) || (!this.showOverview && this.overviewAlpha > 0))
		{
			this.overviewAlpha = Math.max(0, Math.min(1, this.overviewAlpha+(this.showOverview ? .2 : -.2)));
			var viewer = this;
			setTimeout(function() {viewer.refreshCanvas(false);}, 30);
		}
	}
}

</script>