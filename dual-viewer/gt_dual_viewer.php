<?php
require_once ("gt_html_viewer.php");
/**
 * A componnent for displaying two image viewers side by side, the image cell and the transcription cell.
 * The transcription cell can be toggled to display regular HTML with no rendering or an SVG rendered version.
 * A hideable toolbar is displayed at the bottom of the component.
 */
?>
<style>
.toolbarButtonCell {border: none; margin: 0px; padding: 0em 0.5em 0em 0.5em; cursor: default; vertical-align: middle;}
.toolbarButton {
	border-radius: 1em; 
	display: table-cell; margin: 0px; padding: 0px; vertical-align: middle;}

/* Tooltip text */
.toolbarButton .tooltiptext {
    visibility: hidden;
    width: 240px;
    background-color: #555;
    color: #fff;
    text-align: center;
    padding: 5px 0;
    border-radius: 6px;

    /* Position the tooltip text */
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;

    /* Fade in tooltip */
    opacity: 0;
    transition: opacity 0.3s;
}

/* Show the tooltip text when you mouse over the tooltip container */
.toolbarButton:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
}
</style>

<script>
//Fullscreen callbacks
/** @namespace THREEx */
var THREEx		= THREEx 		|| {};
THREEx.FullScreen	= THREEx.FullScreen	|| {};
THREEx.FullScreen.available	= function() {return this._hasWebkitFullScreen || this._hasMozFullScreen;}
THREEx.FullScreen.activated	= function()
{
	if (this._hasWebkitFullScreen) {return document.webkitIsFullScreen;}
	else if (this._hasMozFullScreen) {return document.mozFullScreen;}
	else {console.assert(false);}
}
THREEx.FullScreen.request	= function(element)
{
	element	= element	|| document.body;
	if (this._hasWebkitFullScreen) {element.webkitRequestFullScreen();}
	else if (this._hasMozFullScreen) {element.mozRequestFullScreen();}
	else {console.assert(false);}
}
THREEx.FullScreen.cancel	= function()
{
	if (this._hasWebkitFullScreen) {document.webkitCancelFullScreen();}
	else if (this._hasMozFullScreen) {document.mozCancelFullScreen();}
	else {console.assert(false);}
}
THREEx.FullScreen._hasWebkitFullScreen	= 'webkitCancelFullScreen' in document	? true : false;	
THREEx.FullScreen._hasMozFullScreen	= 'mozCancelFullScreen' in document	? true : false;

'use strict';
/**
 * Creates a dual viewer that is appended to a parent HTML element. Since SVG rendering will not work on all browsers, a noSvg option prevents it.
 */
function GTDualViewer(parent, noSvg)
{
	this.noSvg = typeof(noSvg) != "undefined" && noSvg;
	this.fullscreen = false;
	this.onLayoutChanged = [];
	this.onFullscreen = [];

	//the top level element that is appended to the parent
	this.panel = document.createElement("DIV");
	this.panel.style.cssText = "width: 100%; height: 100%; border: none; padding: 0px; margin: 0px";
	parent.appendChild(this.panel);
	this.table = document.createElement("DIV");
	this.table.style.position = "relative";
	this.table.style.left = "0px";
	this.table.style.top = "0px";
	this.table.style.width = "100%";
	this.table.style.height = "100%";
	this.panel.appendChild(this.table);

	//the image cell
	this.imageCell = document.createElement("DIV");
	this.table.appendChild(this.imageCell);
	this.imageCell.style.position = "absolute";
	this.imageCell.style.border = "none";
	this.imageCell.style.margin = "0px";
	this.imageCell.style.padding = "0px";
	//the transcription cell
	this.transCell = document.createElement("DIV");
	this.table.appendChild(this.transCell);
	this.transCell.style.position = "absolute";
	this.transCell.style.border = "none";
	this.transCell.style.margin = "0px";
	this.transCell.style.padding = "0px";
	this.layout = 0;
	this.setupTable();

	//the toolbar
	this.toolbar = document.createElement("DIV");
	this.table.appendChild(this.toolbar);
	this.toolbar.style.textAlign = "center";
	this.toolbar.style.width = "100%";
	this.toolbar.style.position = "absolute";
	this.toolbar.style.left = "0px";
	this.toolbar.style.bottom = "0px";
	this.toolbar.style.border = "none";
	this.toolbar.style.margin = "0px";
	this.toolbar.style.padding = "0px";
	this.toolbar.style.backgroundColor = "transparent";

	var bs = "4em", ss = "3em";
	var onCol = "lightsteelblue", offCol = "rgba(0, 0, 0, .1)";
	/* the toolbar buttons:
	 *  - Hide/show toolbar. When hidden the toolbar collapses to this single button.
	 *  - Display the cells from left to right
	 *  - Display the cells from top to bottom
	 *  - Show the image cell only
	 *  - Show the transcription cell only
	 *  - Toggle the locking of cells together. If the transcription cell is rendering HTML, both views will use identical configuratioins for scrolling and zooming when locked.
	 *  - Toggle the transcription between regular display and rendered display
	 *  - Toggle fullscreen
	 */
	this.toolbar.innerHTML = 
		"<div style='border: none; margin: 0px; padding: 0px; width: 100%; background-color: rgba(255, 255, 255, .75); text-align: center'>"
			+"<table style='border: none; margin: auto; padding: 0px; width: initial; background-color: transparent'><tr>" 

			+"<td class='toolbarButtonCell'>"
				+"<div class='toolbarButton' style='width: "+ss+"; height: "+ss+"; background-color: "+onCol+"'>&larr;"
				+"<span class='tooltiptext'><?php echo __("Toggle toolbar"); ?></span>"
				+"</div></td>"

			+"<td class='toolbarButtonCell' style='visibility: hidden'></td>"
			
			+"<td class='toolbarButtonCell'>"
				+"<div class='toolbarButton' style='width: "+bs+"; height: "+bs+"; background-color: "+onCol+"'>&#128247;&nbsp;&#9778;"
				+"<span class='tooltiptext'><?php echo __("Horizontal layout"); ?></span>"
				+"</div></td>"
			+"<td class='toolbarButtonCell'>"
				+"<div class='toolbarButton' style='width: "+bs+"; height: "+bs+"; background-color: "+offCol+"'>&#128247;<br>&#9778;"
				+"<span class='tooltiptext'><?php echo __("Vertical layout"); ?></span>"
				+"</div></td>"
			+"<td class='toolbarButtonCell'>"
				+"<div class='toolbarButton' style='width: "+bs+"; height: "+bs+"; background-color: "+offCol+"'>&#128247;"
				+"<span class='tooltiptext'><?php echo __("Digitization only"); ?></span>"
				+"</div></td>"
			+"<td class='toolbarButtonCell'>"
				+"<div class='toolbarButton' style='width: "+bs+"; height: "+bs+"; background-color: "+offCol+"'>&#9778;"
				+"<span class='tooltiptext'><?php echo __("Transcription only"); ?></span>"
				+"</div></td>"
				
			+"<td class='toolbarButtonCell' style='visibility: hidden'></td>"
			
			+"<td class='toolbarButtonCell'>"
				+"<div class='toolbarButton' style='width: "+ss+"; height: "+ss+"; background-color: "+onCol+"'>&#128274;"
				+"<span class='tooltiptext'><?php echo __("Keep the digitization and the transcritption locked together (moving either will move the other if the transcription is displayed as an image)"); ?></span>"
				+"</div></td>"
			+"<td class='toolbarButtonCell'>"
				+"<div class='toolbarButton' style='width: "+ss+"; height: "+ss+"; background-color: "+onCol+"'><b>T</b>"
				+"<span class='tooltiptext'><?php echo __("Display the transcritption as text or image (not all browsers can display as image)"); ?></span>"
				+"</div></td>"
			+"<td class='toolbarButtonCell'>"
				+"<div class='toolbarButton' style='width: "+ss+"; height: "+ss+"; background-color: "+offCol+"'><span style='font-size: 200%'><b>&#9974;</b></span>"
				+"<span class='tooltiptext'><?php echo __("Toggle fullscreen"); ?></span>"
				+"</div></td>"

			+"</tr></table>"
		+"</div>";

	var viewer = this;
	//the buttons are fetched in the order of definition
	var buttons = this.toolbar.getElementsByClassName("toolbarButton");
	buttons[0].onclick = function() {viewer.toggleTools();};
	buttons[1].onclick = function() {buttons[viewer.layout+1].style.backgroundColor = offCol; viewer.layout = 0; viewer.setupTable(); this.style.backgroundColor = onCol;};
	buttons[2].onclick = function() {buttons[viewer.layout+1].style.backgroundColor = offCol; viewer.layout = 1; viewer.setupTable(); this.style.backgroundColor = onCol;};
	buttons[3].onclick = function() {buttons[viewer.layout+1].style.backgroundColor = offCol; viewer.layout = 2; viewer.setupTable(); this.style.backgroundColor = onCol;};
	buttons[4].onclick = function() {buttons[viewer.layout+1].style.backgroundColor = offCol; viewer.layout = 3; viewer.setupTable(); this.style.backgroundColor = onCol;};
	buttons[5].onclick = function() {viewer.lockViews = !viewer.lockViews; this.style.backgroundColor = viewer.lockViews ? onCol : offCol;};
	buttons[6].onclick = function() {viewer.toggleTrans(); this.style.backgroundColor = viewer.transCell.children[0] == viewer.transViewer.canvas ? offCol : onCol;};
	buttons[7].onclick = function() {viewer.toggleFullscreen(); this.style.backgroundColor = !THREEx.FullScreen.activated() ? onCol : offCol;};
	if (this.noSvg)
	{
		buttons[5].style.display = "none";
		buttons[6].style.display = "none";
	}

	//the collapsed toolbar replaces the normal toolbar when collapsed
	this.collaspsedToolbar = document.createElement("DIV");
	this.collaspsedToolbar.style.position = "absolute";
	this.collaspsedToolbar.style.left = "0px";
	this.collaspsedToolbar.style.bottom = "0px";
	this.collaspsedToolbar.style.border = "none";
	this.collaspsedToolbar.style.margin = "0px";
	this.collaspsedToolbar.style.padding = "0px";
	this.collaspsedToolbar.style.backgroundColor = "transparent";

	this.collaspsedToolbar.innerHTML = 
		"<div style='border: none; margin: 0px; padding: 0px; width: 100%; background-color: rgba(255, 255, 255, .75); text-align: center'>"
			+"<table style='border: none; margin: auto; padding: 0px; width: initial; background-color: transparent;'><tr>" 
			+"<td class='toolbarButtonCell'>"
				+"<div class='toolbarButton' style='width: "+ss+"; height: "+ss+"; background-color: "+offCol+"'>&rarr;</div></td>"
					+"</tr></table>"
		+"</div>";
	var collapsedButton = this.collaspsedToolbar.getElementsByClassName("toolbarButton");
	collapsedButton[0].onclick = function() {viewer.toggleTools();};

	//the two viewer components
	this.imageViewer = new GTImageViewer(this.imageCell);
	this.transViewer = new GTHTMLViewer(this.transCell);
	//an element to hold the transcription HTML
	this.transPlain = document.createElement("DIV");
	this.transPlain.style.cssText = "font-size: 100%";

	this.lockViews = true;
	this.imageViewer.listeners.push(function (canvas) {viewer.onCanvasRefreshed(canvas);});
	this.transViewer.listeners.push(function (canvas) {viewer.onCanvasRefreshed(canvas);});
	this.toggleTrans();
}

function isValidGtViewer(v) {return v.img != null && v.img.naturalWidth > 0 && v.img.naturalHeight > 0;}
//after a cell is refreshed, the other cell should be refreshed if they are locked
GTDualViewer.prototype.onCanvasRefreshed = function(from)
{
	if (!this.lockViews)
		return;
	var to = from == this.imageViewer ? this.transViewer : this.imageViewer;
	//are both viewers valid (they have something to display and have a non zero area)
	if (!isValidGtViewer(from) || !isValidGtViewer(to))
		return;
	var k = to.img.naturalWidth*1./from.img.naturalWidth;
	to.x0 = k*from.x0;
	to.y0 = k*from.y0;
	to.scale = from.scale/k;
	to.refreshCanvas(false);
}
//toggle the fullscreen state of the viewer
GTDualViewer.prototype.toggleFullscreen = function()
{
	//already fullscreen?
	if (!THREEx.FullScreen.activated())
	{
		//fullscreen is supported?
		if (THREEx.FullScreen.available())
		{
			//go fullscreen
			THREEx.FullScreen.request(this.panel);
			for (var i=0;i<this.onFullscreen.length;i++)
				this.onFullscreen[i](this, true);
		}
	}
	else
	{
		//exit fullscreen
		THREEx.FullScreen.cancel();
		for (var i=0;i<this.onFullscreen.length;i++)
			this.onFullscreen[i](this, false);
	}
}
//toggle the toolbar between full and collapsed states
GTDualViewer.prototype.toggleTools = function()
{
	if (this.toolbar.parentElement == this.table)
	{
		this.table.removeChild(this.toolbar);
		this.table.appendChild(this.collaspsedToolbar);
	}
	else
	{
		this.table.removeChild(this.collaspsedToolbar);
		this.table.appendChild(this.toolbar);
	}
}
//toggle the transcription between SVG rendering and regular display
GTDualViewer.prototype.toggleTrans = function()
{
	if (this.transCell.children[0] == this.transViewer.canvas)
	{
		this.transCell.removeChild(this.transViewer.canvas);
		this.transCell.style.overflow = "scroll";
		//this.transCell.style.whiteSpace = "nowrap";
		this.transCell.appendChild(this.transPlain);
	}
	else
	{
		this.transCell.removeChild(this.transPlain);
		this.transCell.style.overflow = "hidden";
		this.transCell.appendChild(this.transViewer.canvas);
		this.transViewer.refreshCanvas();
	}
}
//set the layout of the cells
GTDualViewer.prototype.setupTable = function()
{
	//Horizontal layout
	if (this.layout == 0)
	{
		this.imageCell.style.width = "50%";
		this.imageCell.style.height = "100%";
		this.imageCell.style.left = "0px";
		this.imageCell.style.top = "0px";
		this.transCell.style.width = "50%";
		this.transCell.style.height = "100%";
		this.transCell.style.left = "50%";
		this.transCell.style.top = "0px";
	}
	//Vertical layout
	else if (this.layout == 1)
	{
		this.imageCell.style.width = "100%";
		this.imageCell.style.height = "50%";
		this.imageCell.style.left = "0px";
		this.imageCell.style.top = "0px";
		this.transCell.style.width = "100%";
		this.transCell.style.height = "50%";
		this.transCell.style.left = "0px";
		this.transCell.style.top = "50%";
	}
	//Image cell only
	else if (this.layout == 2)
	{
		this.imageCell.style.width = "100%";
		this.imageCell.style.height = "100%";
		this.imageCell.style.left = "0px";
		this.imageCell.style.top = "0px";
		this.transCell.style.width = "0%";
		this.transCell.style.height = "0%";
		this.transCell.style.left = "0px";
		this.transCell.style.top = "0px";
	}
	//Transcription cell only
	else
	{
		this.imageCell.style.width = "0%";
		this.imageCell.style.height = "0%";
		this.imageCell.style.left = "0px";
		this.imageCell.style.top = "0px";
		this.transCell.style.width = "100%";
		this.transCell.style.height = "100%";
		this.transCell.style.left = "0px";
		this.transCell.style.top = "0px";
	}
	//refresh the cells
	if (typeof this.imageViewer != "undefined") this.imageViewer.refreshCanvas();
	if (typeof this.transViewer != "undefined") this.transViewer.refreshCanvas();
	for (var i=0;i<this.onLayoutChanged.length;i++)
		this.onLayoutChanged[i](this);
}
/**
 * Set the contents of the viewer:
 *  - imgSrc: URL for the image cell
 *  - htmlSrc: HTML of the transcription cell
 *  - cssSrc: CSS of the transcription cell
 */
GTDualViewer.prototype.setSrc = function(imgSrc, htmlSrc, cssSrc)
{
	this.imageViewer.setSrc(imgSrc);
	if (!this.noSvg)
		this.transViewer.setHtml(htmlSrc, cssSrc, 24);
	this.transPlain.innerHTML = htmlSrc;
}
</script>