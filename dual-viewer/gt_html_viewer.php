<?php require_once("gt_image_viewer.php"); ?>
<script>
'use strict';

/**
 * A subclass of the GTImageViewer that renders an HTML element as the image
 */

//Creates a new viewer which is appended to a parent HTML element
function GTHTMLViewer(parent, useDragEvents)
{
	GTImageViewer.call(this, parent, useDragEvents);
}
GTHTMLViewer.prototype = Object.create(GTImageViewer.prototype);
GTHTMLViewer.prototype.constructor = GTHTMLViewer;

//renders the given html as an image with the specified CSS and font size
GTHTMLViewer.prototype.setHtml = function(html, css, fontSize)
{
	//place the html inside a div with the specified style
	var transHolder = document.createElement("DIV");
	transHolder.style.cssText = "position: absolute; visibility: hidden; height: auto; width: auto; white-space: nowrap; border: none; margin: 0px; padding: 0px"
	//the DIV is temporarily added to the document to have its size computed
	document.body.appendChild(transHolder);
	transHolder.innerHTML = "<style>"+css+"</style><span style='all: initial; white-space: nowrap'>"+html+"</span>";
	var width = transHolder.clientWidth+1;
	var height = transHolder.clientHeight+1;
	document.body.removeChild(transHolder);

	//a new document is created to be rendered
	var doc = document.implementation.createHTMLDocument('');
	//doc.write(transHolder.innerHTML);
	doc.body.innerHTML = transHolder.innerHTML;
	doc.body.style.backgroundColor = "#ffffff";
	doc.documentElement.setAttribute('xmlns', "http://www.w3.org/1999/xhtml");
	//the HTML is converted to XML prior to integration in a SVG object
	var xml = (new XMLSerializer).serializeToString(doc.body);
	//the HTML is embedded within an SVG
	var src = "data:image/svg+xml;charset=utf-8,<svg xmlns='http://www.w3.org/2000/svg' width='"+width+"' height='"+height+"' style='background: white'>"
		+"<foreignObject width='100%' height='100%'>"
		+xml
		+"</foreignObject>"
		+"</svg>";
	this.setSrc(src);
}
</script>
