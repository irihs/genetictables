<script>
/**
 * Methods for converting a read request XML (obtained in response to a ajax_output_*.php invocation) into a javascript object
 */

function parseChapters(xml)
{
	var res = [];
	var chapters = xml.getElementsByTagName("Chapter");
	for (var i=0;i<chapters.length;i++)
	{
		res[i] = {};
		res[i].id = parseInt(chapters[i].getAttribute("id"));
		res[i].name = chapters[i].getAttribute("name");
		res[i].toPage = parseInt(chapters[i].getAttribute("toPage"));
	}
	return res;
}
function parseCodes(xml)
{
	var res = [];
	var codes = xml.getElementsByTagName("Code");
	for (var i=0;i<codes.length;i++)
	{
		res[i] = {};
		res[i].id = parseInt(codes[i].getAttribute("id"));
		res[i].name = codes[i].getAttribute("name");
		res[i].style = codes[i].getAttribute("style");
		res[i].rank = parseInt(codes[i].getAttribute("rank"));
	}
	return res;
}
function parseDisplays(xml)
{
	 var res = [];
	 var displays = xml.getElementsByTagName("Display");
	 for (var i=0;i<displays.length;i++)
	 {
		 res[i] = {};
		 res[i].chapterDisplay = parseInt(displays[i].getAttribute("chapterDisplay")) != 0;
		 res[i].pageSpan = parseInt(displays[i].getAttribute("pageSpan"));
		 res[i].tableColor = displays[i].getAttribute("tableColor");
		 while (res[i].tableColor.length < 6)
				res[i].tableColor = "0"+res[i].tableColor;
		 res[i].textColor = displays[i].getAttribute("textColor");
		 while (res[i].textColor.length < 6)
				res[i].textColor = "0"+res[i].textColor;
		res[i].incipitSourceId = displays[i].getAttribute("incipitSourceId");
	 }
	 return res;
}
function parseIncipit(xml)
{
	var res = {};
	res.chapter = xml.getElementsByTagName("Chapter")[0].textContent;
	res.incipit = xml.getElementsByTagName("Incipit")[0].textContent;
	res.explicit = xml.getElementsByTagName("Explicit")[0].textContent;
	return res;
}
function parseTags(xml)
{
	var res = [];
	var tags = xml.getElementsByTagName("Tag");
	for (var i=0;i<tags.length;i++)
	{
		res[i] = {};
		res[i].id = parseInt(tags[i].getAttribute("id"));
		res[i].name = tags[i].getAttribute("name");
		res[i].rank = parseInt(tags[i].getAttribute("rank"));
		res[i].label = tags[i].getAttribute("label");
		res[i].color = tags[i].getAttribute("color");
		while (res[i].color.length < 6)
			res[i].color = "0"+res[i].color;
	}
	return res;
}
function parseTagFragments(xml)
{
	var res = [];
	var frags = xml.getElementsByTagName("Fragment");
	for (var i=0;i<frags.length;i++)
		res[i] = parseInt(frags[i].getAttribute("id"));
	return res;
}
function parseSources(xml)
{
	var res = [];
	var sources = xml.getElementsByTagName("Source");
	for (var i=0;i<sources.length;i++)
	{
		res[i] = {};
		res[i].id = parseInt(sources[i].getAttribute("id"));
		res[i].name = sources[i].getAttribute("name");
		res[i].rank = parseInt(sources[i].getAttribute("rank"));
		res[i].label = sources[i].getAttribute("label");
		res[i].color = sources[i].getAttribute("color");
		while (res[i].color.length < 6)
			res[i].color = "0"+res[i].color;
	}
	return res;
}

function parseFolios(xml)
{
	var res = [];
	var sources = xml.getElementsByTagName("Folio");
	for (var i=0;i<sources.length;i++)
	{
		res[i] = {};
		res[i].id = parseInt(sources[i].getAttribute("id"));
		res[i].number = sources[i].getAttribute("number");
		res[i].sourceId = parseInt(sources[i].getAttribute("sourceId"));
	}
	//sort the folios by number (if parsable)
	return res.sort(function (a, b)
	{
		var ai = parseInt(a.number);
		var bi = parseInt(b.number);
		if (ai != NaN && bi != NaN) {
			if (ai != bi)
				return ai-bi;}
		else if (ai != NaN) return -1;
		else if (bi != NaN) return 1;
		return a.number.localeCompare(b.number);
	});
}

function parseFragments(xml)
{
	var res = [];
	var frags = xml.getElementsByTagName("Fragment");
	for (var i=0;i<frags.length;i++)
	{
		res[i] = {};
		res[i].id = parseInt(frags[i].getAttribute("id"));
		res[i].folioId = parseInt(frags[i].getAttribute("folioId"));
		res[i].folioNum = frags[i].getAttribute("folioNum");
		res[i].fromPage = parseInt(frags[i].getAttribute("fromPage"));
		res[i].fromLine = parseInt(frags[i].getAttribute("fromLine"));
		res[i].toPage = parseInt(frags[i].getAttribute("toPage"));
		res[i].toLine = parseInt(frags[i].getAttribute("toLine"));
		res[i].parentId = parseInt(frags[i].getAttribute("parentId"));
		res[i].previousId = parseInt(frags[i].getAttribute("previousId"));
		res[i].position = frags[i].getAttribute("position");
		res[i].withheld = frags[i].getAttribute("withheld") == "true";
		res[i].sourceId = parseInt(frags[i].getAttribute("sourceId"));
		res[i].isPublic = frags[i].getAttribute("public") == "true";
		//tags are in a space separated list of names
		res[i].tags = frags[i].getAttribute("tags").split(" ");
		//if none
		if (res[i].tags.length == 1 && res[i].tags[0].length == 0)
		{
			res[i].tags = [];
			res[i].tagTables = [];
			res[i].tagRanks = [];
		}
		//otherwise get the table ids for each tag from a space separated list
		else
		{
			res[i].tagTables = frags[i].getAttribute("tagTables").split(" ");
			res[i].tagRanks = frags[i].getAttribute("tagRanks").split(" ");
		}
	}
	
	return res;
}
</script>
