<?php
require_once(dirname(__FILE__).'/../db/db_folios.php');

function handle($p)
{
	//output folio name
	if (array_key_exists("name", $_GET) && $_GET["name"] != "false")
	{
		?><GTResponse><?php echo getFolioName($p, $_GET["folioId"]); ?></GTResponse><?php
	}
	//output folio id
	else if (array_key_exists("id", $_GET) && $_GET["id"] != "false")
	{
		?><GTResponse><?php echo getFolioId($p, $_GET["workId"], $_GET["source"], $_GET["number"]); ?></GTResponse><?php
	}
	//output folio image URL
	else if (array_key_exists("image", $_GET) && $_GET["image"] != "false")
	{
		?><GTResponse><?php echo getFolioImageUrl($p, $_GET["folioId"]); ?></GTResponse><?php
	}
	//output folio miniature URL
	else if (array_key_exists("mini", $_GET) && $_GET["mini"] != "false")
	{
		?><GTResponse><?php echo getFolioMiniUrl($p, $_GET["folioId"]); ?></GTResponse><?php
	}
	//output id, number and source id of a single folio or a set of folios within a source
	else
	{
		$workId = $_GET['workId'];
		//filter by folio id
		$folioId = array_key_exists("folioId", $_GET) ? $_GET["folioId"] : null;
		//filter by source
		$sourceId = array_key_exists("sourceId", $_GET) ? $_GET["sourceId"] : null;
		$folios = getFolioData($p, $workId, $sourceId, $folioId);
?><GTResponse>
	<Folios>
	<?php
		foreach ($folios as $v)
		{
	?>	<Folio id="<?php echo $v['id']; 
			?>" number="<?php echo $v['number']; 
			?>" sourceId="<?php echo $v['sourceId']; 
			?>" />
<?php 
		}
?>	</Folios>
</GTResponse><?php 
	}
}
?>
