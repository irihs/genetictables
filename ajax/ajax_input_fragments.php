<?php
require_once(dirname(__FILE__).'/../db/db_fragments.php');

function handle($p)
{
	$op = $_GET['op'];
	//fragment creation
	if ($op == "new")
	{
		$folioId = $_GET['folioId'];
		$fragId = createFragment($p, $folioId);
		//after creation, send back the fragment id
		?>
<GTResponse>
<Fragments>
	<Fragment id="<?php echo $fragId;
?>" />
</Fragments>
</GTResponse><?php 
	}
	//update of fragment attributes
	else if ($op == "update")
		updateFragment($p, 
				$_GET['fragmentId'],
				$_GET['fromPage'],
				$_GET['fromLine'],
				$_GET['toPage'],
				$_GET['toLine'],
				$_GET['position'],
				$_GET['withheld'] == "true" ? 1 : 0,
				$_GET['parent'] == "" ? null : $_GET['parent'],
				$_GET['previous'] == "" ? null : $_GET['previous']);
	//removal of a fragment
	else if ($op == "remove")
		removeFragment($p, $_GET['fragmentId']);
}
?>