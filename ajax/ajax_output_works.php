<?php
require_once(dirname(__FILE__).'/../db/db_works.php');

function handle($p)
{
	//output the attributes of the chapters within a work
	if (array_key_exists("chapters", $_GET) && $_GET["chapters"] != "false")
	{
		$chapters = getChapterData($p, $_GET['workId']);
?>
<GTResponse>
<Chapters>
<?php
		foreach ($chapters as $v)
		{
?>	<Chapter id="<?php echo $v['id']; 
		?>" name="<?php echo htmlspecialchars($v['name']); 
		?>" toPage="<?php echo $v['toPage']; 
		?>" />
<?php 
		}
?>
</Chapters>
</GTResponse>
<?php 
	}
	//output the transcription codes of a given work
	else if (array_key_exists("codes", $_GET) && $_GET["codes"] != "false")
	{
		$codes = getCodeData($p, $_GET['workId']);
		?>
<GTResponse>
<Codes>
<?php
		foreach ($codes as $v)
		{
?>	<Code id="<?php echo $v['id']; 
		?>" name="<?php echo $v['name']; 
		?>" style="<?php echo $v['style']; 
		?>" rank="<?php echo $v['rank'];
		?>" />
<?php 
		}
?>
</Codes>
</GTResponse>
<?php 
	}
	//output the name and id of all works
	else
	{
		$works = getWorks($p);
?>
<GTResponse>
<Works>
<?php 
		foreach ($works as $v)
		{
?>	<Work id="<?php echo $v['id'];
		?>" name="<?php echo htmlspecialchars(getWorkName($p, $v['id']));
		?>" />
<?php 
		}
?>
</Works>
</GTResponse>
<?php 
	}
}
?>
