<?php
/**
 * List of asynchroneous queries to read and write genetic table data.
 * Queries are sent to the public 'genetic-tables/index/ajax'. The view at views/shared/index/ajax.php calls handleAsyncRequest() in GeneticTablesPlugin.php. 
 */
require_once("parsers.php");
?>
<div id="debugZone"></div>
<script>
"use strict";
//An error response starts with an 'Error' tag
function isError(response)
{
	return response.indexOf("<Error>") >= 0;
}
//Extracts the text inside an Error tag
function parseError(response)
{
	var s = response.indexOf("<Error>");
	if (s < 0)
		return response;
	s += 7;
	var e = response.indexOf("</Error>");
	if (e < 0)
		return response.substring(s);
	return response.substring(s, e);
}

var zone = document.getElementById("debugZone");
/**
 * Central method for sending a request
 * params: GET query string
 * parse: a method that can parse the XML response, the result of which is passed on to onResult. Can be null
 * onResult: called with the response XML (possibly parsed) upon success
 * onError: called upon failure. Can be null
 * postData: data to be included in the POST data of the request
 * isFile: specifies that POST data shouldn't be escaped
 */
var gtUseAsynRequests = true;
function sendRequest(params, parse, onResult, onError, postData, isFile)
{
	var req = new XMLHttpRequest();
	req.onreadystatechange = function()
	{
		if (req.readyState !== XMLHttpRequest.DONE)
			return;
		var error = null;
		if (req.status === 200)
		{
			//console.log("response: "+req.responseText);
			var parser = new DOMParser();
			var fromInd = req.responseText.indexOf("<GTResponse>");
			var toInd = req.responseText.indexOf("</GTResponse>");
			//a response with no GTResponse tag is considered empty, but not an error
			var data = fromInd < 0 || toInd < 0 ? "" : req.responseText.substring(fromInd+12, toInd);
			if (isError(data))
				error = parseError(data);
			else
			{
				if (onResult !== null)
					//if a parser was given, parse the xml
					onResult(parse !== null ? parse(parser.parseFromString(data, "text/xml")) : data);
				return;
			}
		}

		if (error === null)
			error = "Une erreur s'est produite.";
		if (onError !== undefined && onError !== null)
			onError(error);
		//a global errorHandler is given the error if no specific handler was specified
		if (typeof errorHandler !== 'undefined')
			errorHandler(error);
	}
	var url = "<?php echo public_url("genetic-tables/index/ajax"); ?>?"+params;
	console.log(url);
	//POST request
	if (typeof(postData) !== "undefined")
	{
		req.open("POST", url, gtUseAsynRequests);
		//send raw encoded POST data
		if (typeof(isFile) !== "undefined" && isFile)
		{
			var formData = new FormData();
			formData.append(postData.name, postData);
			try {req.send(formData);} catch (error) {console.log(error.name+" "+error.message);}
		}
		//send url escaped POST data
		else
		{
			req.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			try {req.send(postData);} catch (error) {console.log(error.name+" "+error.message);}
		}
	}
	//GET request
	else
	{
		req.open("GET", url, gtUseAsynRequests);
		try {req.send();} catch (error) {console.log(error.name+" "+error.message);}
	}
}

/**
 * Read queries for tables and works
 */
function readWorks(tableId, onResult, onError)
{
	sendRequest("output=works&tableId="+tableId, parseChapters, onResult, onError);
}
function readChapters(workId, onResult, onError)
{
	sendRequest("output=works&workId="+workId+"&chapters=true", parseChapters, onResult, onError);
}
function readCodes(workId, onResult, onError)
{
	sendRequest("output=works&workId="+workId+"&codes=true", parseCodes, onResult, onError);
}
function readDisplay(tableId, onResult, onError)
{
	sendRequest("output=tables&tableId="+tableId+"&display=true", parseDisplays, onResult, onError);
}
function readTableIncipit(tableId, from, to, onResult, onError)
{
	sendRequest("output=tables&tableId="+tableId+"&from="+from+"&to="+to+"&incipit=true", parseIncipit, onResult, onError);
}
/**
 * Read queries for tags
 */
function readTags(tableId, onResult, onError)
{
	sendRequest("output=tags&tableId="+tableId, parseTags, onResult, onError);
}
function readTagForName(tableId, name, onResult, onError)
{
	sendRequest("output=tags&tableId="+tableId+"&name="+name, 
		function(xml) {return xml.getElementsByTagName("Tag")[0].getAttribute('id')}, 
		onResult, onError);
}
function readTagFragments(tableId, tagId, onResult, onError)
{
	sendRequest("output=tags&tableId="+tableId+"&tagId="+tagId, parseTagFragments, onResult, onError);
}
/**
 * Read queries for sources
 */
function readSources(workId, onResult, onError)
{
	sendRequest("output=sources&workId="+workId, parseSources, onResult, onError);
}

/**
 * Read queries for folios
 */
function readFoliosForWork(workId, onResult, onError) {readFolios(workId, null, onResult, onError);}
function readFolio(workId, folioId, onResult, onError)
{
	sendRequest("output=folios&workId="+workId+"&folioId="+folioId, parseFolios, onResult, onError);
}
function readFolios(workId, sourceId, onResult, onError)
{
	sendRequest("output=folios&workId="+workId+(sourceId === null ? "" : "&sourceId="+sourceId), parseFolios, onResult, onError);
}
function readFolioId(workId, source, number, onResult, onError)
{
	sendRequest("output=folios&workId="+workId+"&source="+source+"&number="+number+"&id=true", null, onResult, onError);
}
function readFolioName(folioId, onResult, onError)
{
	sendRequest("output=folios&folioId="+folioId+"&name=true", null, onResult, onError);
}
function readFolioImage(folioId, onResult, onError)
{
	sendRequest("output=folios&folioId="+folioId+"&image=true", null, onResult, onError);
}
function readFolioMini(folioId, onResult, onError)
{
	sendRequest("output=folios&folioId="+folioId+"&mini=true", null, onResult, onError);
}

/**
 * Read queries for fragments
 */
function readFragment(fragId, onResult, onError)
{
	sendRequest("output=fragments&fetchParents=false&fragId="+fragId, parseFragments, onResult, onError);
}
function readFragmentsForTable(tableId, fromPage, toPage, onResult, onError)
{
	readFragments(tableId, null, fromPage, toPage, true, onResult, onError);
}
function readFragmentsForFolio(tableId, folioId, onResult, onError)
{
	readFragments(tableId, folioId, null, null, false, onResult, onError);
}
function readFragments(tableId, folioId, fromPage, toPage, fetchParents, onResult, onError)
{
	readFragments(tableId, folioId, fromPage, toPage, fetchParents, onResult, onError)
}
function readFragments(tableId, folioId, fromPage, toPage, fetchParents, onResult, onError)
{
	sendRequest("output=fragments&tableId="+tableId+"&fetchParents="+(fetchParents ? "true" : "false")
			+(fromPage === null ? "" : "&from="+fromPage)
			+(toPage === null ? "" : "&to="+toPage)
			+(folioId === null ? "" : "&folioId="+folioId),
		parseFragments, onResult, onError);
}
function readFragmentNextId(fragId, onResult, onError)
{
	sendRequest("output=fragments&idsOnly=true&fragId="+fragId+"&nextId=true", null, onResult, onError);
}
function readFragmentChildrenIds(fragId, onResult, onError)
{
	sendRequest("output=fragments&idsOnly=true&fragId="+fragId+"&childrenIds=true", null, onResult, onError);
}

/**
 * Write queries for works
 */
function writeWorkHelp(workId, help, onResult, onError)
{
	sendRequest("input=works&op=update&workId="+workId+"&help=true", null, onResult, onError, "text="+encodeURIComponent(help));
}
function writeNewChapter(workId, onResult, onError)
{
	sendRequest("input=works&op=newChapter&workId="+workId, 
		function(xml) {return xml.getElementsByTagName("Chapter")[0].getAttribute('id')}, 
		onResult, onError);
}
function writeUpdateChapter(workId, chapterId, name, toPage, onResult, onError)
{
	sendRequest("input=works&op=updateChapter"
		+"&workId="+workId
		+"&chapterId="+chapterId
		+"&name="+encodeURIComponent(name)
		+"&toPage="+toPage, 
		null, onResult, onError);
}
function writeNewCode(workId, onResult, onError)
{
	sendRequest("input=works&op=newCode&workId="+workId, onResult, onError);
}
function writeUpdateCode(workId, codeId, name, style, rank, onResult, onError)
{
	sendRequest("input=works&op=updateCode"
		+"&workId="+workId
		+"&codeId="+codeId
		+"&name="+encodeURIComponent(name)
		+"&style="+encodeURIComponent(style)
		+"&rank="+rank, 
		null, onResult, onError);
}

/**
 * Write queries for tables
 */
function writeNewTable(workId, name, onResult, onError)
{
	sendRequest("input=tables&op=new"
		+"&workId="+workId+"&name="+encodeURIComponent(name),
		function(xml) {return xml.getElementsByTagName("Table")[0].getAttribute('id')}, 
		onResult, 
		onError);
}
function writeTableFooter(tableId, footer, onResult, onError)
{
	sendRequest("input=tables&op=update&tableId="+tableId+"&footer=true", null, onResult, onError, "text="+encodeURIComponent(footer));
}
function writeUpdateDisplay(tableId, chapterDisplay, pageSpan, tableColor, textColor, incipitSourceId, onResult, onError)
{
	sendRequest("input=tables&op=updateDisplay"
		+"&tableId="+tableId
		+"&chapterDisplay="+chapterDisplay
		+"&pageSpan="+pageSpan
		+"&tableColor="+tableColor
		+"&textColor="+textColor
		+"&incipitSourceId="+incipitSourceId, 
		null, onResult, onError);
}

/**
 * Write queries for tags
 */
function writeNewTag(tableId, onResult, onError)
{
	sendRequest("input=tags&op=new"
		+"&tableId="+tableId,
		function(xml) {return xml.getElementsByTagName("Tag")[0].getAttribute('id')}, onResult, onError);
}
function writeNewTagFragment(workId, tagId, fragmentId, onResult, onError)
{
	sendRequest("input=tags&op=new&workId="+workId+"&tagId="+tagId+"&fragmentId="+fragmentId, null, onResult, onError);
}
function writeRemoveTagFragment(workId, tagId, fragmentId, onResult, onError)
{
	sendRequest("input=tags&op=remove&workId="+workId+"&tagId="+tagId+"&fragmentId="+fragmentId, null, onResult, onError);
}
function writeUpdateTagName(tableId, tagId, name, onResult, onError)
{
	sendRequest("input=tags&op=update&tableId="+tableId+"&tagId="+tagId+"&name="+name, null, onResult, onError);
}
function writeUpdateTagRank(tableId, tagId, rank, onResult, onError)
{
	sendRequest("input=tags&op=update&tableId="+tableId+"&tagId="+tagId+"&rank="+rank, null, onResult, onError);
}
function writeUpdateTagLabel(tableId, tagId, label, onResult, onError)
{
	sendRequest("input=tags&op=update&tableId="+tableId+"&tagId="+tagId+"&label="+label, null, onResult, onError);
}
function writeUpdateTagColor(tableId, tagId, color, onResult, onError)
{
	sendRequest("input=tags&op=update&tableId="+tableId+"&tagId="+tagId+"&color="+color, null, onResult, onError);
}
function writeRemoveTag(tableId, tagId, onResult, onError)
{
	sendRequest("input=tags&op=remove&tableId="+tableId+"&tagId="+tagId, null, onResult, onError);
}
function writeUpdateTag(tableId, tagId, name, label, rank, color, onResult, onError)
{
	sendRequest("input=tags&op=update&tableId="+tableId+"&tagId="+tagId+"&name="+name+"&rank="+rank+"&label="+label+"&color="+color, null, onResult, onError);
}

/**
 * Write queries for sources
 */
function writeNewSource(workId, name, onResult, onError)
{
	sendRequest("input=sources&op=new"
		+"&workId="+workId
		+"&name="+encodeURIComponent(name), 
		function(xml) {return xml.getElementsByTagName("Source")[0].getAttribute('id')}, onResult, onError);
}
function writeUpdateSourceName(tableId, sourceId, name, renameFolios, onResult, onError)
{
	sendRequest("input=sources&op=update&tableId="+tableId+"&sourceId="+sourceId+"&name="+name+"&renameFolios="+renameFolios, null, onResult, onError);
}
function writeUpdateSource(sourceId, rank, label, color, onResult, onError)
{
	sendRequest("input=sources&op=update&sourceId="+sourceId+"&rank="+rank+"&label="+label+"&color="+color, null, onResult, onError);
}
function writeUpdateSourceRank(tableId, sourceId, rank, onResult, onError)
{
	sendRequest("input=sources&op=update&tableId="+tableId+"&sourceId="+sourceId+"&rank="+rank, null, onResult, onError);
}
function writeUpdateSourceLabel(tableId, sourceId, label, onResult, onError)
{
	sendRequest("input=sources&op=update&tableId="+tableId+"&sourceId="+sourceId+"&label="+label, null, onResult, onError);
}
function writeUpdateSourceColor(tableId, sourceId, color, onResult, onError)
{
	sendRequest("input=sources&op=update&tableId="+tableId+"&sourceId="+sourceId+"&color="+color, null, onResult, onError);
}
function writeUpdateSourceVisibility(sourceId, visibility, onResult, onError)
{
	sendRequest("input=sources&op=update&sourceId="+sourceId+"&visibility="+visibility, null, onResult, onError);
}
function writeMergeSource(tableId, sourceId, mergeId, renameFolios, onResult, onError)
{
	sendRequest("input=sources&op=update&tableId="+tableId+"&sourceId="+sourceId+"&merge="+mergeId+"&renameFolios="+renameFolios, null, onResult, onError);
}
function writeRemoveSource(tableId, sourceId, cascade, onResult, onError)
{
	sendRequest("input=sources&op=remove&tableId="+tableId+"&sourceId="+sourceId+"&cascade="+cascade, null, onResult, onError);
}

/**
 * Write queries for folios
 * Note: folios being proper Omeka items that are created with uploaded images, they are not created through asynchronous queries. Instead they are created when a genetic table is edited and saved and handled by the hook
 * 'hookAfterSaveRecord' which includes "update_folios.php"  
 */
function writeNewFolio(workId, sourceId, number, incipit, explicit, authorNotes, censored, onResult, onError)
{
	sendRequest("input=folios&op=new"
		+"&workId="+workId
		+"&sourceId="+sourceId
		+"&number="+number
		+"&incipit="+incipit
		+"&explicit="+explicit
		+"&authorNotes="+authorNotes
		+"&censored="+censored,
		function(xml) {return xml.getElementsByTagName("Folio")[0].getAttribute('id')}, 
		onResult, 
		onError);
}
function writeUpdateFolioSource(folioId, sourceId, onResult, onError)
{
	sendRequest("input=folios&op=update&sourceId="+sourceId+"&folioId="+folioId, null, onResult, onError);
}
function writeUpdateFolioNumber(folioId, number, onResult, onError)
{
	sendRequest("input=folios&op=update&folioId="+folioId+"&number="+number, null, onResult, onError);
}
function writeRemoveFolio(folioId, onResult, onError)
{
	sendRequest("input=folios&op=remove&folioId="+folioId, null, onResult, onError);
}
function writeUpdateFolioTranscription(folioId, trans, onResult, onError)
{
	sendRequest("input=folios&op=update&folioId="+folioId+"&transcription=true", null, onResult, onError, "trans="+encodeURIComponent(trans));
}
function writeUpdateFolioAttachImage(folioId, file, onResult, onError)
{
	sendRequest("input=folios&op=update&folioId="+folioId+"&image=true", null, onResult, onError, file, true);
}
function writeUpdateFolioAddLink(folioId, url, onResult, onError)
{
	sendRequest("input=folios&op=update&folioId="+folioId+"&link="+encodeURIComponent(url), null, onResult, onError);
}

/**
 * Write queries for fragments
 */
function writeNewFragment(workId, folioId, onResult, onError)
{
	sendRequest("input=fragments&op=new"
		+"&workId="+workId
		+"&folioId="+folioId, 
		function(xml) {return xml.getElementsByTagName("Fragment")[0].getAttribute('id')}, 
		onResult, 
		onError);
}
function writeUpdateFragment(workId, fragmentId, p0, l0, p1, l1, pos, withheld, parent, previous, onResult, onError)
{
	sendRequest("input=fragments&op=update&workId="+workId+"&fragmentId="+fragmentId+"&fromPage="+p0+"&fromLine="+l0+"&toPage="+p1+"&toLine="+l1+"&position="+pos+"&withheld="+withheld+"&parent="+parent+"&previous="+previous, null, onResult, onError);
}
function writeRemoveFragment(workId, fragmentId, onResult, onError)
{
	sendRequest("input=fragments&op=remove&tableId="+workId+"&fragmentId="+fragmentId, null, onResult, onError);
}

/**
 * Write queries for session
 */
function writeUpdateSessionPageSpan(tableId, nPages)
{
	 sendRequest("input=session&op=update&tableId="+tableId+"&nPages="+nPages, null, null, null);
}
</script>