<?php
require_once(dirname(__FILE__).'/../db/db_sources.php');

function handle($p)
{
	$op = $_GET['op'];
	//creation of a source
	if ($op == "new")
	{
		$name = $_GET['name'];
		//create the associated omeka collection
		$collection = insert_collection(
				array(
						"public" => true,
						"featured" => false),
				array(
						"Dublin Core" => array(
								"Title" => array(array("text" => $name, "html" => false))))
				);
		//create the DB data
		createSource($p, $collection->id, $_GET['workId']);
		//after creation, send back the source id
	?>
<GTResponse>
	<Sources>
		<Source id="<?php echo $collection->id;
?>" />
	</Sources>
</GTResponse><?php
	}
	else if ($op == "update")
	{
		//update of the source name
		if (array_key_exists('name', $_GET))
		{
			$name = $_GET['name'];
			//if (sourceNameExists($p, $name))
			//	outputErrorXML(__("Source name already exists"));
			$sourceId = $_GET['sourceId'];
			setSourceName($p, $sourceId, $name);
			
			//optionally, all folios within the source are updated to reflect the new name
			if (array_key_exists('renameFolios', $_GET) && $_GET['renameFolios'] == "true")
			{
				set_time_limit(0);
				
				$work = getWorkForSource($p, $sourceId);
				$folios = getSourceFolioIds($p, $sourceId);
				foreach ($folios as $folio)
					renameFolioSource($p, $folio['id'], $work, $name);
			}
		}
		//merge a source into another
		if (array_key_exists('merge', $_GET))
		{
			$mergeId = $_GET['merge'];
			$sourceId = $_GET['sourceId'];
			
			//optionally, all folios within the source are updated to reflect the new source
			if (array_key_exists('renameFolios', $_GET) && $_GET['renameFolios'] == "true")
			{
				set_time_limit(0);
				
				$name = getNameForSource($p, $mergeId);
				$work = getWorkForSource($p, $sourceId);
				$folios = getSourceFolioIds($p, $sourceId);
				foreach ($folios as $folio)
					renameFolioSource($p, $folio['id'], $work, $name);
			}
			
			moveFolios($p, $sourceId, $mergeId);
			removeSource($p, $sourceId);
		}
		//updates of the individual attributes of the source
		if (array_key_exists('rank', $_GET))
			updateSourceRank($p, $_GET['sourceId'], $_GET['rank']);
		if (array_key_exists('label', $_GET))
			updateSourceLabel($p, $_GET['sourceId'], $_GET['label']);
		if (array_key_exists('color', $_GET))
			updateSourceColor($p, $_GET['sourceId'], $_GET['color']);
		if (array_key_exists("visibility", $_GET))
		{
			//changes to visibility are propagated to the folios within the source
			set_time_limit(0);
			
			$vis = $_GET["visibility"] == "true" ? 1 : 0;
			$folios = getSourceFolioIds($p, $_GET['sourceId']);
			foreach ($folios as $folio)
				setSourceVisibility($p, $folio['id'], $vis);
		}
	}
	//removal of a source
	else if ($op == "remove")
	{
		//optionally, all folios within the source are deleted
		if (array_key_exists("cascade", $_GET) && $_GET["cascade"] == "true")
		{
			set_time_limit(0);
			
			$folios = getSourceFolioIds($p, $_GET['sourceId']);
			foreach ($folios as $folio)
				removeFolio($p, $folio['id']);
		}
		removeSource($p, $_GET['sourceId']);
	}
}
?>