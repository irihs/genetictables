<?php 
require_once(dirname(__FILE__).'/../db/db_works.php');

function handle($p)
{
	$op = $_GET['op'];
	//creation of a chapter
	if ($op == "newChapter")
	{
		$chpaterId = createChapter($p, $_GET['workId'], __("New"));
		//after creation, send back the new id
		?>
<GTResponse>
<Chapters>
	<Chapter id="<?php echo $chpaterId; ?>" />
</Chapters>
</GTResponse><?php 
	}
	//update the attributes of a chapter
	else if ($op == "updateChapter")
	{
		updateChapter($p, $_GET['workId'], $_GET['chapterId'], $_GET['name'], $_GET['toPage']);
	}
	//creation of a transcription code
	else if ($op == "newCode")
	{
		createCode($p, $_GET['workId'], __("New"));
	}
	//update the attributes of a transcription code
	else if ($op == "updateCode")
	{
		updateCode($p, $_GET['workId'], $_GET['codeId'], $_GET['name'], $_GET['style'], $_GET['rank']);
	}
	//update the transcription help text (uploaded in POST data)
	else if ($op == "update" && array_key_exists("help", $_GET) && $_GET["help"] != "false")
		setWorkTranscriptionHelp($p, $_GET['workId'], $_POST["text"]);
}
?>
