<?php
require_once(dirname(__FILE__).'/../db/db_tags.php');

function handle($p)
{
	//output the name of a tag
	if (array_key_exists('name', $_GET))
	{
		$tagId = getTagIdForName($p, $tableId, $name);
		?>
<GTResponse>
<Tags>
	<Tag id="<?php echo $tagId === nulll ? -1 : $tagId; ?>" />
</Tag>
</GTResponse>
<?php
	}
	//output the fragment ids associated with a tag
	else if (array_key_exists('tagId', $_GET))
	{
		?>
<GTResponse>
<Fragments>
<?php
		$frags = getTagFragmentIds($p, $_GET['tagId']);
		foreach ($frags as $v)
		{
			?>	<Fragment id="<?php echo $v['id']; 
		?>" />
<?php 
		}
?>
</Fragments>
</GTResponse>
<?php
	}
	//output the attributes of a tag
	else
	{
?>
<GTResponse>
<Tags>
<?php
		$tags = getTagData($p, $_GET['tableId']);
		foreach ($tags as $v)
		{
?>	<Tag id="<?php echo $v['id']; 
		?>" name="<?php echo htmlspecialchars($v['name']); 
		?>" rank="<?php echo $v['rank']; 
		?>" label="<?php echo htmlspecialchars($v['shortName']); 
		?>" color="<?php echo dechex($v['color']); 
		?>" />
<?php 
		}
?>
</Tag>
</GTResponse>
<?php
	}
}
?>
