<?php
require_once(dirname(__FILE__).'/../db/db_tables.php');

function handle($p)
{
	//output the display attributes of a given table
	if (array_key_exists("display", $_GET) && $_GET["display"] != "false")
	{
		$display = getTableDisplay($p, $_GET['tableId']);
		?>
<GTResponse>
<Display chapterDisplay="<?php echo $display[0]['chapterDisplay']; 
	?>" pageSpan="<?php echo $display[0]['pageSpan']; 
	?>" tableColor="<?php echo dechex($display[0]['tableColor']); 
	?>" textColor="<?php echo dechex($display[0]['textColor']);
	?>" incipitSourceId="<?php echo $display[0]['incipitSourceId'] == null ? "" : $display[0]['incipitSourceId']; 
	?>" />
</GTResponse>
<?php 
	}
	//output the name of the incipit, explicit and chapter name for a given page number 
	else if (array_key_exists("incipit", $_GET) && $_GET["incipit"] != "false")
	{
		$chapters = getChapterDataForPage($p, getTableWorkId($p, $_GET['tableId']), $_GET['from']);
?>
<GTResponse>
<Incipits>
	<Chapter><?php echo count($chapters) > 0 ? $chapters[0]['name'] : ""; ?></Chapter>
	<Incipit><?php echo getTableIncipit($p, $_GET['tableId'], $_GET['from']); ?></Incipit>
	<Explicit><?php echo getTableExplicit($p, $_GET['tableId'], $_GET['to']); ?></Explicit>
</Incipits>
</GTResponse>
<?php 
	}
}
?>
