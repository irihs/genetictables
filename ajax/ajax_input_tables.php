<?php 
require_once(dirname(__FILE__).'/../db/db_tables.php');

function handle($p)
{
	$op = $_GET['op'];
	//creation of a new table
	if ($op == "new")
	{
		$dcArray = array("Title" => array(array("text" => $_GET['name'], "html" => false)));
		$mdArray = array();
		//if a footer is specified it is added to the table metadata
		if (array_key_exists("footer", $_GET))
			$mdArray[$p->tableFooterElementName] = array(array("text" => $_GET["footer"], "html" => true));
		//create the omeka item associated
		$table = insert_item(
				array(
						"public" => true,
						"item_type_name" => $p->tableTypeName),
				array(
						"Dublin Core" => $dcArray,
						"Item Type Metadata" => $mdArray
				)
				);
		//create the DB data
		createTableDisplay($p, $table->id, $_GET['workId'])
		//after creation, send back the table id
		?>
<GTResponse>
	<Tables>
		<Table id="<?php echo $table->id; ?>" />
	</Tables>
</GTResponse><?php
	}
	//update of the display attributes
	else if ($op == "updateDisplay")
	{
		setTableDisplay($p, $_GET['tableId'], $_GET['chapterDisplay'], $_GET['pageSpan'], $_GET['tableColor'], $_GET['textColor'], $_GET['incipitSourceId']);
	}
	//update of the table footer
	else if ($op == "update" && array_key_exists("footer", $_GET) && $_GET["footer"] != "false")
		setTableFooter($p, $_GET['tableId'], $_POST["text"]);
}
?>
