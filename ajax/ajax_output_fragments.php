<?php 
require_once(dirname(__FILE__).'/../db/db_fragments.php');

function handle($p)
{
	//output the attributes of a set of fragments
	if (!array_key_exists('idsOnly', $_GET) || $_GET['idsOnly'] !== 'true')
	{
		//filter by folio
		$folioId = array_key_exists('folioId', $_GET) ? $_GET['folioId'] : null;
		//filter by work
		$workId = array_key_exists('workId', $_GET) ? $_GET['workId'] : null;
		//filter by page range
		$from = array_key_exists('from', $_GET) ? $_GET['from'] : null;
		$to = array_key_exists('to', $_GET) ? $_GET['to'] : null;
		//filter by a list of filter ids
		$ids = array_key_exists('fragId', $_GET) ? array($_GET['fragId']) : null;
		
		//build the SQL query given the filters
		$req = getFragmentData($p, $folioId, $workId, $from, $to, $ids);
		?>
		<GTResponse>
		<Fragments>
		<?php
		if ($req[0]->execute($req[1]))
		{
			$rs = $req[0]->fetchAll();
			
			//whether or not to include parents (if missing) of returned fragments even if outside range
			if (!array_key_exists('fetchParents', $_GET) || $_GET['fetchParents'] === 'true')
			{
				$fragmentIds = array();
				foreach ($rs as $v)
					$fragmentIds[$v['id']] = null;
				$parentIds = array();
				//build the list of parent ids
				foreach ($rs as $v)
					if (!array_key_exists($v['parentId'], $fragmentIds))
						$parentIds[] = $v['parentId'];
				//for all parents, append their attributes to the children
				if (count($parentIds) > 0)
				{
					$preq = getFragmentData($p, null, $workId, null, null, $parentIds);
					if ($preq[0]->execute($preq[1]))
					{
						$prs = $preq[0]->fetchAll();
						foreach ($prs as $v)
							$rs[] = $v;
					}
				}
			}
			
			//for all fragments (optionally including parents)
			foreach ($rs as $v)
			{
				$tags = getTagsForFragment($p, $v['id']);
				$tagList = "";
				$tableList = "";
				$rankList = "";
				foreach ($tags as $tag)
				{
					$tagList = $tagList.(strlen($tagList) == 0 ? "" : " ").$tag['tagId'];
					$tableList= $tableList.(strlen($tableList) == 0 ? "" : " ").$tag['tableId'];
					$rankList= $rankList.(strlen($rankList) == 0 ? "" : " ").$tag['rank'];
				}
				//for each fragment, output attributes
		?>	<Fragment id="<?php echo $v['id']; 
				?>" folioId="<?php echo $v['folioId'];
				?>" folioNum="<?php echo $v['num'];
				?>" fromPage="<?php echo $v['fromPage']; 
				?>" fromLine="<?php echo $v['fromLine']; 
				?>" toPage="<?php echo $v['toPage']; 
				?>" toLine="<?php echo $v['toLine']; 
				?>" parentId="<?php echo $v['parentId']; 
				?>" previousId="<?php echo $v['previousId']; 
				?>" position="<?php echo $v['position']; 
				?>" withheld="<?php echo ($v['withheld'] != 0 ? "true" : "false"); 
				?>" sourceId="<?php echo $v['sourceId']; 
				?>" public="<?php echo ($v['public'] == "1" ? "true" : "false");
				?>" tags="<?php echo $tagList;
				?>" tagRanks="<?php echo $rankList;
				?>" tagTables="<?php echo $tableList;
				?>" />
		<?php 
			}
		}
		?>
		</Fragments>
		</GTResponse>
		<?php
	}
	//output only ids of associated fragments as a comma separated list
	else 
	{
		$fragId = $_GET['fragId'];
		$ids = "";
		$isEmpty = true;
		//get associated fragments though the "next" relation (only one?)
		if (array_key_exists('nextId', $_GET) && $_GET['nextId'] !== 'false')
		{
			$folioIds = getNextFragmentId($p, $fragId);
			foreach ($folioIds as $v)
			{
				$ids = ($isEmpty ? $v['id'] : $ids." ".$v['id']);
				$isEmpty = false;
			}
		}
		//get associated fragments though the "child" relation
		else if (array_key_exists('childrenIds', $_GET) && $_GET['childrenIds'] !== 'false')
		{
			$folioIds = getChildrenFragmentIds($p, $fragId);
			foreach ($folioIds as $v)
			{
				$ids = ($isEmpty ? $v['id'] : $ids." ".$v['id']);
				$isEmpty = false;
			}
		}
		//output the id list
		?><GTResponse><?php echo $ids; ?></GTResponse><?php
	}
}
?>
