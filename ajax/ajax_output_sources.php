<?php
require_once(dirname(__FILE__).'/../db/db_sources.php');

function handle($p)
{
	//output the attributes of the sources within a given work
	$sources = getSourceData($p, $_GET['workId']);
?>
<GTResponse>
<Sources>
<?php
	foreach ($sources as $v)
	{
?>	<Source id="<?php echo $v['id']; 
		?>" name="<?php echo htmlspecialchars($v['name']); 
		?>" rank="<?php echo $v['rank']; 
		?>" label="<?php echo htmlspecialchars($v['shortName']); 
		?>" color="<?php echo dechex($v['color']); 
		?>" />
<?php 
	}
?>
</Sources>
</GTResponse>
<?php 
}
?>
