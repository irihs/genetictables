<?php
require_once(dirname(__FILE__).'/../db/db_folios.php');
require_once(dirname(__FILE__).'/../db/db_works.php');

function handle($p)
{
	$op = $_GET['op'];
	//folio creation
	if ($op == "new")
	{
		$workId = $_GET['workId'];
		$sourceId = $_GET['sourceId'];
		$work = getWorkForSource($p, $sourceId);
		$source = getNameForSource($p, $sourceId);
		$num = $_GET['number'];
		$prefix = $work." - ";
		//the name of the document is the concatenation of the name of the source and the folio number (preceded by the name of the work if the source name begins otherwise)
		$name = ((substr($source, 0, strlen($prefix)) == $prefix ? "" : $prefix).$source)." - Folio ".$num;
		//the folio is created with an incipit and explicit if they are specified
		$incipit = array_key_exists("incipit", $_GET) ? $_GET["incipit"] : "";
		$explicit = array_key_exists("explicit", $_GET) ? $_GET["explicit"] : "";
		$authorNotes = array_key_exists("authorNotes", $_GET) ? $_GET["authorNotes"] : "";
		$censored = array_key_exists("censored", $_GET) ? $_GET["censored"] : "";
		$dcArray = array("Title" => array(array("text" => $name, "html" => false)));
		
		$folio = insert_item(
				array(
						"public" => true,
						"collection_id" => $sourceId,
						"item_type_name" => $p->folioTypeName),
				array(
						"Dublin Core" => $dcArray,
						'Item Type Metadata'=> array(
								$p->numberElementName => array(array("text" => "".$num, "html" => false)),
								$p->folioIncipitElementName => array(array("text" => $incipit, "html" => false)),
								$p->folioExplicitElementName => array(array("text" => $explicit, "html" => false)),
								$p->folioAuthorNotesElementName => array(array("text" => $authorNotes, "html" => false)),
								$p->folioCensoredElementName => array(array("text" => $censored, "html" => false))
						))
				);
		//after the creation, the folio id is sent back
		?>
<GTResponse>
	<Folios>
		<Folio id="<?php echo $folio->id; ?>" />
	</Folios>
</GTResponse><?php
	}
	else if ($op == "update")
	{
		$folioId = $_GET["folioId"];
		//update of the source
		if (array_key_exists("sourceId", $_GET))
			setFolioSourceId($p, $folioId, $_GET["sourceId"]);
		//update of the folio number
		else if (array_key_exists("number", $_GET))
		{
			$number = $_GET["number"];
			//send back an error if the number is already in use
			if (folioNumberExistsInSource($p, getFolioSourceId($p, $folioId), $number))
				outputErrorXML(__("Page number already exists"));
			setFolioNumber($p, $folioId, $number);
		}
		//update of the transcription (uploaded in the POST data)
		else if (array_key_exists("transcription", $_GET) && $_GET["transcription"] != "false")
			setFolioTranscription($p, $folioId, $_POST["trans"]);
		//update of the image (uploaded in the POST data)
		else if (array_key_exists("image", $_GET) && $_GET["image"] != "false")
		{
			addFolioImage($p, $folioId);
		}
		//update of the image through a link
		else if (array_key_exists("link", $_GET))
		{
			addFolioLink($p, $folioId, $_GET["link"]);
		}
	}
}
?>
