<?php
require_once(dirname(__FILE__).'/../db/db_tags.php');

function handle($p)
{
	$op = $_GET['op'];
	
	if ($op == "new")
	{
		//associate a fragment with a tag
		if (array_key_exists('fragmentId', $_GET))
			addTagFragment($p, $_GET['tagId'], $_GET['fragmentId']);
		//creation of a new tag (or group as displayed in the UI)
		else
		{
			$tagId = createTag($p, $_GET['tableId']);
			//after creation, send back the tag id
		?>
<GTResponse>
	<Tags>
		<Tag id="<?php echo $tagId;
?>" />
	</Tags>
</GTResponse><?php
		}
	}
	//update of the individual attributes of a given tag
	else if ($op == "update")
	{
		if (array_key_exists('name', $_GET))
			setTagName($p, $_GET['tagId'], $_GET['name']);
		if (array_key_exists('rank', $_GET))
			updateTagRank($p, $_GET['tagId'], $_GET['rank']);
		if (array_key_exists('label', $_GET))
			updateTagLabel($p, $_GET['tagId'], $_GET['label']);
		if (array_key_exists('color', $_GET))
			updateTagColor($p, $_GET['tagId'], $_GET['color']);
	}
	//removal of a tag or of an association with a fragment
	else if ($op == "remove")
	{
		if (array_key_exists('fragmentId', $_GET))
			removeTagFragment($p, $_GET['tagId'], $_GET['fragmentId']);
		else removeTag($p, $_GET['tagId']);
	}
}
?>