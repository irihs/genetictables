<?php
require_once(dirname(__FILE__).'/../db/db_tables.php');
require_once(dirname(__FILE__).'/../db/db_works.php');
require_once(dirname(__FILE__).'/../db/db_sources.php');
require_once(dirname(__FILE__).'/../db/db_tags.php');
require_once(dirname(__FILE__).'/../ajax/ajax_queries.php');

$workId = getTableWorkId($this, $item->id);

//get the list of sources
$sources = array();
$sourcesData = getSourceData($this, $workId);
foreach ($sourcesData as $val)
	$sources[$val['id']] = array(
			$val['name'],
			$val['shortName'],
			$val['rank'], 
			dechex($val['color']));
	
//get the list of tags
$tags = array();
$tagData = getTagData($this, $item->id);
foreach ($tagData as $val)
	$tags[$val['id']] = array(
		$val['name'],
		$val['shortName'],
		$val['rank'],
		dechex($val['color']));

function toHexColor($v)
{
	$col = "".dechex($v);
	while (strlen($col) < 6)
		$col = "0".$col;
	return $col;
}

//get the display settings
$displaySettings = getTableDisplay($this, $item->id)[0];

//get the list of chapters
$chapters = getChapterData($this, $workId);

$fromPage = array_key_exists('fromPage', $_GET) ? $_GET['fromPage'] : '1';
//error_log('111 '.session_id().' nPages'.$item->id." == ".$_SESSION['nPages'.$item->id]);
if (session_status() == PHP_SESSION_NONE)
	session_start();
$nPages = (
	array_key_exists('nPages', $_GET) ? $_GET['nPages'] : 
	(array_key_exists('nPages'.$item->id, $_SESSION) ? $_SESSION['nPages'.$item->id] :
	(int)($displaySettings['pageSpan']/2)));
//$_SESSION['nPages'.$item->id] = $nPages;
// error_log('111 '.session_id().' nPages'.$item->id." = ".$_SESSION['nPages'.$item->id]);

include('gt_viewer_fragment_previewer.php');
include('gt_viewer_layout.php');
include('gt_viewer_canvas.php');
?>

<script>
	"use strict";

	var pagesRow = document.getElementById("pagesRow");
	var chaptersRow = document.getElementById("chaptersRow");
	var fragsRow = document.getElementById("fragRow");
	var sources = [];
	var tags = [];
	var chapters = [];
	var canvas = document.getElementById("canvas");

	<?php
		//put all the sources in JS array
 		foreach ($sources as $sourceId => $source)
		{
			?> 
			var source = {};
			source.id = <?php echo $sourceId; ?>;
			source.name = "<?php echo $source[0]; ?>";
			source.label = "<?php echo $source[1]; ?>";
			source.rank = <?php echo $source[2]; ?>;
			source.col = "<?php echo $source[3]; ?>";
			source.visible = true;
			source.workName = "<?php echo getWorkForSource($this, $sourceId); ?>";
			while (source.col.length < 6)
				source.col = "0"+source.col;
			sources[sources.length] = source;
			<?php 
		}
		
		//put all the tags in JS array
		foreach ($tags as $tagId => $tag)
		{
			?>
			var tag = {};
			tag.id = <?php echo $tagId; ?>;
			tag.name = "<?php echo $tag[0]; ?>";
			tag.label = "<?php echo $tag[1]; ?>";
			tag.rank = <?php echo $tag[2]; ?>;
			tag.col = "<?php echo $tag[3]; ?>";
			while (tag.col.length < 6)
				tag.col = "0"+tag.col;
			tags[tags.length] = tag;
			<?php 
		}
		
		//put all the chapters in JS array
		$prevPage = 0;
		foreach ($chapters as $chapter)
		{
			?>
			var chapter = {};
			chapter.name = "<?php echo $chapter['name']; ?>";
			chapter.toPage = <?php echo $chapter['toPage']; ?>;
			chapter.fromPage = <?php echo $prevPage; ?>;
			chapters[chapters.length] = chapter;
			<?php
			$prevPage = $chapter['toPage'];
		}
	?>

	//fillSourceList();
	fillTagList();

	//finds the index of a source in 'sources' array from an id
	function sourceIndex(sourceId)
	{
		for (var i=0;i<sources.length;i++)
			if (sourceId == sources[i].id)
				return i;
		return -1;
	}
	//finds the index of a tag in 'tags' array from an id
	function tagIndex(tagId)
	{
		for (var i=0;i<tags.length;i++)
			if (tagId == tags[i].id)
				return i;
		return -1;
	}

	//variables to control page range (read by 'gt_viewer_canvas.php', written by 'gt_viewer_layout.php')
	var fromPage = <?php echo $fromPage; ?>, 
		nPages = <?php echo $nPages; ?>;
	var toPage = fromPage+nPages-1;
	var segs = null;
	//refreshes table data and display
	function requestRefresh()
	{
		segs = null;
		refreshCanvas();

		//fill the 'display link' field
		document.getElementById('displayLink').value = window.location.protocol+"//"+window.location.host
			+"<?php echo public_url("/items/show/$item->id");?>"
			+"?fromPage="+fromPage+"&nPages="+nPages;
		//fill the 'from' field
		document.getElementById('fromPageInput').value = fromPage;
		
		//read incipit/explicit data and displayt it in the header row
		readTableIncipit(<?php echo $item->id; ?>, fromPage, toPage, function(res)
		{
			document.getElementById("incipitRow").innerHTML = res.chapter+" : "+res.incipit+" ... "+res.explicit;
		}, function(error) {console.log(error);});
		
		//in case offscreen fragments are required for proper display, spread takes some beyond the page range
		var spread = 0;
		//async query for the fragments in a given page range
		readFragmentsForTable(<?php echo $item->id; ?>, fromPage-spread, toPage+spread, function(res)
		{
			//prepare some extra properties for fragments and build an id-indexed array 
			var fragsById = {};
			for (var i=0;i<res.length;i++)
			{
				res[i].children = [];
				res[i].parent = null;
				res[i].source = sourceIndex(res[i].sourceId);
				res[i].tag = -1;
				for (var j=0;j<res[i].tags.length;j++)
					if (res[i].tagTables[j] == <?php echo $item->id; ?>)
						{res[i].tag = tagIndex(res[i].tags[j]); break;}
				if (res[i].tag == -1)
					continue;
				res[i].prev = null;
				res[i].next = null;
				fragsById[""+res[i].id] = res[i];
			}
			//compute the child-parent and previous-next relationships between the fragments
			for (var i=0;i<res.length;i++)
				if (res[i].tag >= 0)
			{
				if (res[i].parentId != "" && typeof(fragsById[""+res[i].parentId]) != "undefined")
				{
					var parent = fragsById[""+res[i].parentId];
					res[i].parent = parent;
					parent.children[parent.children.length] = res[i];
				}
				if (res[i].previousId != "" && typeof(fragsById[""+res[i].previousId]) != "undefined")
				{
					res[i].prev = fragsById[""+res[i].previousId];
					res[i].prev.next = res[i];
				}
			}

			//create the segments. A segment is a list of fragments bound by the previous-next relationships. A segment is mostly treated as big fragment
			var segments = [];
			var segmentsByFragId = {};
			for (var i=0;i<res.length;i++)
				//if a fragment is not in a segment already
				if (typeof(segmentsByFragId[""+res[i].id]) == "undefined" && res[i].tag >= 0)
			{
				//then build a new segment for it
				var segment = {};
				segments.push(segment);
				//find the first fragment of the segment
				var first = res[i];
				while (first.prev !== null)
					first = first.prev;
				segment.frags = [];
				//doesn't handle multi-source, can this happen?
				segment.source = first.source;
				segment.tag = first.tag;
				segment.parents = [];
				segment.children = [];
				//go through all the fragments and add them to segment
				while (first !== null)
				{
					segment.frags.push(first);
					segmentsByFragId[""+first.id] = segment;
					segment.source = Math.min(segment.source, first.source);
					segment.tag = Math.min(segment.tag, first.tag);
					first = first.next;
				}

				//write the edges of the segment from first and last fragments
				segment.fromPage = segment.frags[0].fromPage;
				segment.fromLine = segment.frags[0].fromLine;
				segment.toPage = segment.frags[segment.frags.length-1].toPage;
				segment.toLine = segment.frags[segment.frags.length-1].toLine;
			}

			//for all segments, compute children and parents by aggregating the fragments inside
			for (var i=0;i<segments.length;i++)
			{
				var segment = segments[i];
				for (var j=0;j<segment.frags.length;j++)
				{
					var frag = segment.frags[j];
					if (frag.parent !== null && typeof(segmentsByFragId[""+frag.parent.id]) != "undefined" && segment.parents.indexOf(segmentsByFragId[""+frag.parent.id]) < 0)
						segment.parents.push(segmentsByFragId[""+frag.parent.id]);
					for (var k=0;k<frag.children.length;k++)
						if (typeof(segmentsByFragId[""+frag.children[k].id]) != "undefined" && segment.children.indexOf(segmentsByFragId[""+frag.children[k].id]) < 0)
							segment.children.push(segmentsByFragId[""+frag.children[k].id]);
				}
			}
			
			//compute depth in tree (unused atm?)
// 			for (var i=0;i<segments.length;i++)
// 				if (segments[i].children.length == 0)
// 					assignDepth(segments[i], 0);
			
			segs = segments;
			//update the display
			refreshCanvas();
		});
	}
	requestRefresh();

	//recursive function for computing depth of fragments (unused atm?)
	function assignDepth(seg, depth)
	{
		if (seg.depth !== undefined && seg.depth <= depth)
			return;
		seg.depth = depth;
		for (var i=0;i<seg.parents.length;i++)
			assignDepth(seg.parents[i], depth+1);
	}

	//get the display roots from a set of segments (ie. segments with no parents, appear highest in table)
	function getDisplayRoots(frags)
	{
		var roots = [];
		for (var i=0;i<frags.length;i++)
			if (frags[i].parents.length == 0)
				roots[roots.length] = frags[i];
		return roots;
	}
</script>
