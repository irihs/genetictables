<script>
'use strict';

//given a set of segments (a segment is a list of fragments that have a previous-next relationship), which one should be displayed first (highest in table)
//logic : eligible if children are displayed (roots at bottom, children at top)
//	if eligible, then use priorities:
//		1. lowest source rank
// 		2. lowest page number
//		3. lowest line number
//if none eligible (shouldn't happen => would mean cyclical hierarchy), select first
function chooseSegment(segs)
{
	var min = null, minSource = null, minTag = null, minPage = null, minLine = null; 
	for (var i=0;i<segs.length;i++)
		if (min === null 
			|| (areParentsDisplayed(segs[i]) &&
				   (sources[segs[i].source].rank > sources[minSource].rank
				|| (sources[segs[i].source].rank == sources[minSource].rank && tags[segs[i].tag].rank > tags[minTag].rank)
				|| (sources[segs[i].source].rank == sources[minSource].rank && tags[segs[i].tag].rank == tags[minTag].rank && segs[i].fromPage < minPage)
				|| (sources[segs[i].source].rank == sources[minSource].rank && tags[segs[i].tag].rank == tags[minTag].rank && segs[i].fromPage == minPage && segs[i].fromLine < minLine))))
		{
			min = i;
			minSource = segs[i].source;
			minTag = segs[i].tag;
			minPage = segs[i].fromPage;
			minLine = segs[i].fromLine;
		}
	return min;
}
//checks if two fragments overlap
function fragmentsIntersect(f1, f2)
{
	if (f1.toPage < f2.fromPage || (f1.toPage == f2.fromPage && f1.toLine <= f2.fromLine)) return false;
	if (f2.toPage < f1.fromPage || (f2.toPage == f1.fromPage && f2.toLine <= f1.fromLine)) return false;
	return true;
}
//checks if a fragment intersects with the fragments already in a row
function fragmentIntersectsRow(fragment, row)
{
	for (var i=0;i<row.length;i++)
		if (fragmentsIntersect(fragment, row[i]))
			return true;
	return false;
}
//inserts a segment in data, starting at index fromRow, making sure there are no intersections. If impossible, a new row is appended
function insertFragment(fragment, data, fromRow)
{
	for (var i=fromRow;i<data.length;i++)
		if (!fragmentIntersectsRow(fragment, data[i]))
		{
			data[i][data[i].length] = fragment;
			fragment.rowIndex = i;
			return;
		}
	//no room, add a new row
	data[data.length] = [fragment];
	fragment.rowIndex = data.length-1;
}
//checks if parents of a segment are displayed
function areParentsDisplayed(fragment)
{
	for (var i=0;i<fragment.parents.length;i++)
		if (!fragment.parents[i].displayed)
			return false;
	return true;
}

//link to the item page of a folio
//function buildFolioLink(folioId) {return function() {var popup = window.open("about:blank", "_blank"); popup.location = "<?php echo public_url("items/show"); ?>/"+folioId+"#folioNav";};}
function buildFolioLink(folioId) {return function() {window.location.assign("<?php echo public_url("items/show"); ?>/"+folioId+"#folioNav");};}
//displays info for a fragment in th einfo area (under the table)
function buildFragmentInfo(fragment, seg) {return function() {document.getElementById('fragInfoDiv').innerHTML = "<i>"+sources[fragment.source].name+"</i>"+
	" folio "+fragment.folioNum+(fragment.position.length > 0 ? " "+fragment.position : "")+","+
	" <?php echo __("p."); ?>"+fragment.fromPage+" <?php echo __("l."); ?>"+fragment.fromLine+
	" - <?php echo __("p."); ?>"+fragment.toPage+" <?php echo __("l."); ?>"+fragment.toLine+
	(fragment.parent !== null ? " (<?php echo __("comes from"); ?> "+sources[fragment.parent.source].label+" "+fragment.parent.folioNum+")" : "")
	//+"("+seg.depth+")"
	;};}
//clears the info area
function clearFragmentInfo() {document.getElementById('fragInfoDiv').innerHTML = "&nbsp;";}

function buildOnMouseMoveFragmentCallback(fragment) {return function(e) {FragmentPreviewer.setFragment(fragment, e.clientX, e.clientY);}};
function buildOnMouseOutFragmentCallback() {return function() {FragmentPreviewer.setFragment(null, 0, 0);}};

function dumpSegment(seg)
{
	var s = "";
	for (var i=0;i<seg.frags.length;i++)
		s += seg.frags[i].id+",";
	console.log("["+s+"]");
}

//build a header cell (either page number or chapter) with a size and position determined by left and right page numbers
function buildHeaderCell(cell, left, right)
{
	var cell = document.createElement("DIV");
	cell.className = "gteTableHeaderCell";
	//switch from page numbers to %
	left = 100*Math.min(left, toPage-fromPage+1)*1./(toPage-fromPage+1);
	right = 100*Math.min(right, toPage-fromPage+1)*1./(toPage-fromPage+1);
	//update styles
	cell.style.position = "absolute"; 
	cell.style.left = left+"%"; 
	cell.style.width = (right-left)+"%"; 
	cell.style.height = defaulth+"px";
	return cell;
}

//clears and fills the table with the given segments
function refreshCanvas()
{
	//clear the display
	while (fragsRow.childNodes.length > 0)
		fragsRow.removeChild(fragsRow.childNodes[fragsRow.childNodes.length-1]);
	if (segs == null)
	{
		//fragsRow.style.height = "";
		var div = document.createElement("DIV");
		div.style.cssText = "text-align: center; width: 100%; height: 100%; vertical-align: top";
		div.innerHTML = "<i><?php echo __("Waiting for server..."); ?></i>";
		fragsRow.appendChild(div);
		document.getElementById("incipitRow").innerHTML = "&nbsp;";
		return;
	}
	//2d array of segments, first index is row
	var data = [];
	
	//first we measure the number of lines on each page : the max of all fragments on that page
	//pageLengths is index by page number (relative to 'fromPage')
	var pageLengths = [];
	for (var i=0;i<segs.length;i++)
	{
		segs[i].displayed = false;
		for (var j=0;j<segs[i].frags.length;j++)
		{
			var fragment = segs[i].frags[j];
			if (fragment.fromPage >= fromPage)
			{
				var fp = fragment.fromPage-fromPage;
				pageLengths[fp] = Math.max(fragment.fromLine, pageLengths.length <= fp || pageLengths[fp] == null ? 0 : pageLengths[fp]);
			}
			if (fragment.toPage >= fromPage)
			{
				var fp = fragment.toPage-fromPage;
				pageLengths[fp] = Math.max(fragment.toLine, pageLengths.length <= fp || pageLengths[fp] == null ? 0 : pageLengths[fp]);
			}
		}
	}

	//set of segments that are 'candidates' for being displayed on the current row. Inited with the 'display roots'
	var curSegs = getDisplayRoots(segs);
	//set of segments that have been treated (defense against cyclical parent/child relationships - which shouldn't happen)
	var closedSegs = [];
	for (var i=0;i<curSegs.length;i++)
		closedSegs.push(curSegs[i]);
	//insert the segments into data
	while (curSegs.length > 0)
	{
		//row from which to insert. If data ends with an empty, use that one (not sure if this can still happen)
		var fromRow = data.length > 0 && data[data.length-1].length == 0 ? data.length-1 : data.length;
		data[fromRow] = [];
		//choose which segment should be inserted
		var i = chooseSegment(curSegs);
		//make sure the source is visible
		if (sources[curSegs[i].source].visible && curSegs[i].toPage >= fromPage && curSegs[i].fromPage <= toPage)
			insertFragment(curSegs[i], data, fromRow);
		curSegs[i].displayed = true;
		//add the parents of this segment to candidate segs
		for (var j=0;j<curSegs[i].children.length;j++)
			if (closedSegs.indexOf(curSegs[i].children[j]) < 0)
			{
				curSegs.push(curSegs[i].children[j]);
				closedSegs.push(curSegs[i].children[j]);
			}
		//remove this segment
		curSegs.splice(i, 1);
	}
	
	fragsRow.style.height = data.length*defaulth+"px";
	//row by row, display all fragments
	for (var i=0;i<data.length;i++)
	{
		var offseth = (data.length-1-i)*defaulth;
		for (var k=0;k<data[i].length;k++)
		{
			var seg = data[i][k];
			for (var j=0;j<seg.frags.length;j++)
			{
				var frag = seg.frags[j];
				if (!frag.isPublic)
					continue;
				//percentage position of the left and right edges of this fragment
				var left = frag.fromPage < fromPage ? 0 : frag.fromPage-fromPage+Math.max(frag.fromLine-1, 0)*1./Math.max(pageLengths[frag.fromPage-fromPage], 1);
				var right = frag.toPage < fromPage ? 0 : frag.toPage-fromPage+Math.max(frag.toLine-1, 0)*1./Math.max(pageLengths[frag.toPage-fromPage], 1);
				left = 100*Math.min(left, toPage-fromPage+1)/(toPage-fromPage+1);
				right = 100*Math.min(right, toPage-fromPage+1)/(toPage-fromPage+1);
				
				if (left < right)
				{
					//a colored div rectangle represents the fragment
					var div = document.createElement("DIV");
					div.style.cssText = "text-align: center; position: absolute; left: "+left+"%; top: "+offseth+"px; width: "+(right-left)+"%; height: "+defaulth+"px; "+ 
						"border: "+(frag.withheld ? "dashed 1px red" : "solid 1px #<?php echo toHexColor($displaySettings['tableColor']); ?>")+"; cursor: pointer; "+
						"color: #<?php echo toHexColor($displaySettings['textColor']); ?>; "+
						"background-color: #"+tags[frag.tag].col;//sources[frag.source].col;
					div.onclick = buildFolioLink(frag.folioId);
//						div.onmouseenter = buildFragmentInfo(frag, seg);
//						div.onmouseleave = clearFragmentInfo;
					div.onmousemove = buildOnMouseMoveFragmentCallback(frag);
					div.onmouseleave = buildOnMouseOutFragmentCallback();
					
					fragsRow.appendChild(div);
					//check if div isn't too small to show a label
					if (div.clientWidth > 1.5*defaulth)
					{
						var label = (sources[frag.source].label.length > 0 ? sources[frag.source].label : sources[frag.source].name.substring(0, 1))
							+"|"+frag.folioNum+(frag.position !== undefined && frag.position.length > 0 ? frag.position.substring(0, 1).toLowerCase() : "");
						div.innerHTML = "<small>"+label+"</small>";
					}
				}
			}
		}
	}

	//update the table header page numbers
	while (pagesRow.childNodes.length > 0)
		pagesRow.removeChild(pagesRow.childNodes[pagesRow.childNodes.length-1]);
	pagesRow.height = 1.*defaulth;
	for (var i=fromPage;i<=toPage;i++)
	{
		//left and right edges of page
		var left = i-fromPage;
		var right = i-fromPage+1;
		//page label
		var div = buildHeaderCell(div, left, right);
		div.innerHTML = "<small>"+i+"</small>";
		pagesRow.appendChild(div);
	}

	//update the table header chapters
	while (chaptersRow.childNodes.length > 0)
		chaptersRow.removeChild(chaptersRow.childNodes[chaptersRow.childNodes.length-1]);
	chaptersRow.height = 1.*defaulth;
	for (var i=0;i<chapters.length;i++)
	{
		if (chapters[i].toPage <= fromPage)
			continue;
		if (chapters[i].fromPage > toPage)
			break;
		var left = Math.max(0, chapters[i].fromPage-fromPage);
		var right = Math.min(toPage-fromPage+1, chapters[i].toPage-fromPage+1);
		var div = buildHeaderCell(div, left, right);
		div.innerHTML = "<small>"+chapters[i].name+"</small>";
		chaptersRow.appendChild(div);
	}
}
</script>