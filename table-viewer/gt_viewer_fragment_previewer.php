<!-- A popup describing a fragment for when the user hovers over one -->
<table id="FragmentPreviewerDiv" 
	style="width: initial; z-index: 10; display: none; text-align: center; position: fixed; left: 0px; top: 0px; background-color: white; border: solid 1px black" onmouseenter="this.style.display = 'none';">
	<tr>
		<!-- The folio miniature is on the left -->
		<td id="FragmentPreviewerImage" 
			style="text-align: center; vertical-align: middle; width: 15em; height: 15em; background-size: contain; background-repeat: no-repeat; background-position: center; background-color: white"></td>
		<td style="text-align: left">
			<!-- A field for the folio label -->
			<b><span id="FragmentPreviewerFolioLabel"></span>
			<br>
			<!-- A field for the fragment label -->
			<span id="FragmentPreviewerFragmentLabel"></span></b>
			<br>
			<!-- A field for extra info -->
			<span id="FragmentPreviewerInfoLabel"></span>
		</td>
	</tr>
</table>

<script>
var FragmentPreviewer = {};

FragmentPreviewer.div = document.getElementById("FragmentPreviewerDiv");
//FragmentPreviewer.div.parentElement.removeChild(FragmentPreviewer.div);
//document.body.appendChild(FragmentPreviewer.div);
FragmentPreviewer.curFragment = null;

//returns standard frag info string
function buildFragLabel(frag)
{
	return "<?php echo __("Fragment"); ?>"+(frag.position !== undefined && frag.position.length > 0 ? " "+frag.position : "")+
		" <?php echo __("p."); ?>"+frag.fromPage+" <?php echo __("l."); ?>"+frag.fromLine+
		" - <?php echo __("p."); ?>"+frag.toPage+" <?php echo __("l."); ?>"+frag.toLine;
}
//create an info label for a fragment detailing parent/children relations
function buildInfoLabel(frag)
{
	var parent = frag.parent;
	var info = (parent !== null ? "<?php echo __("Comes from"); ?> "+sources[parent.source].label+" "
		+parent.folioNum+(parent.position !== undefined && parent.position.length > 0 ? parent.position.substring(0, 1).toLowerCase() : "") : "");
	if (frag.children.length > 0)
	{
		info += "<br><?php echo __("Origin of"); ?> ";
		for (var i=0;i<frag.children.length;i++)
		{
			var child = frag.children[i];
			info += (i > 0 ? ", " : "")+sources[child.source].label+" "
				+child.folioNum+(child.position !== undefined && child.position.length > 0 ? child.position.substring(0, 1).toLowerCase() : "");
		}
	}
	return info;
}

//sets the background of a div to a fragment image url
function buildOnFolioImage(fragment)
{
	return function(img)
	{
		if (fragment != FragmentPreviewer.curFragment)
			return;
		document.getElementById("FragmentPreviewerImage").innerHTML = "";
		document.getElementById("FragmentPreviewerImage").style.backgroundImage = img.length > 0 ? 'url("'+img+'")' : "none";
	};
}
//sets the text of an element to a name
function buildOnFolioName(fragment)
{
	return function(name)
	{
		if (fragment != FragmentPreviewer.curFragment)
			return;
		document.getElementById("FragmentPreviewerFolioLabel").innerHTML = name;
	};
}
//refresh the contents of the popup
FragmentPreviewer.updateDisplay = function ()
{
	if (FragmentPreviewer.div.style.display != "block")
	{
		FragmentPreviewer.div.style.display = "block";
		document.getElementById("FragmentPreviewerImage").innerHTML = "&#128336;";
		document.getElementById("FragmentPreviewerImage").style.backgroundImage = "none";
		document.getElementById("FragmentPreviewerFolioLabel").innerHTML = "&#128336;";
	}
	document.getElementById("FragmentPreviewerFragmentLabel").innerHTML = buildFragLabel(FragmentPreviewer.curFragment);
	document.getElementById("FragmentPreviewerInfoLabel").innerHTML = buildInfoLabel(FragmentPreviewer.curFragment);
	//async requests to the data model
	readFolioMini(FragmentPreviewer.curFragment.folioId, buildOnFolioImage(FragmentPreviewer.curFragment));
	readFolioName(FragmentPreviewer.curFragment.folioId, buildOnFolioName(FragmentPreviewer.curFragment));
}
//set fragment and position of popup
FragmentPreviewer.setFragment = function (fragment, x, y)
{
	//update position
	if (fragment != null)
	{
		//TODO: right side of screen problems
		FragmentPreviewer.div.style.left = (x < window.innerWidth/2 ? x+20 : x-FragmentPreviewer.div.clientWidth-20)+"px";
		FragmentPreviewer.div.style.top = (y+10)+"px";
	}
	if (fragment == FragmentPreviewer.curFragment)
		return;
	FragmentPreviewer.curFragment = fragment;

	//if no fragment, hide the popup
	if (fragment == null)
		FragmentPreviewer.div.style.display = "none";
	//otherwise wait one second and if setFragment() isn't called with a different fragment, update
	else setTimeout(function() {if (fragment == FragmentPreviewer.curFragment) FragmentPreviewer.updateDisplay();}, 1000);
}

</script>
