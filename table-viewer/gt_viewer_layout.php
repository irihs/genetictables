<?php 
/**
 * Layout of the public table view.
 */
require_once(dirname(__FILE__).'/../gt_menu.php');
?>
<style>
.gteTableHeaderCell {position: absolute; text-align: center; top: 0px; background-color: white; border: 1px solid #ccc;}
</style>

<div>
     <div> 
         <?php
         $workId = getTableWorkId($this, $item->id);
         buildMenu($this, $workId, $item);
         ?>
     </div>
</div>
<br/>

<!-- Displays a URL to the current page with its size settings. The URL is initially hidden behind a link. -->
<span>
	<a onclick="document.getElementById('displayLink').style.display = 'inline'; this.style.display = 'none';" style="cursor: pointer;">
		<u><?php echo __("Link"); ?></u>
	</a>
	<textarea id="displayLink" rows="1" cols="80" readonly style="resize: none;font-size: .8em; display: none" onclick="this.select()"></textarea>
</span>
<br/>

<!-- Displays dropdown lists to select a source and a specific folio and links to the public page of that folio -->
<span>
	<a onclick="document.getElementById('displayFolio').style.display = 'inline'; this.style.display = 'none';" style="cursor: pointer;">
		<u><?php echo __("Display folio"); ?></u>
	</a>
	<div id='displayFolio' style='display: none'>
		<?php echo __("Source"); ?>
		<select onchange='displaySourceSelected(this.value);'>
			<option value="-1">&nbsp;</option>
		<?php
		foreach ($sources as $sourceId => $source)
		{
			?><option value="<?php echo $sourceId; ?>"><?php echo $source[0]; ?></option><?php 
		}
		?>
		</select>&nbsp;&nbsp;&nbsp;
		
		<?php echo __("Folio"); ?>
		<select id='displayFolioList' onchange="displayFolioSelected(this.value)"></select>
	</div>
</span>

<script>
	//a folio has been selected, go to the public page
	function displayFolioSelected(value)
	{
		if (parseInt(value) >= 0)
		{
			window.location.assign("<?php echo public_url("items/show"); ?>/"+value+"#folioNav");
		}
	}
	//a source has been selected, make async request to fill folio dropdown list
	function displaySourceSelected(value)
	{
		var displayFolioList = document.getElementById('displayFolioList');
		if (parseInt(value) >= 0)
		{
			//clear folio list
			displayFolioList.innerHTML = "";
			//make async request for folios
			readFolios(<?php echo $workId; ?>, value, 
				function(folios)
				{
					//fill folio list
					for (var i=0;i<folios.length;i++)
					{
						var option = document.createElement("OPTION");
						displayFolioList.appendChild(option);
						option.value = folios[i].id;
						option.innerHTML = folios[i].number;
					}
				}, 
				function() {});
		}
		//if no source is selected
		else displayFolioList.innerHTML = "<option value='-1'><?php echo __("Select a source");?></option>";
	}
	displaySourceSelected("-1");
</script>

<!-- Main area layout -->
<div id="gtDiv" style="position: relative">
	<!-- View settings: first page, number of pages, prev/next nevigation buttons -->
	<div style="text-align: center">
		<a style="color: blue; cursor: pointer; font-size: large;" onClick="goLeft()">&larr;</a>
		&nbsp;<?php echo __("Starting at"); ?>&nbsp;
		<input type="number" id="fromPageInput" min="1" value="<?php echo $fromPage; ?>" style="width: 9em" />
		&nbsp;<?php echo __("show"); ?>&nbsp;
		<input type="number" id="nPagesInput" min="1" max="<?php echo $displaySettings['pageSpan']; ?>" value="<?php echo $nPages; ?>" style="width: 7em"/>
		&nbsp;<?php echo __("pages"); ?>&nbsp;
		<a style="color: blue; cursor: pointer; font-size: large;" onClick="goRight()">&rarr;</a>
	</div>
	
	<!-- the genetic table itself -->
	<table id="canvas" style="border: none; margin-bottom: 0px">
		<!-- shows incipit, explicit and chapter at the top -->
		<tr>
			<td id="incipitRow" style="position: relative; top: 0px; left: 0px"></td>
		</tr>
		<!-- page columns, hidden if chapter display is set -->
		<tr>
			<td id="pagesRow" style="position: relative; top: 0px; left: 0px; <?php if ($displaySettings['chapterDisplay'] != 0) echo "display: none" ?>"></td>
		</tr>
		<!-- chapter columns, hidden if page display is set -->
		<tr>
			<td id="chaptersRow" style="position: relative; top: 0px; left: 0px; <?php if ($displaySettings['chapterDisplay'] == 0) echo "display: none" ?>"></td>
		</tr>
		<!-- the area containing the fragment bars -->
		<tr>
			<td id="fragRow" style="position: relative; top: 0px; left: 0px; background-color: #<?php echo toHexColor($displaySettings['tableColor']); ?>;"></td>
		</tr>
	</table>
	<div id="fragInfoDiv" style="display: none">
		&nbsp;
	</div>
	<div>
		<?php echo getTableFooter($this, $item->id); ?>
	</div>
	
	<br/>
	<div style="background-color: #E0E0E0; border: solid 1px #BDBDBD">
	<!-- h3><?php echo __("Sources"); ?></h3 -->
	<table id="sourceList" style="border: none">
	</table>
	</div>
</div>

<script>
//function to show/hide sources in table
function visibilityChangeFunc(i)
{
	return function() {sources[i].visible = document.getElementById("visible"+i).checked; refreshCanvas();};
}
//adds an entry for a source (at index) to display color and give a visibility toggle. Negative index shows 'withheld' fragment decoration
function buildSourceEntry(index, list, tr)
{
	var merge = [];
	if (index >= 0)
	{
		//find groups with same name
		for (var i=0;i<tags.length;i++)
			if (i == index || tags[i].name == tags[index].name)
				merge.push(tags[i]);
		//if this is not the first group, skip (it has already been included)
		if (tags[index] != merge[0])
			return;
	}
	else merge.push(null);

	if (tr == null)
	{
		tr = document.createElement("TR");
		list.appendChild(tr);
	}

	var td = document.createElement("TD");
	td.style.border = "none";
	td.style.padding = "0.25em";
	//four tags per row
	td.style.width = "25%";
	td.style.verticalAlign = "middle";
	tr.appendChild(td);

	for (var i=0;i<merge.length;i++)
	{
		var group = merge[i];

		//toggle for visibility (unused!)
		var toggle = document.createElement("INPUT");
		td.appendChild(toggle);
		toggle.id = "visible"+index;
		toggle.type = "checkbox";
		if (index >= 0)
		{
			toggle.checked = true;
			group.visible = true;
			toggle.onchange = visibilityChangeFunc(index);
		}
		else toggle.disabled = true;
		toggle.style.display = "none";
	
		//square showing source color
		var div = document.createElement("DIV");
		td.appendChild(div);
		if (group != null)
			div.style.backgroundColor = "#"+group.col;
		else div.style.border = "dashed 1px red";
		div.style.display = "inline-block";
		div.style.width = (.7*defaulth)+"px";
		div.style.height = (.7*defaulth)+"px";

		var space = document.createElement("SPAN");
		space.innerHTML = "&nbsp;";
		td.appendChild(space);
	}

	//source name
	div = document.createElement("DIV");
	td.appendChild(div);
	div.innerHTML = "&nbsp;<small>"+(index >= 0 ? tags[index].name : "<?php echo __("Withheld"); ?>")+"</small>";
	div.style.display = "inline-block";
	div.style.verticalAlign = "top";

	return tr;
}
function fillTagList()
{
	var tr = null;
	var sourceList = document.getElementById("sourceList");

	//an entry for each source
	for (var i=0;i<tags.length;i++)
		tr = buildSourceEntry(i, sourceList, i%4 == 0 ? null : tr);
	//an entry for 'withheld' fragments
	buildSourceEntry(-1, sourceList, i%4 == 0 ? null : tr);
}

//changes in fields validated by pressing 'enter'
document.getElementById('fromPageInput').onkeypress = function(e) {if (isEnter(e)) setFromPage(document.getElementById('fromPageInput').value);};
document.getElementById('nPagesInput').onkeypress = function(e)
{
	if (isEnter(e))
	{
		var input = document.getElementById('nPagesInput');
		var nPages = input.value;
		//manual input overrides max so we need to check
		var max = parseInt(input.max);
		if (nPages > max)
		{
			nPages = max;
			input.value = nPages;
		}
		setNPages(nPages);
	}
};
function isEnter(e)
{
	if (!e) e = window.event;
	var kc = e.keyCode || e.which;
	if (kc === 13)
		return true;
	return false;
}
//shift view nPages left
function goLeft()
{
	fromPage = Math.max(1, fromPage-nPages);
	toPage = fromPage+nPages-1;
	requestRefresh();
}
//shift view nPages right
function goRight()
{
	fromPage += nPages;
	toPage = fromPage+nPages-1;
	requestRefresh();
}
//set first page
function setFromPage(num)
{
	fromPage = parseInt(num);
	toPage = fromPage+nPages-1;
	requestRefresh();
}
//set number of pages displayed
function setNPages(num)
{
	writeUpdateSessionPageSpan(<?php echo $item->id; ?> , num);
	nPages = parseInt(num);
	toPage = fromPage+nPages-1;
	requestRefresh();
}
//little trick to get default font size : write something in a div, add it to page, measure height, remove it from page
function getDefaultFontSize()
{
	var pa = document.body;
	var who = document.createElement('div');
	who.style.cssText = 'display:inline-block; padding:0; line-height:1; position:absolute; visibility:hidden; font-size:1em';
	who.appendChild(document.createTextNode('M'));
	pa.appendChild(who);
	var fs = who.offsetHeight;
	pa.removeChild(who);
	return fs;
}
var defaulth = 1.5*getDefaultFontSize();
</script>
