<?php
require_once(dirname(__FILE__).'/../db/db_works.php');
require_once(dirname(__FILE__).'/../db/db_tables.php');

$img = getWorkImage($this, $item->id);
$tableIds = getWorkTables($this, $item->id);
?>
<table class="full">
    <tr>
    	<td style="border: none"><div style="width: 25em; height: 25em; background-size: contain; background-repeat: no-repeat; background-position: center; background-color: none; 
    		background-image: url('<?php echo $img; ?>')"></div></td>
    	<td style="border: none; width: 100%; padding: 1em">
    		<table>
	    		<tr><td><h2>&nbsp;<a href="<?php echo public_url("/items/show/$item->id"); ?>">
	    			<?php echo metadata($item, array("Dublin Core", "Title")); ?></a></h2></td></tr>
	    		<?php
	    		foreach ($tableIds as $tableId)
	    		{
	    			$table = $this->itemTable->find($tableId['id']);
	    			?><tr><td style="text-align: left"><a href="<?php echo public_url("/items/show/$table->id"); ?>">
	    				<?php echo __("Genetic Table: ").getTableShortTitle($this, $table->id); ?></a></td></tr><?php 
	    		}
	    		$notes = workHasAuthorNotes($this, $item->id);
	    		$censored = workHasCensorship($this, $item->id);
	    		$full = workHasFullTranscription($this, $item->id);
	    		$prev = false;
	    		if ($notes || $censored || $full)
	    		{
		    		?><tr><td style="text-align: left"><?php 
	    			if ($notes)
		    		{
		    			?><a href="<?php echo public_url("/items/show/$item->id?notes=true"); ?>">
		    				<?php echo __("Author Notes"); ?></a>&nbsp;&nbsp;&nbsp;<?php
		    			$prev = true;
		    		}
		    		if ($censored)
		    		{
		    			if ($prev) echo '|&nbsp;&nbsp;&nbsp;';
		    			?><a href="<?php echo public_url("/items/show/$item->id?censored=true"); ?>">
		    				<?php echo __("Censored"); ?></a>&nbsp;&nbsp;&nbsp;<?php
		    			$prev = true;
		    		}
		    		if ($full)
		    		{
		    			if ($prev) echo '|&nbsp;&nbsp;&nbsp;';
		    			?><a href="<?php echo public_url("/items/show/$item->id?full=true"); ?>">
		    				<?php echo __("Work"); ?></a>&nbsp;&nbsp;&nbsp;<?php
		    			$prev = true;
		    		}
		    		?></td></tr><?php
	    		}
	    		?>
	    		
	    	</table>
	    </td>
	</tr>
</table>