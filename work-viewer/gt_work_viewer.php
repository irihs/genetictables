<?php
/**
 * Public viewer for a work. Handles the display of author notes, censorship information and the full work transcription.
 */
require_once(dirname(__FILE__).'/../db/db_works.php');
require_once(dirname(__FILE__).'/../db/db_tables.php');
require_once(dirname(__FILE__).'/../gt_menu.php');
?>
<div>
     <div> 
     	<?php 
     	//add a menu at the top
     	buildMenu($this, $item->id, $item);
        ?>
        
     </div>
</div>
<br/>

<?php
//display author notes if requested
if (array_key_exists('notes', $_GET) && $_GET['notes'] == "true") 
{ ?>
	<div id="authorNotes">
		<?php
		//description of author notes (in work metadata)
		echo "<big>".metadata($item, array('Item Type Metadata', $this->workAuthorNotesElementName))."</big><br/><br/>";
		$notes = getWorkFoliosWithAuthorNotes($this, $item->id);
		if (count($notes) > 0)
			//for each author note
			foreach ($notes as $note)
		{
			$folio = get_db()->getTable('Item')->find($note["id"]);
			$elems = $folio->getItemTypeElements();
			//display a header with folio data on the left and a link to the folio on the right
			echo '<table style="background-color: #fafafa"><tr><td><b>'
				.metadata($folio->getCollection(), array('Dublin Core', 'Title'))
				." folio ".metadata($folio, array('Item Type Metadata', $this->numberElementName))
				//.'</b></td><td style="text-align: right"><a href="">'.__("Go to folio").'</a></td></tr></table>';
			.'</b></td><td style="text-align: right">'.link_to($folio, "show", __("Go to folio")).'</td></tr></table>';
			echo '<br/>';
			//display the note itself
			echo metadata($folio, array('Item Type Metadata', $this->folioAuthorNotesElementName));
			echo '<br/>';
			?><br/><?php 
		}
		else echo __("No author notes to display");
		?>
	</div>
<?php }
//display censorship information if requested
else if (array_key_exists('censored', $_GET) && $_GET['censored'] == "true")
{ ?>
	<div id="censored">
		<?php
		//description of censorship information (in work metadata)
		echo "<big>".metadata($item, array('Item Type Metadata', $this->workCensoredElementName))."</big><br/><br/>";
		$censored = getWorkFoliosWithCensorship($this, $item->id);
		if (count($censored) > 0)
			//for each extract
			foreach ($censored as $extract)
		{
			$folio = get_db()->getTable('Item')->find($extract["id"]);
			$elems = $folio->getItemTypeElements();
			//display a header with folio data on the left and a link to the folio on the right
			echo '<table style="background-color: #fafafa"><tr><td><b>'
				.metadata($folio->getCollection(), array('Dublin Core', 'Title'))
				." folio ".metadata($folio, array('Item Type Metadata', $this->numberElementName))
				//.'</b></td><td style="text-align: right"><a href="">'.__("Go to folio").'</a></td></tr></table>';
			.'</b></td><td style="text-align: right">'.link_to($folio, "show", __("Go to folio")).'</td></tr></table>';
			echo '<br/>';
			//display the extract itself
			echo metadata($folio, array('Item Type Metadata', $this->folioCensoredElementName));
			echo '<br/>';
			?><br/><?php 
		}
		else echo __("No censorship information");
		?>
	</div>
<?php }
//display the full transcription if requested
else if (array_key_exists('full', $_GET) && $_GET['full'] == "true")
{
	require_once(dirname(__FILE__).'/../folio-viewer/gt_transcription_styles.php');
	//get names and pages of all chapters
	$chapters = getChapterData($this, $item->id);
	//requested chapter number, -1 if none
	$cur = array_key_exists('chapter', $_GET) ? $_GET['chapter'] : -1;
	?>
	<table>
		<tr>
			<td style="width: 20%; vertical-align: text-top">
				<?php
				//display the chapters in the left column
				$cnt = 0;
				foreach ($chapters as $chapter)
				{
					echo ($cnt == $cur ? "<b>" : "")
						."<big><a href='".public_url("/items/show/$item->id?full=true&chapter=$cnt")."'>".$chapter['name']."</a></big><br/>"
						.($cnt == $cur ? "</b>" : "");
					$cnt++;
				}
				?>
			</td>
			<td>
				<?php if ($cur >= 0) { ?>
				<h3><?php echo $chapters[$cur]['name']; ?></h3>
				<?php 
				//display the requested chapter if any
				echo getWorkFullTranscription($this, $item->id, $cur == 0 ? -1 : $chapters[$cur-1]['toPage'], $chapters[$cur]['toPage']); 
				} ?>
			</td>
		</tr>
	</table>
	<?php
}
//if no display is requested, show general information
else 
{
	//display the work image if any
	$img = getWorkImage($this, $item->id);
	?>
	<div style="width: 100%; text-align: center"><img src="<?php echo $img; ?>"></img></div><br/>
	<?php 
}
?>
