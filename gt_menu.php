<style>
.gtMenu {border: 1px solid #ccc; display: inline; position: static; padding: .5em; color: #369; cursor: pointer; text-decoration: underline;}
.gtMenuHl {border: 1px solid #369; display: inline; position: static; padding: .5em; color: #369; cursor: pointer; text-decoration: underline;}
</style>
<?php 
//public menu displayed at the top of works and tables
//empty items are hidden
function buildMenu($p, $workId, $item)
{
	?>&nbsp;<a class="gtMenu" href="<?php echo public_url(""); ?>"><?php echo __('Home'); ?></a>&nbsp;<?php
	$tableIds = getWorkTables ( $p, $workId );
	//links for all tables in the work
	foreach ( $tableIds as $tableId )
	{
		$table = $p->itemTable->find ( $tableId ['id'] );
		?>&nbsp;<a class="<?php echo ($item->id == $table->id ? "gtMenuHl" : "gtMenu"); ?>" 
			href="<?php echo public_url("/items/show/$table->id"); ?>"><?php echo getTableShortTitle($p, $table->id); ?></a>&nbsp;<?php
	}
	//link for author notes
	if (workHasAuthorNotes ( $p, $workId))
	{
		?>&nbsp;<a class="<?php echo ($item->id == $workId && array_key_exists('notes', $_GET) && $_GET['notes'] == "true" ? "gtMenuHl" : "gtMenu"); ?>" 
			href="<?php echo public_url("/items/show/$workId?notes=true"); ?>"><?php echo __('Author Notes'); ?></a>&nbsp;<?php
	}
	//link for censorship information
	if (workHasCensorship ( $p, $workId))
	{
		?>&nbsp;<a class="<?php echo ($item->id == $workId && array_key_exists('censored', $_GET) && $_GET['censored'] == "true" ? "gtMenuHl" : "gtMenu"); ?>" 
			href="<?php echo public_url("/items/show/$workId?censored=true"); ?>"><?php echo __('Censored'); ?></a>&nbsp;<?php
	}
	//link for the full transcription
	if (workHasFullTranscription ( $p, $workId))
	{
		?>&nbsp;<a class="<?php echo ($item->id == $workId && array_key_exists('full', $_GET) && $_GET['full'] == "true" ? "gtMenuHl" : "gtMenu"); ?>"
			href="<?php echo public_url("/items/show/$workId?full=true"); ?>"><?php echo __('Work'); ?></a>&nbsp;<?php
	}
}
?>