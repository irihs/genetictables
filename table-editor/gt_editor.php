<?php 
/**
 * Main file for the genetic table editor.
 * Reading and writing data is done asynchronosly with the methods in "gt_editor_queries.php".
 * Static HTML elements are defined in "gt_editor_layout.php".
 * The tf import section is defined in  "gt_editor_import.php" (for both uploaded images and flaubert database exports).
 */
require_once(dirname(__FILE__).'/../utils.php');
require_once(dirname(__FILE__).'/../db/db_tables.php');
require_once(dirname(__FILE__).'/../db/db_works.php');
require_once(dirname(__FILE__).'/../db/db_sources.php');
//phpinfo();

//get the sources for this table (for incipit selection)
$sources = getSourceDataForTable($this, $item->id);
?>

<script>
"use strict";

//create an array of sources with their name and id
var sources = [];
var source;
<?php foreach ($sources as $source) { ?>
source = {};
source.id = <?php echo $source["id"]; ?>;
source.name = "<?php echo $source["name"]; ?>";
sources.push(source);
<?php } ?>

//arrays of items that are displayed. A null value means the list awaits an async query to be made
//at the bottom of this file are the calss that will initiate the data requests
//the items are in parsed form (see "parsers.php")
var currentTagList = null;
var currentTFList = null;
var currentDisplaySettings = null;

//ids of the items that are currently being edited (none for fragments because all fragments are displayed at once)
var currentTagId = -1;
var currentTFId = -1;

//make all inputs in the editor disabled
function setEnabled(enabled)
{
	setElementEnabled(document.getElementById("tfEditor"), enabled);
	document.getElementById("activityZoneMessage").innerHTML = enabled ? "&nbsp;" : "<?php echo __("Waiting for server..."); ?>";
}

//returns the current tag in parsed form based on the value of currentTagId
function getCurrentTag()
{
	if (currentTagList === null || currentTagId < 0)
		return null;
	for (var i=0;i<currentTagList.length;i++)
		if (currentTagList[i].id == currentTagId)
			return currentTagList[i];
	return null;
}
//returns the current tag-fragment in parsed form based on the value of currentTFId
function getCurrentTF()
{
	if (currentTFList === null || currentTFId < 0)
		return null;
	for (var i=0;i<currentTFList.length;i++)
		if (currentTFList[i].id == currentTFId)
			return currentTFList[i];
	return null;
}
function tagIdExists() {return getCurrentTag() !== null;}
function tfIdExists() {return getCurrentTF() !== null;}
//refresh display settings after a read
function onDisplaySettingsRead(res)
{
	currentDisplaySettings = res.length > 0 ? res[0] : null;
	refreshDisplaySettingsCell();
	setEnabled(true);
}
function onDisplaySettingsFailed()
{
	currentDisplaySettings = null;
	refreshDisplaySettingsCell();
	setEnabled(true);
}

//callback after requestinbg the list of tags. If the currentTagId is undefined or invalid, it defaults to the first element of the list
function onTagsRead(res)
{
	currentTagList = res;
	if (!tagIdExists())
		currentTagId = -1;
	if (currentTagId < 0 && res.length > 0)
		currentTagId = res[0].id;
	refreshTagList(); 
	setEnabled(true);
	refreshTFEditor();
}
function onTagsFailed()
{
	currentTagList = [];
	currentTagId = -1;
	refreshTagList();
	setEnabled(true);
	refreshTFEditor();
}

//callback after requestinbg the list of tfs. If the currentTFId is undefined or invalid, it defaults to the first element of the list
function onTFsRead(res)
{
	currentTFList = res;
	if (!tfIdExists())
		currentTFId = -1;
	if (currentTFId < 0 && res.length > 0)
		currentTFId = res[0].id;
	refreshTFList();
	setEnabled(true);
	refreshTFEditor();
}
function onTFsFailed() 
{
	currentTFList = [];
	currentTFId = -1;
	refreshTFList();
	setEnabled(true);
	refreshTFEditor();
}

//main refresh function that determines which queries should be made
function refreshTFEditor()
{
	//if the tag list is null, request tags
	if (currentTagList === null)
	{
		setEnabled(false);
		currentTFList = null;
		readTags(<?php echo $item->id; ?>, 
			onTagsRead,
			onTagsFailed);
		return;
	}
	//if the tf list is null, request tfs
	if (currentTFList === null)
	{
		if (currentTagId < 0)
			onTFsFailed();
		else
		{
			setEnabled(false);
			readTagFragments(<?php echo $item->id; ?>, currentTagId, 
				onTFsRead,
				onTFsFailed);
		}
		return;
	}
}

//builds a callback for the selection of a tag
function buildTagsOnClick(tag)
{
	return function() {currentTagId = tag.id; currentTFList = null; refreshTagList(); refreshTFEditor();};
}
//builds a callback for the selection of a tf
function buildTFsOnClick(tf)
{
	return function() {currentTFId = tf.id; refreshTFList(); refreshTFEditor();};
}
//strips the full tag name of the work name
function shortTagName(name)
{
	return name;
}
//refreshes the 'tag' section of the interface with the list of tags and the properties of the currently selected tag
function refreshTagList()
{
	var tag = getCurrentTag();
	
	if (typeof updateImportTag !== 'undefined')
		updateImportTag(tag !== null ? tag.id : "", tag !== null ? tag.name : "");
	//console.log("???"+currentTagList.length);
	//clear and repopulate the list of tags
	var tfEditorTags = document.getElementById("tfEditorTags");
	clear(tfEditorTags);
	if (currentTagList !== null)
		for (var i=0;i<currentTagList.length;i++)
	{
		if (i > 0)
			tfEditorTags.appendChild(createSpace());
		//a link is created for each tag
		var link = document.createElement("BUTTON");
		link.style.cssText = "border-style: none; background: none; text-decoration: none; text-shadow: none; color: black; box-shadow: none; margin: 0px";
		link.innerHTML = shortTagName(currentTagList[i].name);
		link.type = "button";
		link.onclick = buildTagsOnClick(currentTagList[i]);
		//highlight the selected tag
		if (currentTagId == currentTagList[i].id)
		{
			link.style.color = "blue";
			link.style.backgroundColor = "#eeeeee";
		}
		tfEditorTags.appendChild(link);
	}
	//a static label if the list is empty (so you know its not because of an error)
	if (currentTagList === null || currentTagList.length == 0)
		tfEditorTags.appendChild(document.createTextNode("<?php echo __("No groups to display"); ?>"));

	//fill the drop down lists (for the tag of a tf and for the tag merge action)
	var tfTagSelect = document.getElementById("tfTagSelect");
	clear(tfTagSelect);
	var tagMergeSelect = document.getElementById("tagMergeSelect");
	clear(tagMergeSelect);
	if (currentTagList !== null)
		for (var i=0;i<currentTagList.length;i++)
	{
		var option = document.createElement("OPTION");
		option.innerHTML = shortTagName(currentTagList[i].name);
		option.value = currentTagList[i].id;
		tfTagSelect.appendChild(option);
		tagMergeSelect.appendChild(option.cloneNode(true));
	}

	var tagInfo = document.getElementById("tagCell");
	var tfsCell = document.getElementById("tfsCell");
	//if no tag is selected (ie. the tag list is empty), hide the tag fields form and the entire tf section
	if (tag === null)
	{
		tagInfo.style.display = "none";
		tfsCell.style.display = "none";
	}
	//otherwise fill all the fields
	else
	{
		tagInfo.style.display = "table-cell";
		//tfsCell.style.display = "table-row";
		document.getElementById("tagMergeSelect").value = tag.id;
		document.getElementById("tagNameInput").value = shortTagName(tag.name);
		document.getElementById("tagRankInput").value = tag.rank;
		document.getElementById("tagLabelInput").value = tag.label;
		document.getElementById("tagColorInput").value = tag.color;
		document.getElementById("tagColorDisplay").style.backgroundColor = "#"+tag.color;
	}
	//tfTagSelect.value = currentTagId;
}

//callback when the 'Remove' button is hit for a tf : opens the Omeka item edit page in a new tab/window
function buildTFLink(tfId) {return function() 
	{var popup = window.open("about:blank", "_blank"); popup.location = "<?php echo admin_url("items/edit"); ?>/"+tfId;};}
//callback when the 'Remove' button is hit for a tf : opens the Omeka item edit page in a new tab/window
function buildTFTranscriptionLink(tfId) {return function() 
	{var popup = window.open("about:blank", "_blank"); popup.location = "<?php echo admin_url("items/edit"); ?>/"+tfId+"#transcription-metadata";};}
//refreshes the 'tag-fragment' section of the interface with the list of tfs and the properties of the currently selected tf
function refreshTFList()
{
	//clear and repopulate the list of tfs
	var tfEditorTFs = document.getElementById("tfEditorTFs");
	clear(tfEditorTFs);
	if (currentTFList != null)
		for (var i=0;i<currentTFList.length;i++)
	{
		if (i > 0)
			tfEditorTFs.appendChild(createSpace());
		//a link is created for each tag
		var link = document.createElement("BUTTON");
		link.style.cssText = "border-style: none; background: none; text-decoration: none; text-shadow: none; color: black; box-shadow: none; margin: 0px";
		link.innerHTML = "<?php echo __("Fragment"); ?> "+currentTFList[i];
		link.onclick = buildTFsOnClick(currentTFList[i]);
		//highlight the selected tf
		if (currentTFId == currentTFList[i].id)
		{
			link.style.color = "blue";
			link.style.backgroundColor = "#eeeeee";
		}
		tfEditorTFs.appendChild(link);
		if (currentTFId == currentTFList[i].id)
			tfEditorTFs.scrollTop = parseInt(link.offsetTop);
	}
	if (currentTFList === null || currentTFList.length == 0)
		tfEditorTFs.appendChild(document.createTextNode("<?php echo __("No fragments to display"); ?>"));

	var tfInfo = document.getElementById("tfCell");
	var tf = getCurrentTF();
	//if no tf is selcted (empty list), hide the tf fields and the fragment section
	if (tf === null)
	{
		tfInfo.style.display = "none";
		document.getElementById("removeTFLink").href = "";
		document.getElementById("editTFLink").onclick = function() {};
	}
	//otherwise fill the tf fields
	else
	{
		tfInfo.style.display = "table-cell";
		document.getElementById("tfTagSelect").value = tf.tagId;
		document.getElementById("tfNumberInput").value = tf.number;
		document.getElementById("removeTFLink").href = buildTFLink(tf.id);
		document.getElementById("editTFLink").onclick = buildTFTranscriptionLink(tf.id);
	}
}
//refresh the display settings frame
function refreshDisplaySettingsCell()
{
	if (currentDisplaySettings == null)
		return;
	var displayUnitSelect = document.getElementById("displayUnitSelect");
	displayUnitSelect.value = currentDisplaySettings.chapterDisplay ? "chapter" : "page";
	var displayPageSpanInput = document.getElementById("displayPageSpanInput");
	displayPageSpanInput.value = currentDisplaySettings.pageSpan;
	var displayTableColorInput = document.getElementById("displayTableColorInput");
	displayTableColorInput.value = currentDisplaySettings.tableColor;
	document.getElementById("displayTableColorDisplay").style.backgroundColor = "#"+currentDisplaySettings.tableColor;
	var displayTextColorInput = document.getElementById("displayTextColorInput");
	displayTextColorInput.value = currentDisplaySettings.textColor;
	document.getElementById("displayTextColorDisplay").style.backgroundColor = "#"+currentDisplaySettings.textColor;
	var displayIncipitInput = document.getElementById("displayIncipitInput");
	var incipitIndex = -1;
	for (var i=0;i<sources.length;i++)
		if (sources[i].id == currentDisplaySettings.incipitSourceId)
		{incipitIndex = i; break;}
	displayIncipitInput.selectedIndex = incipitIndex+1;
}
</script>

<?php
include('gt_editor_queries.php');
include('gt_editor_layout.php');
?>

<script>
//init the page with a first refresh (this will detect the 'null' item lists and trigger read requests)
refreshTFEditor();
refreshDisplaySettings();
refreshTags();
</script>
