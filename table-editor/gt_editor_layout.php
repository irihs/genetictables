<!-- ?php phpinfo(); ? -->
<!--
All the static elements of the table editing interface.
Dynamic behavior is in "gt_editor.php" 
 -->

<script>
"use strict";
//generic error handler used by "async_queries.php"
//pops up an error zone at the top of the page with the message that the user can then hide
function errorHandler(error)
{
	var errorZone = document.getElementById("errorZone");
	if (error === null)
		errorZone.style.display = "none";
	else
	{
		document.getElementById("errorZoneMessage").innerHTML = error;
		errorZone.style.display = "table";
		window.location = "#";
	}
}

function requestUpdateTag()
{
	updateTag(
		document.getElementById('tagNameInput').value,
		document.getElementById('tagLabelInput').value,
		document.getElementById('tagRankInput').value,
		document.getElementById('tagColorInput').value);
}
function requestRemoveTag()
{
	removeTag();
}
function requestUpdateTableDisplay()
{
	updateDisplaySettings(
		document.getElementById('displayUnitSelect').value, 
		document.getElementById('displayPageSpanInput').value, 
		document.getElementById('displayTableColorInput').value,
		document.getElementById('displayTextColorInput').value,
		document.getElementById('displayIncipitInput').value);
}
</script>

<style>
.gteSectionCell {border: none;}
.gteSection {margin-bottom: 2em; border: 1px solid gray}
.gteSelectionListRow {}
.gteSelectionList {border: none; border-bottom: 1px solid lightgray; margin: .1em; padding: .1em; overflow-y: auto; resize: vertical; font-size: small}
.gteHeader {border: none; background: royalblue; color:white; height: 1.5em}
.gteSubHeader {border: none; background: lightgray; font-weight: bold}
</style>

<!-- Popup error area, can be hidden -->
<span id="activityZoneMessage" style="color: #a0a0ff">&nbsp;</span>
<table id="errorZone" style="width: 100%; display: none; background-color: #dddddd"><tr>
	<td><span id="errorZoneMessage" style="color: red"></span></td>
	<td style="text-align: right"><a style="color: blue; text-decoration: underline" onClick="errorHandler(null);"><?php echo __("Hide"); ?></a></td>
</tr></table>
<table id="tfEditor">

	<!-- Tag editing section -->
	<tr><td class="gteSectionCell"><table class="gteSection">
		<tr><td class="gteHeader">
			<b><?php echo __("Groups"); ?></b>
			&nbsp;&nbsp;&nbsp;
			<input type="button" value="<?php echo __("New group"); ?>" onclick="newTag();">
		</td></tr>
		<tr><td style="border: none"><small><?php echo __("List of groups in this table. Click on a tag to edit it."); ?></small></td></tr>
		<tr class="gteSelectionListRow"><td class="gteSelectionList" id="tfEditorTags"></td></tr>
		<tr><td style="border: none" id="tagCell">
			<br/><br/>
			<?php echo __("Name"); ?>
			<input id="tagNameInput" type="text" style="width: 12em" onChange="requestUpdateTag()"/>
			&nbsp;&nbsp;&nbsp;
			<?php echo __("Rank"); ?>
			<input id="tagRankInput" type="number" style="width: 5em" onChange="requestUpdateTag()"/>
			&nbsp;&nbsp;&nbsp;
			<span style="display: none"> 
			<?php echo __("Label"); ?>
			<input id="tagLabelInput" type="text" style="width: 5em" onChange="requestUpdateTag()"/>
			&nbsp;&nbsp;&nbsp;
			</span>
			<?php echo __("Color"); ?>
			<input id="tagColorInput" type="text" style="width: 8em" onChange="requestUpdateTag()"/>
			<span id="tagColorDisplay" style="border: 1px solid black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<span style="display: none">
			<br/><br/>
			<?php echo __("Merge with"); ?>
			<select id="tagMergeSelect" onChange="requestMergeTag(document.getElementById('tagMergeSelect').value);"></select>
			</span>
			<br/><br/>
			<input type="button" value="<?php echo __("Delete"); ?>" onclick="requestRemoveTag();">
		</td></tr>
	</table>
	
	<!-- Tag-fragment editing section -->
	<tr id="tfsCell" style="display: none"><td class="gteSectionCell"><table class="gteSection">
		<tr><td class="gteHeader">
			<b><?php echo __("Fragments"); ?></b>
			&nbsp;&nbsp;&nbsp;
			<a style="color: linen; text-decoration: underline" href="#importTFs" onClick="blinkImportTFs();"><?php echo __("New group"); ?></a>
		</td></tr>
		<tr><td style="border: none"><small><?php echo __("List of fragments in the selected group."); ?></small></td></tr>
		<tr class="gteSelectionListRow"><td class="gteSelectionList"><div id="tfEditorTFs" style="height: 6em"></div></td></tr>
		<tr><td style="border: none" id="tfCell">
			<br/><br/>
			<?php echo __("Group"); ?>
			<select id="tfTagSelect" onChange="updateTFTag(document.getElementById('tfTagSelect').value);"></select>
			&nbsp;&nbsp;&nbsp;
			<?php echo __("Number"); ?>
			<input id="tfNumberInput" type="text" style="width: 7em" onChange="updateTFNumber(document.getElementById('tfNumberInput').value);"/>
			<br/><br/>
			<a style="color: blue; text-decoration: underline" id="removeTFLink" href=""><?php echo __("Delete"); ?></a>
			&nbsp;&nbsp;&nbsp;
			<a style="color: blue; text-decoration: underline; cursor: pointer" id="editTFLink"><?php echo __("Edit Transcription"); ?></a> 
		</td></tr>
	</table></td></tr>
	
	<!-- Table display settings section -->
	<tr id="displayCell"><td class="gteSectionCell"><table class="gteSection">
		<tr><td class="gteHeader" style="background: steelblue">
			<b><?php echo __("Display settings"); ?></b>
		</td></tr>
		<tr><td style="border: none">
			<?php echo __("Navigation Unit"); ?>
			<select id="displayUnitSelect" onChange="requestUpdateTableDisplay();">
				<option value="page"><?php echo __("Page"); ?></option>
				<option value="chapter"><?php echo __("Chapter"); ?></option>
			</select>
			<br/><br/>
			<?php echo __("Page span"); ?>
			<input id="displayPageSpanInput" type="number" style="width: 5em" onChange="requestUpdateTableDisplay();"/>
			<br/><br/>
			<?php echo __("Table background color"); ?>
			<input id="displayTableColorInput" type="text" style="width: 8em" onChange="requestUpdateTableDisplay();"/>
			<span id="displayTableColorDisplay" style="border: 1px solid black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<br/><br/>
			<?php echo __("Fragment text color"); ?>
			<input id="displayTextColorInput" type="text" style="width: 8em" onChange="requestUpdateTableDisplay();"/>
			<span id="displayTextColorDisplay" style="border: 1px solid black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<br/><br/>
			<?php echo __("Source for incipits"); ?>
			<select id="displayIncipitInput" onChange="requestUpdateTableDisplay();">
				<option value="-1"><?php echo __("None"); ?></option>
				<?php foreach ($sources as $source) { ?>
					<option value="<?php echo $source["id"]; ?>"><?php echo $source["name"]; ?></option>
				<?php } ?>
			</select>
		</td></tr>
	</table></td></tr>
</table>
