<?php
require_once(dirname(__FILE__).'/../ajax/ajax_queries.php');
?>

<script>
"use strict";
/**
 * Async queries made by the genetic table editor.
 * Queries are forwarded to "async_queries.php" and the proper refresh calls are made to keep the interface in sync.
 * All inputs should be disabled during a query.
 */

/**
 * Generic callbacks after a request that refresh the appropriate section of the interface
 */
function refreshDisplaySettings()
{
	readDisplay(<?php echo $item->id; ?>, 
		onDisplaySettingsRead,
		onDisplaySettingsFailed);
}
function refreshTFs()
{
	currentTFList = null;
	refreshTFEditor();
}
//the source callback accepts an optional id that will be made the currently selected source
function refreshTags(id)
{
	if (id !== undefined)
		currentTagId = id;
	currentTagList = null;
	refreshTFEditor();
}

/** 
 * Functions for requesting write operations on the data. These are all forwarded to "async_queries.php"
 */
function updateDisplaySettings(navUnit, pageSpan, tableColor, textColor, incipitSourceId)
{
	setEnabled(false);
	writeUpdateDisplay(<?php echo $item->id; ?>, navUnit == "chapter" ? 1 : 0, pageSpan, tableColor, textColor, incipitSourceId,
		refreshDisplaySettings,
		refreshDisplaySettings);
}
function updateTFTag(tagId)
{
	if (currentTFId < 0)
		return;
	setEnabled(false);
	writeUpdateTableFragmentTag(currentTFId, tagId, 
		function(xml)
		{
			currentTagId = tagId;
			currentTagList = null;
			refreshTFEditor();
		},
		null);
}
function newTag()
{
	setEnabled(false);
	writeNewTag(<?php echo $item->id; ?>, 
		refreshTags,
		refreshTags);
}
function newChapter()
{
	setEnabled(false);
	writeNewChapter(<?php echo $item->id; ?>, 
		refreshChapters,
		refreshChapters);
}
function removeTag()
{
	if (currentTagId < 0)
		return;
	setEnabled(false);
	writeRemoveTag(<?php echo $item->id; ?>, currentTagId,  
		refreshTags,
		refreshTags);
}
function updateTag(name, label, rank, color)
{
	if (currentTagId < 0)
		return;
	setEnabled(false);
	writeUpdateTag(<?php echo $item->id; ?>, currentTagId, name, label, rank, color, 
		refreshTags,
		refreshTags);
}
</script>