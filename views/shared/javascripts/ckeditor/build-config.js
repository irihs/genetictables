﻿/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * This file was added automatically by CKEditor builder.
 * You may re-use it at any time to build CKEditor again.
 *
 * If you would like to build CKEditor online again
 * (for example to upgrade), visit one the following links:
 *
 * (1) http://ckeditor.com/builder
 *     Visit online builder to build CKEditor from scratch.
 *
 * (2) http://ckeditor.com/builder/fe9237f55749ae2156d2d225a3283ef6
 *     Visit online builder to build CKEditor, starting with the same setup as before.
 *
 * (3) http://ckeditor.com/builder/download/fe9237f55749ae2156d2d225a3283ef6
 *     Straight download link to the latest version of CKEditor (Optimized) with the same setup as before.
 *
 * NOTE:
 *    This file is not used by CKEditor, you may remove it.
 *    Changing this file will not change your CKEditor configuration.
 */

var CKBUILDER_CONFIG = {
	skin: 'moono-lisa',
	preset: 'standard',
	ignore: [
		'.DS_Store',
		'.bender',
		'.editorconfig',
		'.gitattributes',
		'.gitignore',
		'.idea',
		'.jscsrc',
		'.jshintignore',
		'.jshintrc',
		'.mailmap',
		'.travis.yml',
		'bender-err.log',
		'bender-out.log',
		'bender.ci.js',
		'bender.js',
		'dev',
		'gruntfile.js',
		'less',
		'node_modules',
		'package.json',
		'tests'
	],
	plugins : {
		'a11yhelp' : 1,
		'about' : 1,
		'basicstyles' : 1,
		'bidi' : 1,
		'clipboard' : 1,
		'colorbutton' : 1,
		'contextmenu' : 1,
		'enterkey' : 1,
		'entities' : 1,
		'floatingspace' : 1,
		'font' : 1,
		'format' : 1,
		'horizontalrule' : 1,
		'indentlist' : 1,
		'lineheight' : 1,
		'link' : 1,
		'list' : 1,
		'magicline' : 1,
		'pastefromword' : 1,
		'pastetext' : 1,
		'removeformat' : 1,
		'showborders' : 1,
		'sourcearea' : 1,
		'specialchar' : 1,
		'stylescombo' : 1,
		'tab' : 1,
		'table' : 1,
		'tableresize' : 1,
		'tableresizerowandcolumn' : 1,
		'tableselection' : 1,
		'tabletools' : 1,
		'toolbar' : 1,
		'undo' : 1,
		'wsc' : 1,
		'wysiwygarea' : 1
	},
	languages : {
		'en' : 1,
		'fr' : 1
	}
};