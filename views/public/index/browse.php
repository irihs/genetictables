<?php
require_once(dirname(__FILE__).'/../../../db/db_tables.php');

$plugin = $GLOBALS['geneticTablesPlugin'];

$head = array('bodyclass' => 'primary',
	'title' => html_escape(__('Genetic Editions').' | '.__('Browse'))
    );//'content_class' => 'horizontal-nav');
echo head($head);
?>
<h1><?php echo __("Browse Genetic Editions"); ?></h1>
<h1 style="border-bottom: 1px solid #ccc">&nbsp;</h1>
<?php
$tables = get_db()->getTable('Item')->findBy(array('item_type' => $plugin->tableTypeName));
if (count($tables) > 0)
{
?>
	<table class="full">
	    <tbody>
	    	<?php foreach ($tables as $table) {
	    		$img = getTableImage($plugin, $table->id);
	    	?>
	    	<tr>
	    		<td><div style="width: 20em; height: 14em; background-size: contain; background-repeat: no-repeat; background-position: center; background-color: white; 
	    			background-image: url('<?php echo $img; ?>')"></div></td>
	    		<td style="width: 100%">
	    			<h2>&nbsp;<a href="<?php echo public_url("/items/show/$table->id#gtDiv"); ?>">
		    			<?php echo getTableTitle($plugin, $table->id); ?>
		    		</a></h2>
		    	</td>
	    	</tr>
	    	<?php } ?>
	    </tbody>
	</table>
<?php 
}

echo foot(); 
?>
