<?php
/**
 * Browse view for the table and work items
 */
require_once(dirname(__FILE__).'/../../../db/db_tables.php');
require_once(dirname(__FILE__).'/../../../db/db_works.php');

$plugin = $GLOBALS['geneticTablesPlugin'];

$head = array('bodyclass' => 'primary',
	'title' => html_escape(__('Genetic Tables').' | '.__('Browse')),
    'content_class' => 'horizontal-nav');
echo head($head);

//get the tables and works
$tables = get_db()->getTable('Item')->findBy(array('item_type' => $plugin->tableTypeName));
$works = get_db()->getTable('Item')->findBy(array('item_type' => $plugin->workTypeName));
?>

<h2><?php echo __('Works'); ?></h2>
<a class="add-page button small green" href="<?php echo html_escape(url('items/add').'?geneticWork=true'); ?>"><?php echo __('Add work'); ?></a>
<!-- Work items table -->
<table class="full">
    <thead>
        <tr>
            <th><?php echo __('Title'); ?></th>
            <th><?php echo __('Date Added'); ?></th>
            <th><?php echo __('Last Modified'); ?></th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($works as $work) { ?>
    	<tr>
    		<td>
    			<!-- Link to the public page -->
	    		<a href="<?php echo public_url("/items/show/$work->id"); ?>">
	    			<?php echo getWorkTitle($plugin, $work->id); ?>
	    		</a>
	    		<ul class="action-links group">
	    			<!-- Link to the admin page -->
	                <li><a class="edit" href="<?php echo url("/items/edit/$work->id#genetic-table-metadata"); ?>">
	                    <?php echo __('Edit'); ?>
	                </a></li>
	            </ul>
    		</td>
    		<!-- Added and modified dates -->
    		<td><?php echo format_date($work->added); ?></td>
    		<td><?php echo format_date($work->modified); ?></td>
    	</tr>
    	<?php } ?>
    </tbody>
</table>
<br/><br/>

<h2><?php echo __('Genetic Tables'); ?></h2>
<!-- if works exist, show a dropdown for works that creates a table on selection -->
<?php if (count($works) > 0) { ?> 
<a class="add-page button small green"  
	onclick="window.location.assign('<?php echo html_escape(url('items/add').'?geneticTable=true'); ?>&workId='+document.getElementById('workForTable').value)"><?php echo __('Add a table'); ?></a>&nbsp;
<?php echo __('for'); ?>&nbsp;
<select id="workForTable">
<?php foreach ($works as $work) { ?>
	<option value="<?php echo $work->id; ?>"><?php echo getWorkTitle($plugin, $work->id); ?></option>
<?php }} ?>
</select>
<!-- Gentic table items table -->
<table class="full">
    <thead>
        <tr>
            <th><?php echo __('Title'); ?></th>
            <th><?php echo __('Date Added'); ?></th>
            <th><?php echo __('Last Modified'); ?></th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach ($tables as $table) { ?>
    	<tr>
    		<td>
	    		<!-- Link to the public page -->
	    		<a href="<?php echo public_url("/items/show/$table->id"); ?>">
	    			<?php echo getTableTitle($plugin, $table->id); ?>
	    		</a>
	    		<ul class="action-links group">
	    			<!-- Link to the admin page -->
	                <li><a class="edit" href="<?php echo url("/items/edit/$table->id#genetic-table-metadata"); ?>">
	                    <?php echo __('Edit'); ?>
	                </a></li>
	            </ul>
    		</td>
    		<!-- Added and modified dates -->
    		<td><?php echo format_date($table->added); ?></td>
    		<td><?php echo format_date($table->modified); ?></td>
    	</tr>
    	<?php } ?>
    </tbody>
</table>
