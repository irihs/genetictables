<script>
/**
 * Javascript helper functions
 */

//put text in a bold element
function bold(text)
{
	var element = document.createElement("b");
	element.innerHTML = text;
	return element
}

//remove all children from an element
function clear(element)
{
	while (element.childNodes.length > 0)
		element.removeChild(element.childNodes[element.childNodes.length-1]);
}

//make an element and all children disabled
function setElementEnabled(element, enabled)
{
	if (element.disabled !== undefined)
		element.disabled = !enabled;
	for (var i=0;i<element.childNodes.length;i++)
		setElementEnabled(element.childNodes[i], enabled);
}

//make a span element containing some blank space
function createSpace()
{
	var space = document.createElement("SPAN");
	space.innerHTML = "&nbsp;&nbsp;&nbsp;";
	return space;
}
</script>