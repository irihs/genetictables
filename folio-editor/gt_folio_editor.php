<?php
require_once(dirname(__FILE__).'/../db/db_folios.php');
require_once(dirname(__FILE__).'/../ajax/ajax_queries.php');

/**
 * A component for editing a folio transcription. The digitized image is displayed on the left side of a dual viewer and a CKEditor on the right.
 * A preview of the dual viewer as it will be displayed on the public page is shown underneath.
 */

$ckpath = get_option('gt_cke_path');
//Retreive the image for this folio
$img = getFolioImageUrl($this, $item->id);
if (!$img)
{
	?><b><?php echo __("Save an image for this folio to enable this tab."); ?></b>
	<?php 
}
else
{
	//Retrieve the transcription for this folio
	$trans = getFolioTranscription($this, $item->id);
	//Retrieve the transcription for this folio
	require_once(dirname(__FILE__).'/../db/db_tables.php');
	$trans = getFolioTranscription($this, $item->id);
	$workId = getFolioWorkId($this, $item->id);
	
	if (!$ckpath) echo js_tag("ckeditor/ckeditor");
	else { ?><script src="<?php echo get_option('gt_cke_path'); ?>/ckeditor.js"></script><?php }
	?>
	
	<style>
		.gteSectionCell {border: none; background: transparent}
		.gteSection {margin-bottom: 2em; border: 1px solid gray}
		.gteSelectionListRow {}
		.gteSelectionList {border: none; border-bottom: 1px solid lightgray; margin: .1em; padding: .1em; overflow-y: auto; resize: vertical; font-size: small}
		.gteHeader {border: none; background: royalblue; color:white; height: 1.5em}
		.gteSubHeader {border: none; background: lightgray; font-weight: bold}
	</style>
	
	<style id="transcriptionStyle">
		<?php echo getWorkTranscriptionCss($this, $workId); ?>
	</style>
	<!-- the transcription is hidden in this div for easy JS manipulation -->
	<div id="transcription" style="display: none">
		<div class="transcription-block" id="transcription-block"><?php echo $trans; ?></div>
	</div>
	<?php 
	require_once(dirname(__FILE__).'/../dual-viewer/gt_dual_viewer.php');
	$codes = getCodeData($this, $workId);
	?>
	
	<table id="errorZone" style="width: 100%; display: none; background-color: #dddddd"><tr>
		<td><span id="errorZoneMessage" style="color: red"></span></td>
		<td style="text-align: right"><a style="color: blue; text-decoration: underline" onClick="errorHandler(null);"><?php echo __("Hide"); ?></a></td>
	</tr></table>
	
	<table class="gteHeader" style="margin: 0px"><tr style="background: transparent">
		<td class="gteSectionCell" style="width: 33%"><h4 style="margin: 0px"><?php echo __("Editor"); ?></h4></td>
		<td class="gteSectionCell" style="width: 33%; text-align: center; vertical-align: middle">
			<!-- Quick save button -->
			<input type="button" id="gt_folio_editor_save_button" value="<?php echo __("Save"); ?>" onClick="saveTranscription();"></input>
		</td>
		<td class="gteSectionCell" style="width: 33%"></td>
	</tr></table>
	<!-- Transcription editor -->
	<div id="gt_dual_editor_div" style="width: 100%; height: 64em; border: 1px solid darkgray"></div>
	
	<br>
	<table class="gteHeader" style="margin: 0px"><tr style="background: transparent">
		<td class="gteSectionCell" style="width: 33%"><h4 style="margin: 0px"><?php echo __("Preview"); ?></h4></td>
		<td class="gteSectionCell" style="width: 33%; text-align: center; vertical-align: middle">
			<input type="button" value="&#8635; <?php echo __("Refresh"); ?>" onClick="refreshPreview();"></input>
		</td>
		<td class="gteSectionCell" style="width: 33%"></td>
	</tr></table>
	<!-- Transcription preview -->
	<div id="gt_dual_previewer_div" style="width: 100%; height: 64em; border: 1px solid darkgray">
	</div>
	
	<script>
		"use strict";

		function errorHandler(error)
		{
			var errorZone = document.getElementById("errorZone");
			if (error === null)
				errorZone.style.display = "none";
			else
			{
				document.getElementById("errorZoneMessage").innerHTML = error;
				errorZone.style.display = "table";
				window.location = "#";
			}
		}
		
// 		document.getElementById("gt_dual_editor_div").addEventListener("visibilitychange", function() {
// 			  console.log('!!!');
// 		});
		
		//configure ckeditor
		var codeStyles = [
			<?php foreach ($codes as $v) { ?>new CKEDITOR.style({element: 'span', attributes: {'style': '<?php echo $v['style']; ?>'}}),
			<?php }?>
		];
		CKEDITOR.plugins.add('gtCodes',
		{
			requires: ['richcombo'],
			init: function(editor)
			{
				editor.ui.addRichCombo('gtCodes', 
				{
					label: '<?php echo __('Codes'); ?>',
					title: '<?php echo __('Codes'); ?>',
					multiSelect: false,

		            panel :
		            {
		               css: [editor.config.contentsCss],
		               //voiceLabel: editor.lang.format.panelVoiceLabel
		            },
		            //create entries for the transcription codes
					init: function()
					{
						<?php $index = 0; foreach ($codes as $v) { ?>this.add(<?php echo "'$index', '&nbsp;".$v['name']."'"; ?>);
						<?php $index++;}?>
					},
					//if an option is clicked, apply style to selected text
					onClick: function(value)
					{
						var style = codeStyles[parseInt(value)];
						if (!style.checkActive(editor.elementPath(), editor))
							editor.applyStyle(style);
						else editor.removeStyle(style);
					}
				});
			}
		});
	
		//the viewer shows a side by side view of the folio image and transcription
		var viewer = new GTDualViewer(document.getElementById("gt_dual_editor_div"), true);
		viewer.setSrc('<?php echo $img; ?>', document.getElementById("transcription").innerHTML, document.getElementById("transcriptionStyle").innerHTML);
		viewer.transCell.style.overflowX = "auto";
		viewer.transCell.style.overflowY = "hidden";
		//the ckeditor is set to take the place of the regular content of the transcription pane of the viewer
		var editor = CKEDITOR.replace(viewer.transPlain, 
		{
			enterMode: CKEDITOR.ENTER_BR,
			extraPlugins: 'gtCodes'
		});
		function resizeEditor(editor) {return function()
		{
			editor.resize("100%", viewer.transCell.clientHeight);
		};}
		//resize editor if an event might have changed the layout
		editor.on('contentDom', resizeEditor(editor));
		viewer.onLayoutChanged.push(resizeEditor(editor));
		viewer.onFullscreen.push(resizeEditor(editor));
		window.addEventListener("resize", resizeEditor(editor));

		//the previewer that shows the image and the regular transcription/rendering
		var previewer = new GTDualViewer(document.getElementById("gt_dual_previewer_div"));
		previewer.setSrc('<?php echo $img; ?>', document.getElementById("transcription").innerHTML, document.getElementById("transcriptionStyle").innerHTML);

		//async save of the transcription (without the need to submit the form which would navigate away)
		function saveTranscription()
		{
			var button = document.getElementById("gt_folio_editor_save_button");
			//disable the button until the server responds
			button.disabled = true;
			//get the omeka field for the transcription
			var field = document.getElementById("Elements-<?php echo $this->transcriptionElementId; ?>-0-text");
			//update the omeka field
			field.innerHTML = editor.getData();
			writeUpdateFolioTranscription(<?php echo $item->id; ?>, editor.getData(), 
				function() {button.disabled = false;}, 
				function(error) {button.disabled = false; errorHandler(error)});
// 			var elems = document.getElementsByName("submit");
// 			for (var i=0;i<elems.length;i++)
// 				if (typeof(elems[i].form) != "undefined" && elems[i].form == field.form)
// 					{elems[i].click(); break;}
		}
		
		function refreshPreview()
		{
			document.getElementById("transcription-block").innerHTML = editor.getData();
			//setSrc will refresh the transcription
			previewer.setSrc('<?php echo $img; ?>', document.getElementById("transcription").innerHTML, document.getElementById("transcriptionStyle").innerHTML);
		}

		editor.on('instanceReady', function(e)
		{
		});
	</script>
	<?php 
}
?>
