<?php 
class GeneticTables_IndexController extends Omeka_Controller_AbstractActionController
{
	public function init()
	{
		// Set the model class so this controller can perform some functions,
		// such as $this->findById()
		$this->_helper->db->setDefaultModelName('GeneticTablesList');
	}
	
	public function indexAction()
	{
		// Always go to browse.
		$this->_helper->redirector('browse');
		return;
	}
	
	public function ajaxAction()
	{
	}
}
?>