<div class="field">
    <div id="gt_cke_path_label" class="two columns alpha">
        <label for="gt_cke_path"><?php echo __('CKEditor path'); ?></label>
    </div>
    <div class="inputs five columns omega">
        <p class="explanation"><?php 
        	echo __("Web path to a custom CKEditor installation. If set, it will be used instead of the default editor."); ?><br>
        	<?php echo __("See "); ?>
        	<a href="https://docs.ckeditor.com/ckeditor4/latest/">https://docs.ckeditor.com/ckeditor4/latest/</a>
        </p>
        <?php echo get_view()->formText('gt_cke_path', get_option('gt_cke_path'), array()); ?>
    </div>
</div>
