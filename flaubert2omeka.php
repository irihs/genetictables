<?php
require_once('db/db_tables.php');
require_once('db/db_tags.php');

/**
 * Converts an exported Flaubert database to the plugin format. Database must have been exported with the Flaubert2Omeka Java utility.
 * TODO: deprecated? Better to use the async javascript in 'work-editor/gt_workeditor_import.php'
 */
function handleFlaubert2Omeka($p, $item)
{
	if (array_key_exists('flaubert2omeka', $_POST) && $_POST['flaubert2omeka'] == "true")
	{
		try
		{
			setTableTranscriptionCss($p, $item->id, 's { font-style: strike; }
.biffure { background-color: #ffffcc; }
.censure { background-color: #C9D3B4; }
.crayon { color: #A49F9D; }
.incertain { background-color: #E2E1E0; }
.ital-noir { font-style: italic; }
.bleu-droit { color: #3333cc; }
.note-regie { color: #940000; }
.relecture { background-color: #FFCC99; }
hors-corpus { width: 100% }
div.transcription-block { font-family: Arial; }
div.transcription-block td { font-family: Arial; border: none; vertical-align: top; line-height: 150%; }
div.transcription-block tr { vertical-align: top; padding: 0px;}
div.transcription-block table { border: none; padding: 0px; }
div.transcription-block i { font-style: italic; color: #3333cc; }');
			
			setTableTranscriptionHelp($p, $item->id, '<table width="100%">
	<tr><td width="100%"><div align="left">
		<ul>
			<li>Le texte principal qui correspond au premier jet de Flaubert ou &agrave; la mise au propre du brouillon pr&eacute;c&eacute;dent est en caract&egrave;res romains et en noir.</li>
			<li>Les ajouts marginaux ou interlin&eacute;aires sont en <span style="color: #0000FF; font-style: italic">italiques bleus</span></i>.</li>
			<li>Ce qui est barr&eacute; est <strike>barr&eacute;</strike>.</li>
			<li>Ce qui est soulign&eacute; est <u>soulign&eacute;</u>.</li>
			<li>Les passages biff&eacute;s sont encadr&eacute;s par une double accolade {{&nbsp;&nbsp;&nbsp;}} et apparaissent &agrave; l\'&eacute;cran sur fond jaune.</li>
			<li>Les passages barr&eacute;s d\'une croix de Saint-Andr&eacute; sont encadr&eacute;s par un double crochet [[&nbsp;&nbsp;&nbsp;]].</li>
			<li>Les mots incertains sont suivis d\'un ast&eacute;risque. Deux ast&eacute;risques encadrent une suite de mots incertains.</li>
			<li>Les mots ou les passages illisibles sont marqu&eacute;s [illis.]. On signale par des points de part et d\'autre la longueur du texte non d&eacute;chiffr&eacute;.</li>
			<li>Nous avons choisi de restituer les accents.</li>
			<li>En revanche les fautes, les abr&eacute;viations, les graphies anciennes sont maintenues.</li>
		</ul>
    </div></td></tr>
</table>');
			
			setTableFooter($p, $item->id, 'Le tableau présente les folios depuis le premier état (en bas) jusqu\'au texte publié (en haut).<br/>
Classement génétique : <b>Marie Durel</b> - Coordination des transcriptions : <b>Danielle Girard</b>');
			
			$work = getTableWorkName($p, $item->id);
			//$images = "/mnt/c/Users/aburn/Documents/work/flaubert/exports/bovary/";
			$images = $_POST['flaubertPath'];
			if (substr($images, -1) != '/')
				$images = $images.'/';
			error_log("Importing ".$images.$_POST['flaubertXml']." into table ".$work);
			$xml = new XMLReader;
			//flaubert.xml
			$xml->open($images.$_POST['flaubertXml']);
			
			$f2oSourceIds = [];
			$f2oFolioIds = [];
			$f2oFragIds = [];
			$f2oFragOrigins = [];
			$f2oFragPrevs = [];
			
			set_time_limit(0);
			
			$defaultColors = [
				hexdec("f44336"),
				hexdec("e91e63"),
				hexdec("9c27b0"),
				hexdec("673ab7"),
				hexdec("3f51b5"),
				hexdec("1e88e5"),
				hexdec("0288d1"),
				hexdec("0097a7"),
				hexdec("009688"),
				hexdec("43a047"),
				hexdec("558b2f"),
				hexdec("827717"),
				hexdec("e65100"),
				hexdec("f4511e"),
				hexdec("795548"),
				hexdec("757575"),
				hexdec("607d8b")
			];
			$colCnt = 0;
			
			while ($xml->read())
			{
				if ($xml->name == "Source" && $xml->nodeType == XMLReader::ELEMENT)
				{
					$src = $xml->getAttribute("name");
					$collection = insert_collection(
							array(
									"public" => true,
									"featured" => false),
							array(
									"Dublin Core" => array(
											"Title" => array(array("text" => $work." - ".$src, "html" => false))))
							);
					$stmt = get_db()->prepare("INSERT INTO $p->sourceTableName (id, tableId, rank, color, shortName) VALUES (?, ?, 1, ?, ?)");
					$stmt->execute(array($collection->id, $item->id, $defaultColors[$colCnt%count($defaultColors)], $src));
					$f2oSourceIds[$src] = $collection->id;
					$colCnt++;
				}
				else if ($xml->name == "Folio" && $xml->nodeType == XMLReader::ELEMENT)
				{
					$suffix = ($xml->getAttribute("recto_verso")!= "null" && strlen($xml->getAttribute("recto_verso")) > 0 ? $xml->getAttribute("recto_verso"): "").
						($xml->getAttribute("emplacement")!= "null" && strlen($xml->getAttribute("emplacement")) > 0 ? strtolower($xml->getAttribute("emplacement")) : "");
					$num = $xml->getAttribute("folio").$suffix;
					$src = $xml->getAttribute("source");
					$folder = strripos($src, " ") === FALSE ? $src : substr($src, 0, strripos($src, " "));
					$image = $images.$folder."/jpg/".sprintf("%03d", $xml->getAttribute("folio")).$suffix.".jpg";
					if (!file_exists($image))
						$image = $images.$folder."/".sprintf("%04d", $xml->getAttribute("folio")).$suffix.".jpg";
					$fid = $xml->getAttribute("id");
					$transcription = str_replace("&quot;", "\"", str_replace("&lt;", "<", str_replace("&amp;", "&", trim($xml->readString()))));
					if (!file_exists($image))
						error_log("Couldn't find image ".$image);
					
					$folio = insert_item(
							array(
									"public" => true,
									"featured" => false,
									"collection_id" => $f2oSourceIds[$src],
									"item_type_name" => $p->folioTypeName),
							array(
									"Dublin Core" => array(
											"Title" => array(array("text" => $work." - ".$src." - Folio ".$num, "html" => false))),
									'Item Type Metadata'=> array(
											$p->workElementName => array(array("text" => $work, "html" => false)),
											$p->numberElementName => array(array("text" => "".$num, "html" => false)),
											$p->transcriptionElementName => array(array("text" => $transcription, "html" => true)))),
							file_exists($image) ? array("file_transfer_type" => "Filesystem", "files" => $image, "file_ingest_options" => "") : array()
							);
					
					$f2oFolioIds[$fid] = $folio->id;
				}
				else if ($xml->name == "Fragment" && $xml->nodeType == XMLReader::ELEMENT)
				{
					$fid = $xml->getAttribute("id");
					$foliofid = $xml->getAttribute("id_folio");
					if (!array_key_exists($foliofid, $f2oFolioIds))
						continue;
					$tagId;
					if (!tagNameExists($p, $item->id, $xml->getAttribute('etat')))
					{
						$tagId = createTag($p, $item->id);
						setTagName($p, $tagId, $xml->getAttribute('etat'));
						updateTagLabel($p, $tagId, $xml->getAttribute('etat'));
						updateTagColor($p, $tagId, $defaultColors[$colCnt%count($defaultColors)]);
						$colCnt++;
					}
					else $tagId = getTagIdForName($p, $item->id, $xml->getAttribute('etat'));
						
					get_db()->query("INSERT INTO ".$p->fragmentTableName." (tableId, folioId, fromPage, fromLine, toPage, toLine, position, withheld, parentId, previousId, tagId) VALUES (".
	 					$item->id.", ".
						$f2oFolioIds[$foliofid].", ".
						$xml->getAttribute('debut_page').", ".
						$xml->getAttribute('debut_ligne').", ".
						$xml->getAttribute('fin_page').", ".
						$xml->getAttribute('fin_ligne').", ".
						"'".($xml->getAttribute('emplacement') != "null" ? $xml->getAttribute('emplacement') : "")."', ".
						($xml->getAttribute('present') == 'n' ? 1 : 0).", ".
	 					"NULL, ".
						"NULL, ".
						$tagId.
	 					")");
	
	 				$rs = get_db()->query("SELECT MAX(id) as id FROM ".$p->fragmentTableName." WHERE tableId=".$item->id)->fetchAll();
	 				$f2oFragIds[$fid] = $rs[0]['id'];
	 				$f2oFragOrigins[$fid] = $xml->getAttribute('f_origine');
	 				$f2oFragPrevs[$fid] = $xml->getAttribute('f_prec_vertical');
				}
				else if ($xml->name == "Chapitre" && $xml->nodeType == XMLReader::ELEMENT)
				{
					get_db()->query("INSERT INTO ".$p->chaptersTableName." (tableId, name, toPage) VALUES (".
						$item->id.", ".
						"'".$xml->getAttribute('nom')."', ".
						$xml->getAttribute('fin_page').
						")");
				}
			}
			
			foreach ($f2oFragIds as $fid => $oid)
	 		{
	 			if ($f2oFragOrigins[$fid] != "null" && array_key_exists($f2oFragOrigins[$fid], $f2oFragIds))
		 			get_db()->query("UPDATE ".$p->fragmentTableName." SET parentId=".$f2oFragIds[$f2oFragOrigins[$fid]]." WHERE id=".$oid);
		 		if ($f2oFragPrevs[$fid] != "null" && $f2oFragPrevs[$fid] != "0" && array_key_exists($f2oFragPrevs[$fid], $f2oFragIds))
	 				get_db()->query("UPDATE ".$p->fragmentTableName." SET previousId=".$f2oFragIds[$f2oFragPrevs[$fid]]." WHERE id=".$oid);
		 	}
			
			$xml->close();
		}
		catch (Exception $e) {error_log($e->getMessage());}
	}
}
?>